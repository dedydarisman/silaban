# Local Installation

Python 3.7+ is recommended

You can also create virtual environment to use this module.
1.	Clone source code from netoprmgr repository
	```
	https://gitlab.com/dedydarisman/silaban.git
	cd netoprmgr
	```
	
2.	Install requirements
	```
	pip install -r requirements.txt
	```

# Usage

1.	Run python -m netoprmgr__main__

	![run_main_not_folder](https://ibb.co/9v5wTYb)

2.	Or you can run python from your directory that contains netoprmgr packages

	![run_main_folder](https://ibb.co/7GJHsqB)

3.	Open web browser, type localhost:5002

	![open_web]https://ibb.co/dKtZ9X7)