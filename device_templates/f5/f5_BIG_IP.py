import re
import requests
from netoprmgr.script.insert_dbreport_f5 import insert_dbreport_f5
from netoprmgr.script.eosl_ip import eosl_ip
import datetime
import numpy as np
import numpy as np

class f5_BIG_IP:
    def __init__(self,file):
        #variable constructor
        self.file = file
        #read all things in file
        try:
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
        except:
            try:
                read_file = open(self.file, 'r', encoding='latin-1')
                read_file_list = read_file.readlines()
            except:
                print('Error Codec!!!')
                pass
        for line in read_file_list:
            #get sn
            if re.findall('PID:.*,\s+SN:\s+(\S+)',line):
                sn = re.findall('PID:.*,\s+SN:\s+(\S+)',line)
                sn = sn[0]
                break
            else:
                sn = 'Tidak di temukan'
        for line in read_file_list:
            #get ip
            if re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line):
                ip = re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line)
                ip = ip[0]
                break
            else:
                ip = 'Tidak di temukan'

        #SOFTWARE TABLE
        #GET DEVICENAME
        read_file_list_hostname  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('}', line):
                break
            if read_file_logic_check == True:
                read_file_list_hostname.append(line.strip())
            if 'sys global-settings' in line:
                read_file_logic_check = True
            count_read_file+=1

        for line in read_file_list_hostname:
            #Device Name
            if re.findall('hostname\s+(\S+)',line):
                devicename = re.findall('hostname\s+(\S+)',line)
                devicename = devicename[0]
                devicename = devicename
                # print(devicename)
                break
            else:
                devicename = self.file

        for line in read_file_list:
            #Model
            if re.findall('Name\s+(\S+\s+\S+\s+Guest|\S+\s+\S+)',line):
                model = re.findall('Name\s+(\S+\s+\S+\s+Guest|\S+\s+\S+)',line)
                model = model[0]
                model = model
                print(model)
                print("model")
                break

        for line in read_file_list:
            #OS Version
            if re.findall('Version\s+(\d+.*)',line):
                iosversion = re.findall('Version\s+(\d+.*)',line)
                iosversion = iosversion[0]
                iosversion = iosversion
                version = iosversion
                print(version)
                print("version")
                break
        
        for line in read_file_list:
            #Build menggunakan field Confreg
            if re.findall('Build\s+(\d+.*)',line):
                confreg = re.findall('Build\s+(\d+.*)',line)
                confreg = confreg[0]
                confreg = confreg
                print(confreg)
                print("confreg")
                break

        for line in read_file_list:
            #Uptime (Days)
            if re.findall('.*\s+up\s+(\d+\s+\S+),',line):
                uptime = re.findall('.*\s+up\s+(\d+\s+\S+),',line)
                uptime = uptime[0]
                # print(uptime)
                break

        #HARDWARE TABLE
        read_file_list_hw  = []
        start_line_hw = False
        for line in read_file_list:
            if 'System Information' in line.strip():
                start_line_hw = True
            if 'Host Board Part Revision' in line.strip():
                break
            if start_line_hw == True:
                read_file_list_hw.append(line.strip())

        list_tipe = []
        list_serial_number = []
        for line in read_file_list_hw:
            #Type
            if re.findall('^Type\s+(\S+)', line):
                tipe = re.findall('^Type\s+(\S+)', line)
                tipe = tipe[0]
                tipe = tipe.strip()
                list_tipe.append(tipe)
                print(tipe)
                print("tipe")
            #Serial Number
            if re.findall('^[C,A]\S+\s+Serial\s+(\S+)', line):
                serial_number = re.findall('[C,A]\S+\s+Serial\s+(\S+)', line)
                serial_number = serial_number[0]
                list_serial_number.append(serial_number.upper())
                print(serial_number)
                print("serial_number")
        #EOS SOFTWARE
        try:
            execute_eosl_ip = eosl_ip()
            resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/software/'+version)
            date = resp.json()
            if str(date) == "None":
                date_eos = 'Not Announced'
                date_last = 'Not Announced'
            else:
                date = date[0]
                date_eos = date["eos_maintenance"]
                date_last = date["last_day_of_support"]
        except:
            date_eos = 'Server EOSL Down'
            date_last = 'Server EOSL Down'

        #EOS TABEL
        list_date = []
        for tipe in list_tipe:
            try:
                eosl_tipe = tipe
                eosl_tipe = re.findall('(\S+)',eosl_tipe)
                if len(eosl_tipe) == 0:
                    date = '-'
                else:
                    eosl_tipe = eosl_tipe[0]
                    execute_eosl_ip = eosl_ip()
                    resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/hardware/'+eosl_tipe)
                    date = resp.json()
                    if str(date) == "None":
                        date = 'Isi Manual'
                    else:
                        date = date[0]
                        date = date["date"]
            except:
                date = 'Server EOSL Down'
            list_date.append(date)

        #Stack identification
        stack_break = False
        stack_cond = False
        stack_list = []
        for line in read_file_list:
            if re.findall('\d+\s+\d+\s+(WS\S+)\s+\S+\s+\S+',line):
                stack_break = True
                stack = re.findall('\d+\s+\d+\s+(WS\S+)\s+\S+\s+\S+',line)
                stack = stack[0]
                stack_list.append(stack)
                print(stack)
                print("stack")
            #break loop
            if stack_break == True and re.findall('.*#',line):
                break
        if len(stack_list) == 0:
            stack_cond = False
        else:
            stack_cond = True

        eos_tipe = []
        eos_date = []
        if stack_cond == True:
            for stack in stack_list:
                for enum, tipe in enumerate(list_tipe):
                    if stack in tipe:
                        eos_tipe.append(tipe)
                        eos_date.append(list_date[enum])
        else:
            eos_tipe.append(list_tipe[0])
            eos_date.append(list_date[0])
        device_class_name = self.__class__.__name__
        file_name = self.file
        
        #HARDWARE CONDITION ANALYSIS (ENVIRONMENT)
        #GET FAN
        read_file_list_fan  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_fan.append(line.strip())
            if 'Fan Status' in line:
                read_file_logic_check = True
            count_read_file+=1
               
        #GET HUMADITY
        read_file_list_fan_humidity  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_fan_humidity.append(line.strip())
            if 'Humidity Status' in line:
                read_file_logic_check = True
            count_read_file+=1
          
        list_fan = []
        list_fan_cond_cp = []
        for i in read_file_list_fan:
            if re.findall('^(\d+)\s+\S+',i):
                regex_fan = re.findall('^(\d+)\s+\S+',i)
                fan = regex_fan[0]
                fan = fan.strip()
                list_fan.append('FAN '+fan)
                print(fan)
                print("fan")
                
            if re.findall('^\d+\s+(\S+)', i):
                regex_fan_cond = re.findall('^\d+\s+(\S+)', i)
                fan_cond = regex_fan_cond[0]
                fan_cond = fan_cond.strip()
                list_fan_cond_cp.append(fan_cond)
                print(fan_cond)
                print("fan_cond")
        
        for i in read_file_list_fan_humidity:
            if re.findall('^(\d+)\s+\S+',i):
                regex_fan = re.findall('^(\d+)\s+\S+',i)
                fan = regex_fan[0]
                fan = fan.strip()
                list_fan.append('Humidity '+fan)
                print(fan)
                print("fan Humidity")
            if re.findall('^\d+\s+(\S+)', i):
                regex_fan_cond = re.findall('^\d+\s+(\S+)', i)
                fan_cond = regex_fan_cond[0]
                fan_cond = fan_cond.strip()
                list_fan_cond_cp.append(fan_cond)
                print(fan_cond)
                print("fan_cond")
        
        
        #GET TEMPERATURE
        read_file_list_temp  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_temp.append(line.strip())
            if 'Temperature Status' in line:
                read_file_logic_check = True
            count_read_file+=1
        
        list_temp = []
        list_temp_cond = []
        for i in read_file_list_temp:
            if re.findall('^\d+\s+\d+\s+\d+\s+\d+\s+(.*)',i):
                regex_temp = re.findall('^\d+\s+\d+\s+\d+\s+\d+\s+(.*)',i)
                temp = regex_temp[0]
                temp = temp.strip()
                list_temp.append(temp)
                print(temp)
                print("temp")
            if re.findall('^\d+\s+\d+\s+(\d+)\s+\d+\s+.*', i):
                regex_temp_cond = re.findall('^\d+\s+\d+\s+(\d+)\s+\d+\s+.*', i)
                temp_cond = regex_temp_cond[0]
                temp_cond = temp_cond.strip()
                list_temp_cond.append(temp_cond+ '° C')
                print(temp_cond)
                print("temp_cond")
       
        
        #GET PSU
        read_file_list_psu  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_psu.append(line.strip())
            if 'Power Supply Status' in line:
                read_file_logic_check = True
            count_read_file+=1

        list_psu = []
        list_psu_cond = []
        for i in read_file_list_psu:
            if re.findall('^(\d+)\s+\S+\s+\S+',i):
                regex_psu = re.findall('^(\d+)\s+\S+\s+\S+',i)
                psu = regex_psu[0]
                psu = psu.strip()
                list_psu.append('PSU ' +psu)
                print(psu)
                print("psu")
            if re.findall('^\d+\s+(\S+)\s+\S+', i):
                regex_psu_cond = re.findall('^\d+\s+(\S+)\s+\S+', i)
                psu_cond = regex_psu_cond[0]
                psu_cond = psu_cond.strip()
                list_psu_cond.append(psu_cond)
                print(psu_cond)
                print("psu_cond")
  
        #KONDISI JIKA NULL
        if len(list_fan) == 0:
            list_fan = ['-']
        if len(list_fan_cond_cp) == 0:
            list_fan_cond_cp = ['-']
        if len(list_temp) == 0:
            list_temp = ['-']
        if len(list_temp_cond) == 0:
            list_temp_cond = ['-']
        if len(list_psu) == 0:
            list_psu = ['-']
        if len(list_psu_cond) == 0:
            list_psu_cond = ['-']
        if len(list_fan_cond_cp) == 0:
            list_psu_cond = ['-']

        #CPU SUMMARY TABLE
        #GET CPU
        read_file_list_cpu  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_cpu.append(line.strip())
            if 'System CPU Usage' in line:
                read_file_logic_check = True
            count_read_file+=1

        for line in read_file_list_cpu:
            #CPU 3 HOURS
            if re.findall('^Utilization\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+',line):
                cpu_3h = re.findall('^Utilization\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+',line)
                cpu_3h = float(cpu_3h[0])
            
            #CPU 24 HOURS
            if re.findall('^Utilization\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+',line):
                cpu_24h = re.findall('^Utilization\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+',line)
                cpu_24h = float(cpu_24h[0])
            
            #CPU 7 DAYS
            if re.findall('^Utilization\s+\d+\s+\d+\s+\d+\s+(\d+)\s+\d+',line):
                cpu_7d = re.findall('^Utilization\s+\d+\s+\d+\s+\d+\s+(\d+)\s+\d+',line)
                cpu_7d = float(cpu_7d[0])
            
            #CPU 30 DAYS
            if re.findall('^Utilization\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+)',line):
                cpu_30d = re.findall('^Utilization\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+)',line)
                cpu_30d = float(cpu_30d[0])
            
            #CPU CURRENT
            if re.findall('^Utilization\s+(\d+)\s+\d+\s+\d+\s+\d+\s+\d+',line):
                cpu_current = re.findall('^Utilization\s+(\d+)\s+\d+\s+\d+\s+\d+\s+\d+',line)
                cpu_current = float(cpu_current[0])
                
                #CPU STATUS
                if cpu_current<21 :
                    cpu_status='Low'
                elif cpu_current<81 :
                    cpu_status='Medium'
                else:
                    cpu_status='High'
                cpu_status = str(cpu_status)
                # print(cpu_status)
                break
        
        #MEMORY SUMMARY TABLE
        #GET MEMORY
        read_file_list_mem  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_mem.append(line.strip())
            if 'Memory Used' in line:
                read_file_logic_check = True
            count_read_file+=1

        memory_name_list = []
        memory_current_list = []
        memory_3h_list = []
        memory_24h_list = []
        memory_7d_list = []
        memory_30d_list = []
        memory_status_list = []
        for line in read_file_list_mem:
            #MEMORY NAME
            if re.findall('(.*Used)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line):
                memory_name = re.findall('(.*Used)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line)
                memory_name = memory_name[0]
                memory_name_list.append(memory_name)
            
            #MEMORY 3 HOURS
            if re.findall('.*Used\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                memory_3h = re.findall('.*Used\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                memory_3h = memory_3h[0]
                if re.findall('\d+', memory_3h):
                    memory_3h_list.append(float(memory_3h))
                else:
                    memory_3h = 0
                    memory_3h_list.append(float(memory_3h))

            #MEMORY 24 HOURS
            if re.findall('.*Used\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                memory_24h = re.findall('.*Used\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                memory_24h = memory_24h[0]
                if re.findall('\d+', memory_24h):
                    memory_24h_list.append(float(memory_24h))
                else:
                    memory_24h = 0
                    memory_24h_list.append(float(memory_24h))
            
            #MEMORY 7 DAYS
            if re.findall('.*Used\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                memory_7d = re.findall('.*Used\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                memory_7d = memory_7d[0]
                if re.findall('\d+', memory_7d):
                    memory_7d_list.append(float(memory_7d))
                else:
                    memory_7d = 0
                    memory_7d_list.append(float(memory_7d))
            
            #MEMORY 30 DAYS
            if re.findall('.*Used\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                memory_30d = re.findall('.*Used\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                memory_30d = memory_30d[0]
                if re.findall('\d+', memory_30d):
                    memory_30d_list.append(float(memory_30d))
                else:
                    memory_30d = 0
                    memory_30d_list.append(float(memory_30d))

            #MEMORY CURRENT
            if re.findall('.*Used\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                memory_current = re.findall('.*Used\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                memory_current = memory_current[0]
                if re.findall('\d+', memory_current):
                    memory_current_list.append(float(memory_current))
                else:
                    memory_current = 0
                    memory_current_list.append(float(memory_current))

        #MEMORY STATUS
        for i in memory_current_list:
            if float(i)<21 :
                memory_status='Low'
                memory_status_list.append(str(memory_status))
            elif float(i)<81 :
                memory_status='Medium'
                memory_status_list.append(str(memory_status))
            else:
                memory_status='High'
                memory_status_list.append(str(memory_status))
        
        #THROUGHPUT SUMMARY TABLE
        #GET THROUGHPUT BITS
        read_file_list_throughputbits  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_throughputbits.append(line.strip())
            if 'Throughput(bits)' in line:
                read_file_logic_check = True
            count_read_file+=1

        throughput_bits_name_list = []
        throughput_bits_current_list = []
        throughput_bits_3h_list = []
        throughput_bits_24h_list = []
        throughput_bits_7d_list = []
        throughput_bits_30d_list = []
        throughput_bits_status_list = []

        for line in read_file_list_throughputbits:
            #THROUGHPUT BITS NAME
            if re.findall('^([S,I,O]\S+)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line):
                throughput_bits_name = re.findall('^([S,I,O]\S+)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line)
                throughput_bits_name = throughput_bits_name[0]
                throughput_bits_name_list.append(throughput_bits_name)
            
            #THROUGHPUT BITS 3 HOURS
            if re.findall('^[S,I,O]\S+\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                throughput_bits_3h = re.findall('^[S,I,O]\S+\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                throughput_bits_3h = throughput_bits_3h[0]
                throughput_bits_3h_list.append(throughput_bits_3h)

            #THROUGHPUT BITS 24 HOURS
            if re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                throughput_bits_24h = re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                throughput_bits_24h = throughput_bits_24h[0]
                throughput_bits_24h_list.append(throughput_bits_24h)
            
            #THROUGHPUT BITS 7 DAYS
            if re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                throughput_bits_7d = re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                throughput_bits_7d = throughput_bits_7d[0]
                throughput_bits_7d_list.append(throughput_bits_7d)
            
            #THROUGHPUT BITS 30 DAYS
            if re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                throughput_bits_30d = re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                throughput_bits_30d = throughput_bits_30d[0]
                throughput_bits_30d_list.append(throughput_bits_30d)

            #THROUGHPUT BITS CURRENT
            if re.findall('^[S,I,O]\S+\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                throughput_bits_current = re.findall('^[S,I,O]\S+\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                throughput_bits_current = throughput_bits_current[0]
                throughput_bits_current_list.append(throughput_bits_current)
                throughput_bits_status = 'Isi Manual'
                throughput_bits_status_list.append(throughput_bits_status)
        
        #GET THROUGHPUT SSL      
        read_file_list_throughputssl  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_throughputssl.append(line.strip())
            if 'SSL Transactions' in line:
                read_file_logic_check = True
            count_read_file+=1

        throughput_ssl_name_list = []
        throughput_ssl_current_list = []
        throughput_ssl_3h_list = []
        throughput_ssl_24h_list = []
        throughput_ssl_7d_list = []
        throughput_ssl_30d_list = []
        throughput_ssl_status_list = []
        for line in read_file_list_throughputssl:
            #THROUGHPUT SSL NAME
            if re.findall('^(SSL\s+\S+)\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+', line):
                throughput_ssl_name = re.findall('^(SSL\s+\S+)\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+', line)
                throughput_ssl_name = throughput_ssl_name[0]
                throughput_ssl_name_list.append(throughput_ssl_name)
            
            #THROUGHPUT SSL 3 HOURS
            if re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                throughput_ssl_3h = re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                throughput_ssl_3h = throughput_ssl_3h[0]
                throughput_ssl_3h_list.append(throughput_ssl_3h)

            #THROUGHPUT SSL 24 HOURS
            if re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                throughput_ssl_24h = re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                throughput_ssl_24h = throughput_ssl_24h[0]
                throughput_ssl_24h_list.append(throughput_ssl_24h)
            
            #THROUGHPUT SSL 7 DAYS
            if re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                throughput_ssl_7d = re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                throughput_ssl_7d = throughput_ssl_7d[0]
                throughput_ssl_7d_list.append(throughput_ssl_7d)
            
            #THROUGHPUT SSL 30 DAYS
            if re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                throughput_ssl_30d = re.findall('^SSL\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                throughput_ssl_30d = throughput_ssl_30d[0]
                throughput_ssl_30d_list.append(throughput_ssl_30d)

            #THROUGHPUT SSL CURRENT
            if re.findall('^SSL\s+\S+\s+(?!Current)(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                throughput_ssl_current = re.findall('^SSL\s+\S+\s+(?!Current)(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                throughput_ssl_current = throughput_ssl_current[0]
                throughput_ssl_current_list.append(throughput_ssl_current)
                throughput_ssl_status = 'Isi Manual'
                throughput_ssl_status_list.append(throughput_ssl_status)

        
        


        #GET THROUGHPUT PACKET  
        read_file_list_throughputpacket  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_throughputpacket.append(line.strip())
            if 'Throughput(packets)' in line:
                read_file_logic_check = True
            count_read_file+=1
        
        throughput_packet_name_list = []
        throughput_packet_current_list = []
        throughput_packet_3h_list = []
        throughput_packet_24h_list = []
        throughput_packet_7d_list = []
        throughput_packet_30d_list = []
        throughput_packet_status_list = []
        for line in read_file_list_throughputpacket:
            #THROUGHPUT PACKET NAME
            if re.findall('^([S,I,O]\S+)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line):
                throughput_packet_name = re.findall('^([S,I,O]\S+)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line)
                throughput_packet_name = throughput_packet_name[0]
                throughput_packet_name_list.append(throughput_packet_name)
            
            #THROUGHPUT PACKET 3 HOURS
            if re.findall('^[S,I,O]\S+\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                throughput_packet_3h = re.findall('^[S,I,O]\S+\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                throughput_packet_3h = throughput_packet_3h[0]
                throughput_packet_3h_list.append(throughput_packet_3h)

            #THROUGHPUT PACKET 24 HOURS
            if re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                throughput_packet_24h = re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                throughput_packet_24h = throughput_packet_24h[0]
                throughput_packet_24h_list.append(throughput_packet_24h)
            
            #THROUGHPUT PACKET 7 DAYS
            if re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                throughput_packet_7d = re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                throughput_packet_7d = throughput_packet_7d[0]
                throughput_packet_7d_list.append(throughput_packet_7d)
            
            #THROUGHPUT PACKET 30 DAYS
            if re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                throughput_packet_30d = re.findall('^[S,I,O]\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                throughput_packet_30d = throughput_packet_30d[0]
                throughput_packet_30d_list.append(throughput_packet_30d)

            #THROUGHPUT PACKET CURRENT
            if re.findall('^[S,I,O]\S+\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                throughput_packet_current = re.findall('^[S,I,O]\S+\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                throughput_packet_current = throughput_packet_current[0]
                throughput_packet_current_list.append(throughput_packet_current)
                throughput_packet_status = 'Isi Manual'
                throughput_packet_status_list.append(throughput_packet_status)

        #GET ACTIVE CONNECTIONS      
        read_file_list_connection  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_connection.append(line.strip())
            if 'Active Connections' in line:
                read_file_logic_check = True
            count_read_file+=1

        connection_name_list = []
        connection_current_list = []
        connection_3h_list = []
        connection_24h_list = []
        connection_7d_list = []
        connection_30d_list = []
        connection_status_list = []
        for line in read_file_list_connection:
            #ACTIVE CONNECTIONS NAME
            if re.findall('^(?!Active Connections)(?!-)(\S+)\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+', line):
                connection_name = re.findall('^(?!Active Connections)(?!-)(\S+)\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+', line)
                connection_name = connection_name[0]
                connection_name_list.append(connection_name)
            
            #ACTIVE CONNECTIONS 3 HOURS
            if re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                connection_3h = re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                connection_3h = connection_3h[0]
                connection_3h_list.append(connection_3h)

            #ACTIVE CONNECTIONS 24 HOURS
            if re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                connection_24h = re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                connection_24h = connection_24h[0]
                connection_24h_list.append(connection_24h)
            
            #ACTIVE CONNECTIONS 7 DAYS
            if re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                connection_7d = re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                connection_7d = connection_7d[0]
                connection_7d_list.append(connection_7d)
            
            #ACTIVE CONNECTIONS 30 DAYS
            if re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                connection_30d = re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                connection_30d = connection_30d[0]
                connection_30d_list.append(connection_30d)

            #ACTIVE CONNECTIONS CURRENT
            if re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                connection_current = re.findall('^(?!Active Connections)(?!-)\S+\s+(?!Current)(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                connection_current = connection_current[0]
                connection_current_list.append(connection_current)
                connection_status = 'Isi Manual'
                connection_status_list.append(connection_status)
        
        #GET NEW CONNECTIONS
        read_file_list_newconnection  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_newconnection.append(line.strip())
            if 'Total New Connections' in line:
                read_file_logic_check = True
            count_read_file+=1
        
        newconnection_name_list = []
        newconnection_current_list = []
        newconnection_3h_list = []
        newconnection_24h_list = []
        newconnection_7d_list = []
        newconnection_30d_list = []
        newconnection_status_list = [] 
        for line in read_file_list_newconnection:
            #NEW CONNECTIONS NAME
            if re.findall('^((?!Total)(?!-)\S+\s+(?!Current)\S+)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line):
                newconnection_name = re.findall('^((?!Total)(?!-)\S+\s+(?!Current)\S+)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+', line)
                newconnection_name = newconnection_name[0]
                newconnection_name_list.append(newconnection_name)
            
            #NEW CONNECTIONS 3 HOURS
            if re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                newconnection_3h = re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                newconnection_3h = newconnection_3h[0]
                newconnection_3h_list.append(newconnection_3h)

            #NEW CONNECTIONS 24 HOURS
            if re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                newconnection_24h = re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                newconnection_24h = newconnection_24h[0]
                newconnection_24h_list.append(newconnection_24h)
            
            #NEW CONNECTIONS 7 DAYS
            if re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                newconnection_7d = re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                newconnection_7d = newconnection_7d[0]
                newconnection_7d_list.append(newconnection_7d)
            
            #NEW CONNECTIONS 30 DAYS
            if re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                newconnection_30d = re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                newconnection_30d = newconnection_30d[0]
                newconnection_30d_list.append(newconnection_30d)

            #NEW CONNECTIONS CURRENT
            if re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                newconnection_current = re.findall('^(?!Total)(?!-)\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                newconnection_current = newconnection_current[0]
                newconnection_current_list.append(newconnection_current)
                newconnection_status = 'Isi Manual'
                newconnection_status_list.append(newconnection_status)

        #GET HTTP REQUESTS       
        read_file_list_httprequests  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_httprequests.append(line.strip())
            if 'HTTP Requests(/sec)' in line:
                read_file_logic_check = True
            count_read_file+=1
        
        httprequests_name_list = []
        httprequests_current_list = []
        httprequests_3h_list = []
        httprequests_24h_list = []
        httprequests_7d_list = []
        httprequests_30d_list = []
        httprequests_status_list = []
        for line in read_file_list_httprequests:
            #HTTP REQUESTS NAME
            if re.findall('^(?!-)(HTTP\s+\S+)\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+', line):
                httprequests_name = re.findall('^(?!-)(HTTP\s+\S+)\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+\S+', line)
                httprequests_name = httprequests_name[0]
                httprequests_name_list.append(httprequests_name)
            
            #HTTP REQUESTS 3 HOURS
            if re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                httprequests_3h = re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                httprequests_3h = httprequests_3h[0]
                httprequests_3h_list.append(httprequests_3h)

            #HTTP REQUESTS 24 HOURS
            if re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                httprequests_24h = re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                httprequests_24h = httprequests_24h[0]
                httprequests_24h_list.append(httprequests_24h)
            
            #HTTP REQUESTS 7 DAYS
            if re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                httprequests_7d = re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                httprequests_7d = httprequests_7d[0]
                httprequests_7d_list.append(httprequests_7d)
            
            #HTTP REQUESTS 30 DAYS
            if re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                httprequests_30d = re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                httprequests_30d = httprequests_30d[0]
                httprequests_30d_list.append(httprequests_30d)

            #HTTP REQUESTS CURRENT
            if re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                httprequests_current = re.findall('^(?!-)HTTP\s+\S+\s+(?!Current)(\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                httprequests_current = httprequests_current[0]
                httprequests_current_list.append(httprequests_current)
                httprequests_status = 'Isi Manual'
                httprequests_status_list.append(httprequests_status)   
        
        #GET LTM
        ltm_tipe_list = []
        ltm_name_list = []
        availability_list = []
        state_list = []
        for line in read_file_list:
            #LTM TIPE
            if re.findall('^Ltm::(Virtual\s+Server|Pool|Node):\s+\S+\/.*', line):
                ltm_tipe = re.findall('^Ltm::(Virtual\s+Server|Pool|Node):\s+\S+\/.*', line)
                ltm_tipe = ltm_tipe[0]
                ltm_tipe_list.append(ltm_tipe)
                
            
            #LTM NAME
            if re.findall('^Ltm::(Virtual\s+Server|Pool|Node):\s+\S+\/(.*)', line):
                ltm_name = re.match('^Ltm::(Virtual\s+Server|Pool|Node):\s+\S+\/(.*)', line)
                ltm_name = ltm_name.group(2)
                ltm_name_list.append(ltm_name)
            
            #LTM AVAILABILITY
            if re.findall('^\s+Availability\s+:\s+(\S+)', line):
                availability = re.findall('^\s+Availability\s+:\s+(\S+)', line)
                availability = availability[0]
                availability_list.append(availability)

            #LTM STATE
            if re.findall('^\s+State\s+:\s+(\S+)', line):
                state = re.findall('^\s+State\s+:\s+(\S+)', line)
                state = state[0]
                state_list.append(state)
        
        #GET HA
        device_list = []
        mgmt_ip_list = []
        configsync_ip_list = []
        ha_state_list = []
        ha_status_list = []
        for line in read_file_list:
            #HA DEVICE
            if re.findall('^CentMgmt::Device:\s+(\S+)', line):
                device = re.findall('^CentMgmt::Device:\s+(\S+)', line)
                device = device[0]
                device_list.append(device)
                print(device)
                print("device")
            
            #HA MANAGEMENT IP
            if re.findall('^Mgmt\s+IP\s+(\d+.\d+.\d+.\d+)', line):
                mgmt_ip = re.findall('^Mgmt\s+IP\s+(\d+.\d+.\d+.\d+)', line)
                mgmt_ip = mgmt_ip[0]
                mgmt_ip_list.append(mgmt_ip)
                print(mgmt_ip)
                print("mgmt_ip")

        for line in read_file_list:  
            #HA CONFIG SYNC ADA
            if re.findall('^Configsync\s+IP\s+(\d+.\d+.\d+.\d+)', line):
                configsync_ip = re.findall('^Configsync\s+IP\s+(\d+.\d+.\d+.\d+)', line)
                configsync_ip = configsync_ip[0]
                configsync_ip_list.append(configsync_ip)
                print(configsync_ip)
                print("configsync_ip")

            #HA CONFIG SYNC ADA
            elif re.findall('^Configsync\s+IP\s+(..)', line):
                configsync_ip = 'None'
                configsync_ip_list.append(configsync_ip)
                print(configsync_ip)
                print("configsync_ip jika ada")

        for line in read_file_list:
            #HA STATE LIST
            if re.findall('^Device\s+HA\s+State\s+(\S+)', line):
                ha_state = re.findall('^Device\s+HA\s+State\s+(\S+)', line)
                ha_state = ha_state[0]
                ha_state_list.append(ha_state)
                print(ha_state)
                print("ha_state")

                #HA STATUS LIST
                ha_status = 'Isi Manual'
                ha_status_list.append(ha_status)
                print(ha_status)
                print("ha_status")
                
            
            #HA STATE LIST GK ADA
            elif re.findall('^Device\s+Status\s+(.*)', line):
                ha_state = 'None'
                ha_state_list.append(ha_state)
                print(ha_state)
                print("ha_state")

                #HA STATUS LIST
                ha_status = 'Isi Manual'
                ha_status_list.append(ha_status)
                print(ha_status)
                print("ha_status")

        #GET SYNC HA
        for line in read_file_list:
            #SYNC STATUS HA
            if re.findall('^Status\s+(.*)', line):
                sync_status = re.findall('^Status\s+(.*)', line)
                sync_status = sync_status[0]
                print(sync_status)
                print("sync_status")
                break
            
        #RECOMENDATION ACTION HA
        for line in read_file_list:
            #JIKA IN SYNC
            if re.findall('^\s+(HA|datasync-failover)\s+\(In\s+Sync\):\s+(.*)', line):
                action = re.match('^\s+(HA|datasync-failover)\s+\(In\s+Sync\):\s+(.*)', line)
                action = action.group(2)
                print(action)
                print("action")
                break
            elif re.findall('(.*)\s+\(In\s+Sync\):\s+(.*)', line):
                action = re.match('(.*)\s+\(In\s+Sync\):\s+(.*)', line)
                action = action.group(2)
                print(action)
                print("action")
                break
            #JIKA PENDING
            elif re.findall('^\s+\-\s+Recommended\s+action:\s+(.*)', line):
                action = re.findall('^\s+\-\s+Recommended\s+action:\s+(.*)', line)
                action = action[0]
                print(action)
                print("action")
                break

            #JIKA STANDALONE
            elif re.findall('^\s+Optional\s+action:\s+(.*)', line):
                action = re.findall('^\s+Optional\s+action:\s+(.*)', line)
                action = action[0]
                print(action)
                print("action")
                break
        
        #GET TRAFFIC GROUP
        traffic_group_list = []
        device_traffic_list = []
        traffic_status_list = []
        next_active_list = []
        previous_active_list = []
        active_reason_list = []
        for line in read_file_list:
            #TRAFFIC GROUP
            if re.findall('^(traffic-group-\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line):
                traffic_group = re.findall('^(traffic-group-\S+)\s+\S+\s+\S+\s+\S+\s+\S+', line)
                traffic_group = traffic_group[0]
                traffic_group_list.append(traffic_group)
            
            #DEVICE TRAFFIC
            if re.findall('^traffic-group-\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line):
                device_traffic = re.findall('^traffic-group-\S+\s+(\S+)\s+\S+\s+\S+\s+\S+', line)
                device_traffic = device_traffic[0]
                device_traffic_list.append(device_traffic)
            
            #TRAFFIC STATUS
            if re.findall('^traffic-group-\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line):
                traffic_status = re.findall('^traffic-group-\S+\s+\S+\s+(\S+)\s+\S+\s+\S+', line)
                traffic_status = traffic_status[0]
                traffic_status_list.append(traffic_status)

            #NEXT ACTIVE
            if re.findall('^traffic-group-\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line):
                next_active = re.findall('^traffic-group-\S+\s+\S+\s+\S+\s+(\S+)\s+\S+', line)
                next_active = next_active[0]
                next_active_list.append(next_active)
            
            #PREVIOUS ACTIVE
            if re.findall('^traffic-group-\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                previous_active = re.findall('^traffic-group-\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                previous_active = previous_active[0]
                previous_active_list.append(previous_active)
            
            #ACTIVE REASON
            if re.findall('^traffic-group-\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line):
                active_reason = re.findall('^traffic-group-\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)', line)
                active_reason = active_reason[0]
                active_reason_list.append(active_reason)
        
        #GET CERTIFICATE
        cert_name_list = []
        cn_list = []
        expired_date_list = []
        cert_status_list = []
        convert_expired_date_list =[]
        for line in read_file_list:
            #CERTIFICATE NAME
            if re.findall('^sys\s+file\s+ssl-cert\s+Common\/(.*)\s+\{', line):
                cert_name = re.findall('^sys\s+file\s+ssl-cert\s+Common\/(.*)\s+\{', line)
                cert_name = cert_name[0]
                cert_name_list.append(cert_name)
            
            #CERTIFICATE CN
            if re.findall('^\s+subject\s+\S+(CN.*)', line):
                cn = re.findall('^\s+subject\s+\S+(CN.*)', line)
                cn = cn[0]
                cn_list.append(cn)
            
            #CERTIFICATE EXPIRED
            if re.findall('^\s+expiration-string\s+.(.*).', line):
                expired_date = re.findall('^\s+expiration-string\s+.(.*).', line)
                expired_date = expired_date[0]
                expired_date_list.append(expired_date)

                #Split String Spasi
                expired_date = expired_date.split()
                expired_date = (expired_date[0]+"-"+expired_date[1]+"-"+expired_date[3])

                #Convert String to Date
                expired_date = datetime.datetime.strptime(expired_date, "%b-%d-%Y")
                expired_date = expired_date.date()
                convert_expired_date_list.append(expired_date)
        
        #GET DATE NOW
        date_now = datetime.datetime.now()
        date_now_convert = date_now.date()

        #CERTIFICATE STATUS
        for date in convert_expired_date_list:
            if date <= date_now_convert:
                cert_status = "Expired"
                cert_status_list.append(cert_status)
            else:
                cert_status = "Not Expired"
                cert_status_list.append(cert_status)
        
        #GET LICENSE
        #LICENSE VERSION
        for line in read_file_list:
            if re.findall('^Licensed\s+Version\s+(.*)', line):
                lic_version = re.findall('^Licensed\s+Version\s+(.*)', line)
                lic_version = lic_version[0]
                break
        
        #REGISTER KEY
        for line in read_file_list:
            if re.findall('^Registration\s+key\s+(.*)', line):
                register_key = re.findall('^Registration\s+key\s+(.*)', line)
                register_key = register_key[0]
                break
        
        #LICENSED ON
        for line in read_file_list:
            if re.findall('^Licensed\s+On\s+(.*)', line):
                lic_on = re.findall('^Licensed\s+On\s+(.*)', line)
                lic_on = lic_on[0]

                #LICENSE STATUS
                lic_status = 'Isi Manual'
                break

        #SERVICE CHECK DATE
        for line in read_file_list:
            if re.findall('^Service\s+Check\s+Date\s+(.*)', line):
                svc_date = re.findall('^Service\s+Check\s+Date\s+(.*)', line)
                svc_date = svc_date[0]
                break

        #GET ACTIVE MODULES
        read_file_list_active_module  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_active_module.append(line.strip())
            if 'Active Modules' in line:
                read_file_logic_check = True
            count_read_file+=1


        for module in read_file_list_active_module:
            active_module = '\n'.join(read_file_list_active_module)
        # print(active_module)

        # print(lic_version)
        # print(register_key)
        # print(lic_on)
        # print(svc_date)
        # print(lic_status)

        # input("STOP")
        
        device_class_name = self.__class__.__name__
        file_name = self.file

        #TIPE DEVICES SUMMARY TABLE
        tipe_list = []
        if 'BIG-IP' in model:
            tipe = 'BIG-IP F5'
            tipe_list.append(tipe)
        else:
            tipe = 'Other'
            tipe_list.append(tipe)
        
        func_insert_dbreport_f5 = insert_dbreport_f5(
                devicename, model, iosversion, uptime, confreg, version, date_eos, date_last,
                tipe_list, list_serial_number, list_date,
                eos_date, eos_tipe, list_tipe,
                list_psu, list_psu_cond, list_fan, list_fan_cond_cp, list_temp, list_temp_cond,
                cpu_current, cpu_3h, cpu_24h, cpu_7d, cpu_30d, cpu_status,
                memory_name_list, memory_current_list, memory_3h_list, memory_24h_list, memory_7d_list, memory_30d_list, memory_status_list,
                throughput_bits_name_list, throughput_bits_current_list, throughput_bits_3h_list, throughput_bits_24h_list, throughput_bits_7d_list, throughput_bits_30d_list, throughput_bits_status_list,
                throughput_ssl_name_list, throughput_ssl_current_list, throughput_ssl_3h_list, throughput_ssl_24h_list, throughput_ssl_7d_list, throughput_ssl_30d_list, throughput_ssl_status_list,
                throughput_packet_name_list, throughput_packet_current_list, throughput_packet_3h_list, throughput_packet_24h_list, throughput_packet_7d_list, throughput_packet_30d_list, throughput_packet_status_list,
                connection_name_list, connection_current_list, connection_3h_list, connection_24h_list, connection_7d_list, connection_30d_list, connection_status_list,
                newconnection_name_list, newconnection_current_list, newconnection_3h_list, newconnection_24h_list, newconnection_7d_list, newconnection_30d_list, newconnection_status_list,
                httprequests_name_list, httprequests_current_list, httprequests_3h_list, httprequests_24h_list, httprequests_7d_list, httprequests_30d_list, httprequests_status_list,
                ltm_tipe_list, ltm_name_list, availability_list, state_list,
                device_list, mgmt_ip_list, configsync_ip_list, ha_state_list, ha_status_list,
                traffic_group_list, device_traffic_list, traffic_status_list, next_active_list, previous_active_list, active_reason_list,
                sync_status, action,
                cert_name_list, cn_list, expired_date_list, cert_status_list,
                lic_version, register_key, active_module, lic_on, svc_date, lic_status,
                device_class_name,
                file_name
        )
        execute_insert_dbreport_f5 = func_insert_dbreport_f5.insert_dbreport_f5()
        
        