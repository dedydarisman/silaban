import re
import requests
from netoprmgr.script.insert_dbreport_rs import insert_dbreport_rs
from netoprmgr.script.eosl_ip import eosl_ip


class cisco_2801:
    def __init__(self,file):
        #variable constructor
        self.file = file
        #read all things in file
        try:
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
        except:
            try:
                read_file = open(self.file, 'r', encoding='latin-1')
                read_file_list = read_file.readlines()
            except:
                print('Error Codec!!!')
                pass
        for line in read_file_list:
            #get sn
            if re.findall('PID:.*,\s+SN:\s+(\S+)',line):
                sn = re.findall('PID:.*,\s+SN:\s+(\S+)',line)
                sn = sn[0]
                break
            else:
                sn = 'Tidak di temukan'
        for line in read_file_list:
            #get ip
            if re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line):
                ip = re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line)
                ip = ip[0]
                break
            else:
                ip = 'Tidak di temukan'
        for line in read_file_list:
            #SOFTWARE TABLE
            #get device name
            if re.findall('^hostname (.*)',line):
                devicename = re.findall('^hostname (.*)',line)
                devicename = devicename[0]
                break
            else:
                devicename = self.file
        for line in read_file_list:
            #get device model
            if re.findall('.*isco\s+(\S+).*with.*bytes',line):
                model = re.findall('.*isco\s+(\S+).*with.*bytes',line)
                model = model[0]
                break                
        for line in read_file_list:
            #get ios version
            if re.findall('^System image file is "(.*)"',line):
                iosversion = re.findall('^System image file is "(.*)"',line)
                iosversion = iosversion[0]
                break
        for line in read_file_list:
            #get uptime
            if re.findall(devicename+'\s+uptime is (.*)',line):
                uptime = re.findall(devicename+'\s+uptime is (.*)',line)
                uptime = uptime[0]
                break
        for line in read_file_list:
            #get configuration register
            if re.findall('^Configuration register is (.*)',line):
                confreg = re.findall('^Configuration register is (.*)',line)
                confreg = confreg[0]
                break
        for line in read_file_list:
            #SOFTWARE TABLE SUMMARY
            if re.findall('^.*Version (.*),',line):
                version = re.findall('^.*Version (.*),',line)
                version = version[0]
                break
        
        #Stack identification
        stack_break = False
        stack_cond = False
        stack_list = []
        for line in read_file_list:
            if re.findall('\d+\s+\d+\s+(WS\S+)\s+\S+\s+\S+',line):
                stack_break = True
                stack = re.findall('\d+\s+\d+\s+(WS\S+)\s+\S+\s+\S+',line)
                stack = stack[0]
                stack_list.append(stack)
            #break loop
            if stack_break == True and re.findall('.*#',line):
                break
        if len(stack_list) == 0:
            stack_cond = False
        else:
            stack_cond = True

        list_card = []
        list_serial_number = []
        list_hardware_description = []
        hardware_break = False
        for line in read_file_list:
            #HARDWARE
            #card PID
            if re.findall('^PID: (.*),.*,',line):
                hardware_break = True
                card = re.findall('^PID: (.*),.*,',line)
                card = card[0]
                card = card.strip()
                list_card.append(card)
            #card serial number
            if re.findall('^PID: .*,.*,.*SN: (.*)',line):
                serial_number = re.findall('^PID: .*,.*,.*SN: (.*)',line)
                serial_number = serial_number[0]
                list_serial_number.append(serial_number)
            #description
            if re.findall('.*DESCR:\s+"(.*)"',line):
                hardware_description = re.findall('.*DESCR:\s+"(.*)"',line)
                hardware_description = hardware_description[0]
                list_hardware_description.append(hardware_description)
            #break loop
            if hardware_break == True and re.findall('.*#',line):
                break
        #eos software table
        try:
            execute_eosl_ip = eosl_ip()
            resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/software/'+version)
            date = resp.json()
            if str(date) == "None":
                date_eos = 'Not Announced'
                date_last = 'Not Announced'
            else:
                date = date[0]
                date_eos = date["eos_maintenance"]
                date_last = date["last_day_of_support"]
        except:
            date_eos = 'Server EOSL Down'
            date_last = 'Server EOSL Down'

        #eos card table
        date_list = []
        for card in list_card:
            try:
                eosl_card = card
                eosl_card = re.findall('(\S+)',eosl_card)
                if len(eosl_card) == 0:
                    date = '-'
                else:
                    eosl_card = eosl_card[0]
                    execute_eosl_ip = eosl_ip()
                    resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/hardware/'+eosl_card)
                    date = resp.json()
                    if str(date) == "None":
                        date = 'Not Announced'
                    else:
                        date = date[0]
                        date = date["date"]
            except:
                date = 'Server EOSL Down'
            date_list.append(date)
        
        cpu_break = False
        for line in read_file_list:
            #CPU
            #cpu
            if re.findall('^CPU utilization for five seconds: (.*)%\/.*%;.*;',line):
                cpu_break = True
                total = re.findall('^CPU utilization for five seconds: (.*)%\/.*%;.*;',line)
                total = int(total[0])
                #print('cpu')
                #print(cpu)
            #cpu interrupt
            if re.findall('^CPU utilization for five seconds: .*\/(.*)%;.*;',line):
                interrupt = re.findall('^CPU utilization for five seconds: .*\/(.*)%;.*;',line)
                interrupt = interrupt[0]
                #cpu total
                process = int(total) - int(interrupt)
                #cpu status
                if total<21 :
                    status='Low'
                elif total<81 :
                    status='Medium'
                else:
                    status='High'
                total=str(total)
            #break loop
            if cpu_break == True and re.findall('.*#',line):
                break
        
        memory_break = False
        for line in read_file_list:
            #MEMORY
            #Memory Total
            if re.findall('^Processor Pool Total:(.*)Used:.*Free:.*',line):
                memory_break = True
                memory_total = re.findall('^Processor Pool Total:(.*)Used:.*Free:.*',line)
                memory_total = memory_total[0]
            #Memory Used
            if re.findall('^Processor Pool Total:.*Used:(.*)Free:.*',line):
                memory_used = re.findall('^Processor Pool Total:.*Used:(.*)Free:.*',line)
                memory_used = memory_used[0]
                #memory percentage
                memory_percentage = (int(memory_used)/int(memory_total))*100
                #memory status
                if float(memory_percentage)<21 :
                    memory_status='Low'
                elif float(memory_percentage)<81 :
                    memory_status='Medium'
                else:
                    memory_status='High'
                memory_percentage=re.findall('(^.{5})*',str(memory_percentage))
                utils=memory_percentage[0]
            #break loop
            if memory_break == True and re.findall('.*#',line):
                break
        #sorting memory
        list_memory = []
        list_memory_sorted = []
        memory_sorted_break = False
        memory_sorted_add_list = False
        for line in read_file_list:
            #make conditional statement to let program start append to list, and get ready to break loop
            if re.findall('.*PID\s+TTY\s+Allocated\s+Freed\s+Holding\s+Getbufs\s+Retbufs\s+Process',line):
                memory_sorted_break = True
                memory_sorted_add_list = True
            #append value to list
            if memory_sorted_break == True:
                if re.findall('.*PID\s+TTY\s+Allocated\s+Freed\s+Holding\s+Getbufs\s+Retbufs\s+Process',line):
                    pass
                else:
                    list_memory.append(line)
            #break loop
            if memory_sorted_break == True and re.findall('.*#',line):
                break
            elif memory_sorted_break == True and re.findall('^\s*$',line):
                break
        #create new list that only contain memory allocated and name application that using it
        for i in list_memory:
            try:
                
                
                sort_digit = re.findall('\d+\s+\d+\s+\d+\s+\d+\s+(\d+)\s+\d+\s+\d+\s+.*',i)
                sort_text =  re.findall('\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+(.*)',i)
                list_memory_sorted.append(sort_digit[0].strip()+' '+sort_text[0].strip())
            except:
                pass
        try:
            #sort memory with allocated as key
            list_memory_sorted.sort(reverse=True,key = lambda x: int(x.split()[0]))
            #print('Memory Top Three')
            topproc1 = re.findall('\d+\s+(.*)',list_memory_sorted[0])
            topproc2 = re.findall('\d+\s+(.*)',list_memory_sorted[1])
            topproc3 = re.findall('\d+\s+(.*)',list_memory_sorted[2])
            topproc = (topproc1[0]+'\n'+topproc2[0]+'\n'+topproc3[0])
            #print(memory_top_three)
        except:
            pass

        #sorting cpu
        list_cpu = []
        list_cpu_sorted = []
        cpu_sorted_break = False
        cpu_sorted_add_list = False
        for line in read_file_list:
            #make conditional statement to let program start append to list, and get ready to break loop
            if re.findall('.*PID\s+Runtime\S+\s+Invoked\s+uSecs\s+5Sec\s+1Min\s+5Min\s+TTY\s+Process',line):
                cpu_sorted_break = True
                cpu_sorted_add_list = True
            #append value to list
            if cpu_sorted_break == True:
                if re.findall('.*PID\s+Runtime\S+\s+Invoked\s+uSecs\s+5Sec\s+1Min\s+5Min\s+TTY\s+Process',line):
                    pass
                else:
                    list_cpu.append(line)
            #break loop
            if cpu_sorted_break == True and re.findall('.*#',line):
                break
            elif cpu_sorted_break == True and re.findall('^\s*$',line):
                break
        #create new list that only contain cpu allocated and name application that using it
        for i in list_cpu:
            try:
                
                
                sort_digit = re.findall('\d+\s+\d+\s+\d+\s+\d+\s+(\d+[.]\d+).*\d+[.]\d+.*\d+[.]\d+.*\d+\s+.*',i)
                sort_text =  re.findall('\d+\s+\d+\s+\d+\s+\d+\s+\d+[.]\d+.*\d+[.]\d+.*\d+[.]\d+.*\d+\s+(.*)',i)
                list_cpu_sorted.append(sort_digit[0].strip()+' '+sort_text[0].strip())
            except:
                pass
        try:
            #sort cpu with allocated as key
            list_cpu_sorted.sort(reverse=True,key = lambda x: float(x.split()[0]))
            #print('cpu Top Three')
            topcpu1 = re.findall('\d+\s+(.*)',list_cpu_sorted[0])
            topcpu2 = re.findall('\d+\s+(.*)',list_cpu_sorted[1])
            topcpu3 = re.findall('\d+\s+(.*)',list_cpu_sorted[2])
            topcpu = (topcpu1[0]+'\n'+topcpu2[0]+'\n'+topcpu3[0])
            #print(cpu_top_three)
        except:
            pass
        
        read_file_list_env  = []
        read_file_logic_check = False
        count_read_file = 0
        for enum, line in enumerate(read_file_list):
            if read_file_logic_check == True and 'show' in line:
                break
            if read_file_logic_check == True:
                read_file_list_env.append(line)
            if 'show env' in line and '%' not in read_file_list[count_read_file+1] and '%' not in read_file_list[count_read_file+2] and '!' not in read_file_list[count_read_file+1]:
                read_file_logic_check = True
            count_read_file+=1

        #get environment
        list_psu_capture = []
        list_fan = []
        list_fan_cond_cp = []
        list_temp = []
        list_temp_cond = []
        list_psu = []
        list_psu_cond = []
        psu_line_start = 0
        psu_line_end = 0
        count_line=0
        for i in read_file_list_env:
            if re.findall('.*(Fan\s+\d+)\s+.*',i):
                regex_fan = re.findall('.*(Fan\s+\d+)\s+.*',i)
                fan = regex_fan[0]
                fan = fan.strip()
                list_fan.append(fan)
                #print(fan)
            if re.findall('.*Fan\s+\d+\s+(.*)', i):
                regex_fan_cond = re.findall('.*Fan\s+\d+\s+(.*)', i)
                fan_cond = regex_fan_cond[0]
                fan_cond = fan_cond.strip()
                list_fan_cond_cp.append(fan_cond)
                #print(fan_cond)
            if re.findall('(.*temperature):\s+\d+\s+\S+\s+\S+',i):
                regex_temp = re.findall('(.*temperature):\s+\d+\s+\S+\s+\S+',i)
                temp = regex_temp[0]
                temp = temp.strip()
                list_temp.append(temp)
                #print(temp)
            if re.findall('.*temperature:\s+\d+\s+\S+\s+(\S+)', i):
                regex_temp_cond = re.findall('.*temperature:\s+\d+\s+\S+\s+(\S+)', i)
                temp_cond = regex_temp_cond[0]
                temp_cond = temp_cond.strip()
                list_temp_cond.append(temp_cond)
                #print(temp_cond)
            if re.findall('(.*Power Supply)\s+-\S+',i):
                regex_psu = re.findall('(.*Power Supply)\s+-\S+',i)
                psu = regex_psu[0]
                psu = psu.strip()
                list_psu.append(psu)
                #print(temp)
            if re.findall('.*Power Supply\s+-(\S+)', i):
                regex_psu_cond = re.findall('.*Power Supply\s+-(\S+)', i)
                psu_cond = regex_psu_cond[0]
                psu_cond = psu_cond.strip()
                list_psu_cond.append(psu_cond)
                #print(temp_cond)

        #conditional env if null
        if len(list_fan) == 0:
            list_fan = ['-']
        if len(list_fan_cond_cp) == 0:
            list_fan_cond_cp = ['-']
        if len(list_temp) == 0:
            list_temp = ['-']
        if len(list_temp_cond) == 0:
            list_temp_cond = ['-']
        if len(list_psu) == 0:
            list_psu = ['-']
        if len(list_psu_cond) == 0:
            list_psu_cond = ['-']
        if len(list_fan_cond_cp) == 0:
            list_psu_cond = ['-']

        #variable convert to insert database
        eos_card = []
        eos_date = []
        if stack_cond == True:
            for stack in stack_list:
                for enum, card in enumerate(list_card):
                    if stack in card:
                        eos_card.append(card)
                        eos_date.append(date_list[enum])
                        break
        else:
            eos_card.append(list_card[0])
            eos_date.append(date_list[0])
        device_class_name = self.__class__.__name__
        file_name = self.file

        #to tipe devices
        tipe_list = []
        for i in eos_card:
            if re.findall('^CISCO\S+',i):
                tipe = re.findall('^CISCO\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^ASR\S+',i):
                tipe = re.findall('^ASR\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^ISR\S+',i):
                tipe = re.findall('^ISR\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^WS-\S+',i):
                tipe = re.findall('^WS-\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^C\d+\S+',i):
                tipe = re.findall('^C\d+\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^SG\S+',i):
                tipe = re.findall('^SG\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^AIR-AP\S+',i):
                tipe = re.findall('^AIR-AP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-BR\S+',i):
                tipe = re.findall('^AIR-BR\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-CAP\S+',i):
                tipe = re.findall('^AIR-CAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-LAP\S+',i):
                tipe = re.findall('^AIR-LAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-SAP\S+',i):
                tipe = re.findall('^AIR-SAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-CT\S+',i):
                tipe = re.findall('^AIR-CT\S+',i)
                tipe = tipe[0]
                tipe = 'Wireless Controller (WLC)'
                tipe_list.append(tipe)
            elif re.findall('^ASA\S+',i):
                tipe = re.findall('^ASA\S+',i)
                tipe = tipe[0]
                tipe = 'Adaptive Security Appliance (ASA)'
                tipe_list.append(tipe)
            elif re.findall('^VG\S+',i):
                tipe = re.findall('^VG\S+',i)
                tipe = tipe[0]
                tipe = 'Voice Gateway (VG)'
                tipe_list.append(tipe)
            elif re.findall('^N\S+',i):
                tipe = re.findall('^N\S+',i)
                tipe = tipe[0]
                tipe = 'Nexus'
                tipe_list.append(tipe)
            else:
                tipe = 'Other'
                tipe_list.append(tipe)
            break
        #to database
        try:
            print(version)
        except:
            version = '-'
        try:
            print(devicename)
        except:
            devicename = '-'
        try:
            print(model)
        except:
            model = '-'
        try:
            print(iosversion)
        except:
            iosversion = '-'
        try:
            print(uptime)
        except:
            uptime = '-'
        try:
            print(confreg)
        except:
            confreg = '-'
        try:
            print(card)
        except:
            card = '-'
        try:
            print(list_serial_number)
        except:
            list_serial_number = '-'
        try:
            print(list_hardware_description)
        except:
            list_hardware_description = '-'
        try:
            print(date_list)
        except:
            date_list = '-'
        try:
            print(total)
        except:
            total = '-'
        try:
            print(process)
        except:
            process = '-'
        try:
            print(interrupt)
        except:
            interrupt = '-'
        try:
            print(topcpu)
        except:
            topcpu = '-'
        try:
            print(status)
        except:
            status = '-'
        try:
            print(utils)
        except:
            utils = '-'
        try:
            print(topproc)
        except:
            topproc = '-'
        try:
            print(memory_status)
        except:
            memory_status = '-'
        try:
            print(psu)
        except:
            psu = '-'
        try:
            print(list_psu)
        except:
            list_psu = '-'
        try:
            print(list_psu_cond)
        except:
            list_psu_cond = '-'
        try:
            print(fan)
        except:
            fan = '-'
        try:
            print(list_fan)
        except:
            list_fan = '-'
        try:
            print(list_fan_cond_cp)
        except:
            list_fan_cond_cp = '-'
        try:
            print(list_temp)
        except:
            list_temp = '-'
        try:
            print(list_temp_cond)
        except:
            list_temp_cond = '-'
        try:
            print(eos_date)
        except:
            eos_date = '-'
        try:
            print(device_class_name)
        except:
            device_class_name = '-'
        try:
            print(eos_card)
        except:
            eos_card = '-'
        try:
            print(file_name)
        except:
            file_name = '-'
        try:
            print(list_card)
        except:
            list_card = '-'
        try:
            print(tipe_list)
        except:
            tipe_list = '-'    

        func_insert_dbreport_rs = insert_dbreport_rs(
                version, devicename, model, iosversion, uptime, confreg, date_eos, date_last,
                card, list_serial_number, list_hardware_description, date_list,
                total, process, interrupt, topcpu, status,utils, topproc,
                memory_status, psu, list_psu, list_psu_cond, fan, list_fan, list_fan_cond_cp,
                list_temp, list_temp_cond, eos_date, device_class_name, eos_card, file_name, list_card, tipe_list,
                sn, ip
        )
        execute_insert_dbreport_rs = func_insert_dbreport_rs.insert_dbreport_rs()
        
        
        
        