import re
import requests
import sqlite3



class cisco_None:
    def __init__(self,file):
        #variable constructor
        self.file = file
        #read all things in file
        try:
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
        except:
            try:
                read_file = open(self.file, 'r', encoding='latin-1')
                read_file_list = read_file.readlines()
            except:
                print('Error Codec!!!')
                pass
        for line in read_file_list:
            #get sn
            if re.findall('PID:.*,\s+SN:\s+(\S+)',line):
                sn = re.findall('PID:.*,\s+SN:\s+(\S+)',line)
                sn = sn[0]
                break
            else:
                sn = 'Tidak di temukan'
        for line in read_file_list:
            #get ip
            if re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line):
                ip = re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line)
                ip = ip[0]
                break
            else:
                ip = 'Tidak di temukan'
        for line in read_file_list:
            #SOFTWARE TABLE
            #get device name
            if re.findall('^hostname (.*)',line):
                devicename = re.findall('^hostname (.*)',line)
                devicename = devicename[0]
                break
            else:
                devicename = self.file
                #print(devicename)
        for line in read_file_list:
            #get device model
            if re.findall('^.*isco\s+(\S+).*with.*bytes',line):
                model = re.findall('^.*isco\s+(\S+).*with.*bytes',line)
                model = model[0]
                break
            elif re.findall('^.Product\sNumber\s+:+\s+(\S+)',line):
                model = re.findall('^.Product\sNumber\s+:+\s+(\S+)',line)
                model = model[0]
                break
            elif re.findall('.*PID:\s+(AIR-CT\S+),',line):
                model = re.findall('.*PID:\s+(AIR-CT\S+),,',line)
                model = model[0]
                break
            elif re.findall('^Hardware:\s+(\S+),.*RAM.*CPU',line):
                model = re.findall('^Hardware:\s+(\S+),.*RAM.*CPU',line)
                model = model[0]
                break  
            elif re.findall('.*cisco\s+(Nexus.*hassis)',line):
                model = re.findall('.*cisco\s+(Nexus.*hassis)',line)
                model = model[0]
                break
            elif re.findall('^(ucm_.*)',line):
                model = re.findall('^(ucm_.*)',line)
                model = model[0]
                break

        #open db connection
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()
         #db eos
        try:
            cursor.execute('''INSERT INTO eosltable(model, card, date)
                    VALUES(?,?,?)''', (model, list_card[0], date,))
        except:
            cursor.execute('''INSERT INTO eosltable(model, card, date)
                    VALUES(?,?,?)''', (self.file+'-'+'error', self.file+'-'+'error', self.file+'-'+'error',))
       #LOG Checking
        try:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (devicename+'(template not found)', model+'(template not found)',self.__class__.__name__+'(template not found)',))
        except:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.file+'-'+'error'+'(template not found)', self.file+'-'+'error'+'(template not found)',self.file+'-'+'error'+'(template not found)',))
        db.commit()             
        db.close()

        
        