import re
import requests
from netoprmgr.script.insert_dbreport_ucm import insert_dbreport_ucm
from netoprmgr.script.eosl_ip import eosl_ip
import datetime
import numpy as np

class cisco_CUC_SUB:
    def __init__(self,file):
        #variable constructor
        self.file = file
        #read all things in file
        try:
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
        except:
            try:
                read_file = open(self.file, 'r', encoding='latin-1')
                read_file_list = read_file.readlines()
            except:
                print('Error Codec!!!')
                pass
        for line in read_file_list:
            #get sn
            if re.findall('PID:.*,\s+SN:\s+(\S+)',line):
                sn = re.findall('PID:.*,\s+SN:\s+(\S+)',line)
                sn = sn[0]
                break
            else:
                sn = 'Tidak di temukan'
        for line in read_file_list:
            #get ip
            if re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line):
                ip = re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line)
                ip = ip[0]
                break
            else:
                ip = 'Tidak di temukan'
        
        #HARDWARE TABLE
        for line in read_file_list:
            #Device Name
            if re.findall('^Host\s+Name\s+:\s+(.*)',line):
                devicename = re.findall('^Host\s+Name\s+:\s+(.*)',line)
                devicename = devicename[0]
                # print(devicename)
                break
            else:
                devicename = self.file

        #Device Type
        device_type = "VMware"

        #Hardware Type
        hardware_type = ""

        #Serial Number
        for line in read_file_list:
            #Device Name
            if re.findall('^Serial\s+Number\s+:\s+(.*)',line):
                sn = re.findall('^Serial\s+Number\s+:\s+(.*)',line)
                sn = sn[0]
                break

        #End of Support Hardware
        eos_hardware = ""

        #SOFTWARE TABLE      
        #Role
        model = "Cisco Unity Connection Subscriber"

        #Confreg
        model = "Cisco Unity Connection Subscriber"
        confreg = "-"

        for line in read_file_list:
            #OS Version
            if re.findall('^Product\s+Ver\s+:\s+(.*)',line):
                iosversion = re.findall('^Product\s+Ver\s+:\s+(.*)',line)
                iosversion = iosversion[0]
                version = iosversion
                # print(osversion)
                break
        
        for line in read_file_list:
            #Uptime (Days)
            if re.findall('.*up\s+(\d+\s+\S+),',line):
                uptime = re.findall('.*up\s+(\d+\s+\S+),',line)
                uptime = uptime[0]
                # print(uptime)
                break

        #End of Support Software
        try:
            execute_eosl_ip = eosl_ip()
            resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/software/'+version)
            date = resp.json()
            if str(date) == "None":
                date_eos = 'Not Announced'
                date_last = 'Not Announced'
            else:
                date = date[0]
                date_eos = date["eos_maintenance"]
                date_last = date["last_day_of_support"]
        except:
            date_eos = 'Server EOSL Down'
            date_last = 'Server EOSL Down'
        
        #CPU SUMMARY TABLE
        #CPU Utilization (%)
        for line in read_file_list:
            #idle
            if re.findall('^CPU\s+Idle:\s+(\S+)%',line):
                idle = re.findall('^CPU\s+Idle:\s+(\S+)%',line)
                idle = float(idle[0])

            if re.findall('^CPU\s+Idle:\s+\S+%\s+System:\s+(\S+)%\s+User:\s+\S+%',line):
                system_cpu = re.findall('^CPU\s+Idle:\s+\S+%\s+System:\s+(\S+)%\s+User:\s+\S+%',line)
                system_cpu = float(system_cpu[0])
            
            if re.findall('^CPU\s+Idle:\s+\S+%\s+System:\s+\S+%\s+User:\s+(\S+)%',line):
                user_cpu = re.findall('^CPU\s+Idle:\s+\S+%\s+System:\s+\S+%\s+User:\s+(\S+)%',line)
                user_cpu = float(user_cpu[0])
            
                # print(idle)
                cpu = system_cpu + user_cpu
                # print(cpu)
                
                #cpu status
                if cpu<21 :
                    cpu_status='Low'
                elif cpu<81 :
                    cpu_status='Medium'
                else:
                    cpu_status='High'
                cpu_status = str(cpu_status)
                # print(cpu_status)
                break

        #Top Process CPU
        start_line_cpu = False
        line_cpu_list = []
        top_cpu_list = []
        for line in read_file_list:
            if 'show process load cpu' in line:
                start_line_cpu = True
            if 'show process load memory' in line:
                break
            if start_line_cpu == True:
                line_cpu_list.append(line)

        for line_cpu in line_cpu_list:
            #Top CPU
            try:
                sort_digit = re.findall('\d+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\d+.\d+)\s+\d+.\d+\s+\S+\s+.*',line_cpu)
                sort_digit = sort_digit[0]
                sort_digit_float = float(sort_digit)*100
                sort_digit_int = int(sort_digit_float)
                sort_text =  re.findall('\d+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\d+.\d+\s+\d+.\d+\s+\S+\s+(.*)',line_cpu)
                top_cpu_list.append(str(sort_digit_int)+' '+sort_text[0].strip())
            except:
                pass

        #sort cpu with allocated as key
        top_cpu_list.sort(reverse=True,key = lambda x: int(x.split()[0]))
        topproc1 = re.findall('\d+\s+(.*)',top_cpu_list[0])
        topproc2 = re.findall('\d+\s+(.*)',top_cpu_list[1])
        topproc3 = re.findall('\d+\s+(.*)',top_cpu_list[2])
        top_cpu = (topproc1[0]+'\n'+topproc2[0]+'\n'+topproc3[0])
        
        #MEMORY SUMMARY TABLE
        #Memory Utilization (%)
        start_line_memory = False
        line_memory_list = []
        top_memory_list = []
        for line in read_file_list:
            if 'show process load memory' in line:
                start_line_memory = True
            if 'utils disaster_recovery history backup' in line:
                break
            if start_line_memory == True:
                line_memory_list.append(line)

        for line_memory in line_memory_list:
            #Memory Total
            if re.findall('^Mem:\s+(\d+)k\s+total,\s+\d+k\s+used,',line_memory):
                mem_total = re.findall('^Mem:\s+(\d+)k\s+total,\s+\d+k\s+used,',line_memory)
                mem_total = int(mem_total[0])
            elif re.findall('^KiB\s+Mem\s+:\s+(\d+)\s+total,\s+\d+\s+free,\s+\d+\s+used,',line_memory):
                mem_total = re.findall('^KiB\s+Mem\s+:\s+(\d+)\s+total,\s+\d+\s+free,\s+\d+\s+used,',line_memory)
                mem_total = int(mem_total[0])
            
            if re.findall('^Swap:\s+(\d+)k\s+total,\s+\d+k\s+used,',line_memory):
                swap_total = re.findall('^Swap:\s+(\d+)k\s+total,\s+\d+k\s+used,',line_memory)
                swap_total = int(swap_total[0])
            elif re.findall('^KiB\s+Swap:\s+(\d+)\s+total,\s+\d+\s+free,\s+\d+\s+used',line_memory):
                swap_total = re.findall('^KiB\s+Swap:\s+(\d+)\s+total,\s+\d+\s+free,\s+\d+\s+used',line_memory)
                swap_total = int(swap_total[0])
                
            #Memory Used
            if re.findall('^Mem:\s+\d+k\s+total,\s+(\d+)k\s+used,',line_memory):
                mem_used = re.findall('^Mem:\s+\d+k\s+total,\s+(\d+)k\s+used,',line_memory)
                mem_used = int(mem_used[0])
            elif re.findall('^KiB\s+Mem\s+:\s+\d+\s+total,\s+\d+\s+free,\s+(\d+)\s+used,',line_memory):
                mem_used = re.findall('^KiB\s+Mem\s+:\s+\d+\s+total,\s+\d+\s+free,\s+(\d+)\s+used,',line_memory)
                mem_used = int(mem_used[0])
            
            if re.findall('^Swap:\s+\d+k\s+total,\s+(\d+)k\s+used,',line_memory):
                swap_used = re.findall('^Swap:\s+\d+k\s+total,\s+(\d+)k\s+used,',line_memory)
                swap_used = int(swap_used[0])
            elif re.findall('^KiB\s+Swap:\s+\d+\s+total,\s+\d+\s+free,\s+(\d+)\s+used',line_memory):
                swap_used = re.findall('^KiB\s+Swap:\s+\d+\s+total,\s+\d+\s+free,\s+(\d+)\s+used',line_memory)
                swap_used = int(swap_used[0])
            
            #Top Memory
            try:
                sort_digit = re.findall('\d+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\d+.\d+\s+(\d+.\d+)\s+\S+\s+.*',line_memory)
                sort_digit = sort_digit[0]
                sort_digit_float = float(sort_digit)*100
                sort_digit_int = int(sort_digit_float)
                sort_text =  re.findall('\d+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\d+.\d+\s+\d+.\d+\s+\S+\s+(.*)',line_memory)
                top_memory_list.append(str(sort_digit_int)+' '+sort_text[0].strip())
            except:
                pass

        #sort memory with allocated as key
        top_memory_list.sort(reverse=True,key = lambda x: int(x.split()[0]))
        topproc1 = re.findall('\d+\s+(.*)',top_memory_list[0])
        topproc2 = re.findall('\d+\s+(.*)',top_memory_list[1])
        topproc3 = re.findall('\d+\s+(.*)',top_memory_list[2])
        top_memory = (topproc1[0]+'\n'+topproc2[0]+'\n'+topproc3[0])
        
        memory_total = (mem_total + swap_total)
        memory_used = (mem_used + swap_used)
        memory = float(memory_used/memory_total)*100

        if float(memory)<21 :
            memory_status='Low'
        elif float(memory)<81 :
            memory_status='Medium'
        else:
            memory_status='High'
            
        memory_status = str(memory_status)
        
        #DISK SUMMARY TABLE
        disk_name_list = []
        disk_total_list = []
        disk_free_list = []
        disk_used_list = []
        disk_percentage_list = []
        disk_status = []
        for line in read_file_list:
            if re.findall('^Disk.\S+\s+\d+K\s+\d+K\s+\d+K\s+[(]\d+%[)]',line):
                #Disk Name
                if re.findall('^Disk.(\S+)',line):
                    disk_name = re.findall('^Disk.(\S+)',line)
                    disk_name = disk_name[0]
                    disk_name_list.append(disk_name)

                #Disk Total
                if re.findall('^Disk.\S+\s+(\d+K)',line):
                    disk_total = re.findall('^Disk.\S+\s+(\d+K)',line)
                    disk_total = disk_total[0]
                    disk_total_list.append(disk_total)

                #Disk Free
                if re.findall('^Disk.\S+\s+\d+K\s+(\d+K)',line):
                    disk_free = re.findall('^Disk.\S+\s+\d+K\s+(\d+K)',line)
                    disk_free = disk_free[0]
                    disk_free_list.append(disk_free)

                #Disk Used
                if re.findall('^Disk.\S+\s+\d+K\s+\d+K\s+(\d+K)',line):
                    disk_used = re.findall('^Disk.\S+\s+\d+K\s+\d+K\s+(\d+K)',line)
                    disk_used = disk_used[0]
                    disk_used_list.append(disk_used)

                #Disk Percentage
                if re.findall('^Disk.\S+\s+.*[(](\d+)%[)]',line):
                    disk_percentage = re.findall('^Disk.\S+\s+.*[(](\d+)%[)]',line)
                    disk_percentage = disk_percentage[0]
                    disk_percentage_list.append(disk_percentage)

        #Disk Status    
        for status in disk_percentage_list:
            if int(status)<31 :
                status='Low'
                disk_status.append(status)
            elif int(status)<91 :
                status='Medium'
                disk_status.append(status)
            else:
                status='High'
                disk_status.append(status)
        
        #BACKUP SUMMARY TABLE
        status_backup = "No last status backup"
        type_backup = "No last type backup"
        last_date_backup = "No last date backup"
        fitur_backup = "No last fitur backup"
        failed_backup = "No last failed backup"   
        for line in read_file_list:
            #Last Succesful Scheduled Backup 
            if re.findall('(^\d+.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+\S+\s+.*)',line):
                all_status = re.findall('(^\d+.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+\S+\s+.*)',line)
                all_status = all_status[0]
            
                if re.findall('.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+\S+\s+(\S+\s+-\s+\S+)',all_status):
                    str_1 = re.findall('.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+\S+\s+(\S+\s+-\s+\S+)',all_status)
                    str_1 = str_1[0]

                    if re.findall('[\d+,.,-]',str_1):
                        data = re.findall('[\d+,.,-]',str_1)
                        dash_checker = False
                        new_str = []
                        for my_string in data:
                            if dash_checker == True:
                                new_str.append(my_string)
                            elif dash_checker == False:
                                try:
                                    int(my_string)
                                    new_str.append(my_string)
                                    dash_checker = True
                                except:
                                    pass
                        strver = (("").join(new_str))

                    stringall = re.sub(strver, ' '+strver, all_status)
                    stringdesk = re.sub(' - ', '-', stringall)
                    stringstrip = re.sub('---$', ' ---', stringdesk)
                    newstring = ' '.join(stringstrip.split())

                    newstring = newstring.split()
                    newstring = np.array(newstring)
                    
                    index_fitur = np.array([11])
                    index_failed = np.array([12])

                    fitur_backup = (newstring[index_fitur])
                    fitur_backup = fitur_backup[0]
                    fitur_backup = re.sub(',', '\n', fitur_backup)
                    
                    if index_failed == len(newstring):
                        failed_backup = '---'
                    else:
                        failed_backup = (newstring[index_failed])
                        failed_backup = failed_backup[0]
                        failed_backup = re.sub(',', '\n', failed_backup)


                else:
                    fitur_backup = "No last fitur backup"
                    failed_backup = "No last failed backup"
                
                if re.findall('^\d+.*.tar\s+\S+\s+(\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+)\s+\S+\s+.*',all_status):
                    last_date_backup = re.findall('^\d+.*.tar\s+\S+\s+(\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+)\s+\S+\s+.*',all_status)
                    last_date_backup = last_date_backup[0]

                else:
                    last_date_backup = "No last date backup"
                
                if re.findall('^\d+.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+(\S+)\s+.*',all_status):
                    status_backup = re.findall('^\d+.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+(\S+)\s+.*',all_status)
                    status_backup = status_backup[0]
                
                else:
                    status_backup = "No last status backup"

                if 'SCHEDULED' in all_status:
                    type_backup = "SCHEDULED"

                elif 'MANUAL' in all_status:
                    type_backup = "MANUAL"

                else:
                    type_backup = "No last type backup"

        list_all_status_backup = []
        schedule_backup = "Not Enabled"
        for line in read_file_list:
            #Last Succesful Scheduled Backup 
            if re.findall('(^\d+.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+\S+\s+.*)',line):
                all_status_backup = re.findall('(^\d+.*.tar\s+\S+\s+\S+\s+\S+\s+\d+\s+\d+:\d+:\d+\s+\S+\s+\d+\s+\S+\s+.*)',line)
                all_status_backup = all_status_backup[0]
                list_all_status_backup.append(all_status_backup)
        
        if len(list_all_status_backup) == 0:
            pass

        else:
            strbackup = '\n'
            data_backup = strbackup.join(list_all_status_backup)
            
            if 'SCHEDULED' in data_backup:
                schedule_backup = "Enabled"

        #CERTIFICATE SUMMARY TABLE
        cert_name_list = []
        from_date_list = []
        to_date_list = []
        cert_method_list = []
        cert_status_list = []
        for line in read_file_list:
            #Certificate Name
            if re.findall('(.*.pem$)',line):
                certificate_name = re.findall('(.*.pem$)',line)
                certificate_name = certificate_name[0]
                cert_name_list.append(certificate_name)
        
            #From Date
            if re.findall('From:\s+(.*)',line):
                from_date = re.findall('From:\s+(.*)',line)
                from_date = from_date[0]
                #Split String Spasi
                from_date = from_date.split()
                from_date = (from_date[1]+"-"+from_date[2]+"-"+from_date[5])
                #Convert String to Date
                from_date = datetime.datetime.strptime(from_date, "%b-%d-%Y")
                from_date = from_date.date()
                # print(to_date)
                #Convert Date to String
                from_date_str = from_date.strftime("%B %d, %Y")
                # print(to_date_str)
                from_date_list.append(from_date_str)
        
            #To Date
            if re.findall('To:\s+(.*)',line):
                to_date = re.findall('To:\s+(.*)',line)
                to_date = to_date[0]
                #Split String Spasi
                to_date = to_date.split()
                to_date = (to_date[1]+"-"+to_date[2]+"-"+to_date[5])
                #Convert String to Date
                to_date = datetime.datetime.strptime(to_date, "%b-%d-%Y")
                to_date = to_date.date()
                # print(to_date)
                #Convert Date to String
                to_date_str = to_date.strftime("%B %d, %Y")
                # print(to_date_str)
                to_date_list.append(to_date_str)

            #Method Certificate
            if re.findall('.*.pem:\s+(.*)certificate\s+generated\s+by\s+system',line):
                cert_method = re.findall('.*.pem:\s+(.*)certificate\s+generated\s+by\s+system',line)
                cert_method = cert_method[0]
                cert_method_list.append(cert_method)

            elif re.findall('.*.pem:\s+$',line):
                cert_method = 'Not Record'
                cert_method_list.append(cert_method)
        
        # print(cert_name_list)
        # print(from_date_list)
        # print(to_date_list)
        # print(cert_method_list)

        #Get Date Now
        date_now = datetime.datetime.now()
        date_now_convert = date_now.date()
        # print(date_now_convert)
        for date_list in to_date_list:
            # print(date_list)
            #Convert String to Date
            date_convert = datetime.datetime.strptime(date_list, "%B %d, %Y")
            date_convert = date_convert.date()
            # print(date_now_convert)
            if date_convert <= date_now_convert:
                cert_status = "Expired"
                cert_status_list.append(cert_status)
                # print(cert_status)
            else:
                cert_status = "Not Expired"
                cert_status_list.append(cert_status)
                # print(cert_status)
        # print(cert_status_list)

        #REPLICATION STATUS SUMMARY TABLE
        replication_list = ''
        rep_status_list = ''
        node_list = ''

        #PHONE SUMMARY TABEL
        tipe_list = ''
        list_value_phone = ''
        list_firmware = ''
        eos_phone = ''
        list_status_phone = ''
        list_desc_phone = ''
        list_name_phone = ''
        list_name_ext = ''
        list_ext_phone = ''
        list_status_ext = ''

        #ALERT AND LOG SUMMARY TABLE
        start_line_log = False
        line_log_list = []
        for line in read_file_list:
            if 'file tail activelog /cm/log/amc/AlertLog/' in line:
                start_line_log = True
            elif start_line_log == True:
                line_log_list.append(line)
            if re.findall('^\s*$',line) and start_line_log == True:
                break

        if len(line_log_list) > 1:
            #create list from txt
            list_value_desc_log = []
            for text_log in line_log_list:
                current_text_log = text_log.split(',')
                list_value_desc_log.append(current_text_log)
            #create list only description and split it
            list_desc_log = []
            for value_desc_log in list_value_desc_log:
                try:
                    new_value_desc_log = value_desc_log[3].strip()
                    new_value_desc_log = new_value_desc_log.split()
                    list_desc_log.append(new_value_desc_log)
                except:
                    pass
            #comparation list
            list_compare_log = []
            #iteration from source which is list_desc_log
            for enum_desc_log, desc_log in enumerate(list_desc_log):
                #we need to add first index of list_desc_log for comparing(1st iteration)
                if len(list_compare_log) == 0:
                    if re.findall('^\d+', list_value_desc_log[enum_desc_log][0]):
                        list_compare_log.append(desc_log)
                    else:
                        pass
                #this code below will happen in 2nd iteration
                else:
                    #this list is comparation rating between desc_log(list_desc_log) and all inside list_compare_log
                    list_rating = []
                    #this variable is for capturing rating inside for below
                    rating = 0
                    #iteration from result which is list_compare_log
                    for enum_compare_log, compare_log in enumerate(list_compare_log):
                        #iteration for every word in compare_log
                        for enum_c_log, c_log in enumerate(compare_log):
                            #we need to add try to handle error out of range
                            try:
                                #this condition will compare every word between desc_log and c_log(compare_log)
                                if re.findall('\d+', c_log):
                                    pass
                                elif c_log != desc_log[enum_c_log]:
                                    #adding rating for every word match
                                    rating += 1
                                    # code below is for print purpose in every comparation 
                                    # print(len(compare_log))
                                    # print(enum_c_log)
                                    # print(c_log)
                                    # print(desc_log[enum_c_log])
                                    # tulis=input('Pause')
                            except:
                                #if there is index out of range error, we will drop rating to 0
                                rating = 30
                        #appen all rating
                        list_rating.append(rating)
                        #normalize rating to 0 so next iteration, rating will still 0
                        rating = 0
                    #print below is for showing all comparation
                    # print(f'first_word : {desc_log[6]} | len : {len(list_rating)} | min : {min(list_rating)} | all_rating : {list_rating}')
                    #choose between condition below
                    '''
                    we will check if value max inside rating list is 0, that indicate value in desc_log
                    don't have any match in compare_log
                    '''
                    # if max(list_rating) == 0:
                    # if max(list_rating) < 10:
                    if min(list_rating) > 0:
                        #append value to list_compare_log
                        list_compare_log.append(desc_log)

            #make list only contain 3 value max
            if len(list_compare_log) > 3:
                for compare_log in range(3, len(list_compare_log)):
                    list_compare_log.pop(3)

            alert_log = '-'
            for log_data in list_compare_log:
                alert_log = alert_log+' '.join(log_data)+'\n'
            
            # for ii in list_compare_log:
            #     print(ii)
            #     print('\n')

            # tulis = input(len(list_compare_log))
        
        else:
            alert_log = '-'
        
        severity = ""

        #LICENSE CCX
        lic_name = ''
        list_lic_detail = ''
        list_lic_count = ''

        #APLICATION CCX
        list_app_name = ''
        list_app_enable = ''
        list_app_ses = ''

        #SCRIPT CCX
        list_script_name = ''

        #PROMPT CCX
        list_promp_name = ''

        #TRIGER NUMBER CCX
        list_triger_no = ''
        list_triger_name = ''
        list_triger_enable = ''
        list_triger_ses = ''

        #MESSAGE UNITY
        list_message_user = ''
        list_message = ''
        list_message_inbox = ''
        list_message_delete = ''

        #USER ID UNITY
        list_userid = ''

        device_class_name = self.__class__.__name__
        file_name = self.file
        # print(file_name)

        #TIPE DEVICES SUMMARY TABLE
        tipe_list = []
        for line in read_file_list:
            if re.findall('^ucm_cucm.*',line):
                tipe = re.findall('^ucm_cucm.*',line)
                tipe = tipe[0]
                tipe = 'Cisco Unified Communication Manager (CUCM)'
                tipe_list.append(tipe)
            elif re.findall('^ucm_cuc.*',line):
                tipe = re.findall('^ucm_cuc.*',line)
                tipe = tipe[0]
                tipe = 'Cisco Unity Connection (CUC)'
                tipe_list.append(tipe)
            elif re.findall('^ucm_imp.*',line):
                tipe = re.findall('^ucm_imp.*',line)
                tipe = tipe[0]
                tipe = 'Cisco Unified IM and Presence (CIMP)'
                tipe_list.append(tipe)
            elif re.findall('^ucm_ccx.*',line):
                tipe = re.findall('^ucm_ccx.*',line)
                tipe = tipe[0]
                tipe = 'Cisco Unified Contact Center Express (CUCX)'
                tipe_list.append(tipe)
            # else:
            #     tipe = 'Other'
            #     tipe_list.append(tipe)

        

        func_insert_dbreport_ucm = insert_dbreport_ucm(
                devicename, device_type, hardware_type, sn, eos_hardware,
                model, iosversion, uptime, confreg, version, date_eos, date_last,
                cpu, top_cpu, cpu_status,
                memory, top_memory, memory_status,
                disk_name_list, disk_total_list, disk_free_list, disk_used_list, disk_percentage_list, disk_status,
                last_date_backup, type_backup, status_backup, fitur_backup, failed_backup,
                schedule_backup,
                cert_name_list, from_date_list, to_date_list, cert_method_list, cert_status_list,
                node_list, replication_list, rep_status_list,
                alert_log, severity,
                tipe_list,
                list_value_phone, list_firmware, eos_phone,
                list_status_phone, list_desc_phone, list_name_phone,
                list_name_ext, list_ext_phone, list_status_ext,
                lic_name, list_lic_detail, list_lic_count,
                list_app_name, list_app_enable, list_app_ses,
                list_script_name,
                list_promp_name,
                list_triger_no, list_triger_name, list_triger_enable, list_triger_ses,
                list_message_user, list_message, list_message_inbox, list_message_delete,
                list_userid,
                device_class_name,
                file_name
        )
        execute_insert_dbreport_ucm = func_insert_dbreport_ucm.insert_dbreport_ucm()
        
        