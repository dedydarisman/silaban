import re
import requests
from netoprmgr.script.insert_dbreport_rs import insert_dbreport_rs
from netoprmgr.script.eosl_ip import eosl_ip



class aruba_HPE_5130:
    def __init__(self,file):
        #variable constructor
        self.file = file
        #read all things in file
        try:
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
        except:
            try:
                read_file = open(self.file, 'r', encoding='latin-1')
                read_file_list = read_file.readlines()
            except:
                print('Error Codec!!!')
                pass
        for line in read_file_list:
            #get sn
            if re.findall('PID:.*,\s+SN:\s+(\S+)',line):
                sn = re.findall('PID:.*,\s+SN:\s+(\S+)',line)
                sn = sn[0]
                break
            else:
                sn = 'Tidak di temukan'
        for line in read_file_list:
            #get ip
            if re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line):
                ip = re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line)
                ip = ip[0]
                break
            else:
                ip = 'Tidak di temukan'
        for line in read_file_list:
            #SOFTWARE TABLE
            #get device name
            if re.findall('.*sysname (.*)',line):
                devicename = re.findall('.*sysname (.*)',line)
                devicename = devicename[0]
                print(devicename)
                # input('device name')
                break
            else:
                devicename = self.file
        for line in read_file_list:
            #get device model
            if re.findall('(.*).*\s+uptime',line):
                model = re.findall('(.*).*\s+uptime',line)
                model = model[0]
                # print(model)
                # input('model')
                break                
        for line in read_file_list:
            #get ios version
            if re.findall('.*, Version\s+(\S+),\s+Release',line):
                iosversion = re.findall('.*, Version\s+(\S+),\s+Release',line)
                iosversion = iosversion[0]
                # print(iosversion)
                # input('iosversion')
                break
        for line in read_file_list:
            #get uptime
            if re.findall('.*uptime is (.*)',line):
                uptime = re.findall('.*uptime is (.*)',line)
                uptime = uptime[0]
                #print(uptime)
                # input('uptime')
                break
        for line in read_file_list:
            #SOFTWARE TABLE SUMMARY
            if re.findall('^.*Version (.*),',line):
                version = re.findall('^.*Version (.*),',line)
                version = version[0]
                #print(version)
                # input('version')
                break
        
        confreg = "-"
        status = "-"
        #Stack identification #ask
        stack_break = False
        stack_cond = False
        stack_list = []
        for line in read_file_list:
            if re.findall('\d+\s+(5130).*Master',line):
                stack_break = True
                stack = re.findall('\d+\s+(5130).*Master',line)
                stack = stack[0]
                stack_list.append(stack)
                #print(stack_list)
                # input(stack_list)
            #break loop
            if stack_break == True:
                break
        if stack == 0:
            stack_cond = False
        else:
            stack_cond = True
        # print(stack_list)
        # print(stack_cond)
        # input('stack')
        

        #Hardware
        read_file_list_hw  = []
        start_line_hw = False
        for line in read_file_list:
            if 'display fan' in line and start_line_hw == True:
                break
            if 'display device manuinfo' in line:
                start_line_hw = True
            if start_line_hw == True:
                read_file_list_hw.append(line)

        list_card = []
        list_serial_number = []
        list_hardware_description = []
        slot_check = ''
        for line in read_file_list_hw:
            #HARDWARE
            slot_merge = ''
            if re.findall('(Slot\s+\d+)\s+CPU\s+\d+:', line):
                slot = re.findall('(Slot\s+\d+)\s+CPU\s+\d+:', line)
                slot = slot[0]
                if slot != slot_check:
                    slot_check = slot
            #card PID
            elif re.findall('.EVICE_NAME\s+:\s+(\S+\s+\S+.*)',line):
                card = re.findall('.EVICE_NAME\s+:\s+(\S+\s+\S+.*)',line)
                card = card[0]
                card = card.strip()
                list_card.append(card)
                list_hardware_description.append(slot_check)
                #print(list_card)
                # input('list card')
            #card serial number
            elif re.findall('.EVICE_SERIAL_NUMBER\s+:\s+(\S+)',line):
                serial_number = re.findall('.EVICE_SERIAL_NUMBER\s+:\s+(\S+)',line)
                serial_number = serial_number[0]
                list_serial_number.append(serial_number)
                # print(list_serial_number)
                # input('serial number')

        #eos software table
        try:
            execute_eosl_ip = eosl_ip()
            resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/software/'+version)
            date = resp.json()
            if str(date) == "None":
                date_eos = 'Not Announced'
                date_last = 'Not Announced'
            else:
                date = date[0]
                date_eos = date["eos_maintenance"]
                date_last = date["last_day_of_support"]
        except:
            date_eos = 'Server EOSL Down'
            date_last = 'Server EOSL Down'

        #eos card table
        date_list = []
        for card in list_card:
            try:
                eosl_card = card
                eosl_card = re.findall('(\S+)',eosl_card)
                if len(eosl_card) == 0:
                    date = '-'
                else:
                    eosl_card = eosl_card[0]
                    execute_eosl_ip = eosl_ip()
                    resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/hardware/'+eosl_card)
                    date = resp.json()
                    if str(date) == "None":
                        date = 'Not Announced'
                    else:
                        date = date[0]
                        date = date["date"]
            except:
                date = 'Server EOSL Down'
            date_list.append(date)

        #CPU
        read_file_list_cpu  = []
        start_line_cpu = False
        for line in read_file_list:
            if 'display' in line and start_line_cpu == True:
                break
            if 'display cpu' in line:
                start_line_cpu = True
            if start_line_cpu == True:
                read_file_list_cpu.append(line)

        total = []
        status = []
        process = []
        interrupt = []
        topcpu = []
        for line in read_file_list_cpu:
            #CPU
            #cpu
            if re.findall('^\s+(\d+)%\s+in\s+last\s+5\s+seconds',line):
                # cpu_break = True
                cpu = re.findall('^\s+(\d+)%\s+in\s+last\s+5\s+seconds',line)
                cpu = cpu[0]
                cpu = int(cpu)
                # print(total)
                if cpu < 21 :
                    cond = 'Low'
                    cond = status.append(cond)
                elif cpu < 81 :
                    cond = 'Medium'
                    cond = status.append(cond)
                else :
                    cond = 'High'
                    cond = status.append(cond)
                cpu = total.append(cpu)
                inter = "-"
                top = "-"
                interrupt.append(inter)
                topcpu.append(top)

            if re.findall('^(Slot\s+\d+)\s+CPU',line):
                # cpu_break = True
                proc = re.findall('^(Slot\s+\d+)\s+CPU',line)
                proc = proc[0]
                process.append(proc)
                # print(total)
                # print(status)
                # input('cpu')

        #Memory
        read_file_list_mem  = []
        start_line_mem = False
        for line in read_file_list:
            if 'display' in line and start_line_mem == True:
                break
            if 'display memory' in line:
                start_line_mem = True
            if start_line_mem == True:
                read_file_list_mem.append(line)

        #Memory
        topproc = []
        memory_status = []
        utils = []
        for line in read_file_list_mem:
            #MEMORY
            #Memory Total
            if re.findall('^Mem:\s+(\d+)\s+\d+\s+\d+',line):
                memory_break = True
                memory_total = re.findall('^Mem:\s+(\d+)\s+\d+\s+\d+',line)
                memory_total = memory_total[0]
                # print(memory_total)
            #Memory Used
            if re.findall('^Mem:\s+\d+\s+(\d+)\s+\d+',line):
                memory_used = re.findall('^Mem:\s+\d+\s+(\d+)\s+\d+',line)
                memory_used = memory_used[0]
                # print(memory_used)
                #memory percentage
                memory_percentage = (int(memory_used)/int(memory_total))*100
                # print(memory_percentage)
                #memory status
                if float(memory_percentage)<21 :
                    m_stat='Low'
                    memory_status.append(m_stat)
                elif float(memory_percentage)<81 :
                    m_stat='Medium'
                    memory_status.append(m_stat)
                else:
                    m_stat='High'
                    memory_status.append(m_stat)
                memory_percentage=re.findall('(^.{5})*',str(memory_percentage))
                u_mem=memory_percentage[0]
                utils.append(u_mem)
                #print(memory_percentage)
                # print(memory_status)

            if re.findall('^(Slot\s+\d+):',line):
                proc = re.findall('^(Slot\s+\d+):',line)
                proc = proc[0]
                topproc.append(proc)
        
        # print(utils)
        # print(memory_status)
        # print(topproc)
        # input("Memory")

        #get environment
        list_psu_capture = []
        list_fan = []
        list_fan_cond_cp = []
        list_temp = []
        list_temp_cond = []
        list_psu = []
        list_psu_cond = []

        #Env FAN
        read_file_list_fan  = []
        start_line_fan = False
        for line in read_file_list:
            if 'display fan' in line:
                start_line_fan = True
            if 'display environment' in line:
                break
            if start_line_fan == True:
                read_file_list_fan.append(line)

        for line in read_file_list_fan:
            if re.findall('Slot\s+(\d+):',line):
                regex_fan = re.findall('Slot\s+(\d+):',line)
                slot_fan = regex_fan[0]
                slot_fan = slot_fan.strip()

            if re.findall('\s+(Fan.*):',line):
                regex_fan = re.findall('\s+(Fan.*):',line)
                fan = regex_fan[0]
                fan = fan.strip()
                list_fan.append('Slot '+slot_fan+ ' ' +fan)
                # print('list fan')
                # print(list_fan)
                # input()
                #print(fan)
            if re.findall('^\s+State\s+:\s+(.*)', line):
                regex_fan_cond = re.findall('^\s+State\s+:\s+(.*)', line)
                fan_cond = regex_fan_cond[0]
                fan_cond = fan_cond.strip()
                list_fan_cond_cp.append(fan_cond)
                # print('fan status')
                # print(list_fan_cond_cp)
                # input()
                #print(fan_cond)
        
        #Env Temperatur
        read_file_list_temp  = []
        start_line_temp = False
        for line in read_file_list:
            if 'display' in line and start_line_temp == True:
                break
            if 'display environment' in line:
                start_line_temp = True
            if start_line_temp == True:
                read_file_list_temp.append(line)

        for line in read_file_list_temp:
            if re.findall('(\d+)\s+hotspot\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\S+',line):
                regex_fan = re.findall('(\d+)\s+hotspot\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\S+',line)
                slot = regex_fan[0]
                slot = slot.strip()

            if re.findall('\d+\s+(hotspot\s+\d+)\s+\d+\s+\d+\s+\d+\s+\d+\s+\S+',line):
                regex_temp = re.findall('\d+\s+(hotspot\s+\d+)\s+\d+\s+\d+\s+\d+\s+\d+\s+\S+',line)
                temp = regex_temp[0]
                temp = temp.strip()
                list_temp.append('Slot '+slot+ ' ' +temp)
                # print('temperature')
                # print(list_temp)
                #print(temp)
            if re.findall('\d+\s+hotspot\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+\s+\S+', line):
                regex_temp_cond = re.findall('\d+\s+hotspot\s+\d+\s+(\d+)\s+\d+\s+\d+\s+\d+\s+\S+', line)
                temp_cond = regex_temp_cond[0]
                temp_cond = str(temp_cond.strip()+'°C')
                # if temp_cond < 58 :
                #     c_temp='Low'
                #     regex_temp_cond.append(c_temp)
                # elif temp_cond >= 58 :
                #     c_temp='Warning'
                #     regex_temp_cond.append(c_temp)
                # else:
                #     c_temp='Alarm On'
                #     regex_temp_cond.append(c_temp)
                list_temp_cond.append(temp_cond)
                # print('temp_condition')
                # print(temp_cond)
                #print(temp_cond)

        #Env Power Supply
        read_file_list_env  = []
        start_line_psu = False
        for line in read_file_list:
            if 'display power' in line:
                start_line_psu = True
            if 'display current-configuration' in line:
                break
            if start_line_psu == True:
                read_file_list_env.append(line)

        # for i in read_file_list_env:
        #     print(i)
        # tuadada = input("STOP")

        slot_check = ''
        list_psu = []
        list_psu_cond = []
        for i in read_file_list_env:
            slot_merge = ''
            if re.findall('Slot.*\d+', i):
                slot = re.findall('(Slot.*\d+)', i)
                slot = slot[0]
                if slot != slot_check:
                    slot_check = slot
            elif re.findall('\s+\d+\W+\w+\W+', i):
                slot_itm = re.findall('\s+(\d+)\W+\w+\W+', i)
                slot_itm = slot_itm[0]
                cond = re.findall('\s+\d+\W+(\w+)\W+', i)
                cond = cond[0]
                slot_merge = slot_check + ' - PS ' + slot_itm
                list_psu.append(slot_merge)
                list_psu_cond.append(cond)

        #conditional env if null
        if len(list_fan) == 0:
            list_fan = ['-']
        if len(list_fan_cond_cp) == 0:
            list_fan_cond_cp = ['-']
        if len(list_temp) == 0:
            list_temp = ['-']
        if len(list_temp_cond) == 0:
            list_temp_cond = ['-']
        if len(list_psu) == 0:
            list_psu = ['-']
        if len(list_psu_cond) == 0:
            list_psu_cond = ['-']
        if len(list_fan_cond_cp) == 0:
            list_psu_cond = ['-']
            
        #variable convert to insert database
        eos_card = []
        eos_date = []
        if stack_cond == True:
            for stack in stack_list:
                for enum, card in enumerate(list_card):
                    if stack in card:
                        eos_card.append(card)
                        eos_date.append(date_list[enum])
        else:
            eos_card.append(list_card[0])
            eos_date.append(date_list[0])
        device_class_name = self.__class__.__name__
        file_name = self.file

        #to tipe devices
        tipe_list = []
        for i in eos_card:
            if re.findall('^CISCO\S+',i):
                tipe = re.findall('^CISCO\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^HPE\s+5130',i):
                tipe = re.findall('^HPE\s+5130',i)
                tipe = tipe[0]
                tipe = 'HP Switch'
                tipe_list.append(tipe)
            elif re.findall('^5130\s+.*HI',i):
                tipe = re.findall('^5130\s+.*HI',i)
                tipe = tipe[0]
                tipe = 'HP Switch'
                tipe_list.append(tipe)
            elif re.findall('^ASR\S+',i):
                tipe = re.findall('^ASR\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^ISR\S+',i):
                tipe = re.findall('^ISR\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^WS-\S+',i):
                tipe = re.findall('^WS-\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^C\d+\S+',i):
                tipe = re.findall('^C\d+\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^SG\S+',i):
                tipe = re.findall('^SG\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^AIR-AP\S+',i):
                tipe = re.findall('^AIR-AP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-BR\S+',i):
                tipe = re.findall('^AIR-BR\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-CAP\S+',i):
                tipe = re.findall('^AIR-CAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-LAP\S+',i):
                tipe = re.findall('^AIR-LAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-SAP\S+',i):
                tipe = re.findall('^AIR-SAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-CT\S+',i):
                tipe = re.findall('^AIR-CT\S+',i)
                tipe = tipe[0]
                tipe = 'Wireless Controller (WLC)'
                tipe_list.append(tipe)
            elif re.findall('^ASA\S+',i):
                tipe = re.findall('^ASA\S+',i)
                tipe = tipe[0]
                tipe = 'Adaptive Security Appliance (ASA)'
                tipe_list.append(tipe)
            elif re.findall('^VG\S+',i):
                tipe = re.findall('^VG\S+',i)
                tipe = tipe[0]
                tipe = 'Voice Gateway (VG)'
                tipe_list.append(tipe)
            elif re.findall('^N\S+',i):
                tipe = re.findall('^N\S+',i)
                tipe = tipe[0]
                tipe = 'Nexus'
                tipe_list.append(tipe)
            elif re.findall('^HP\s+\S+\s+Switch\s+\S+',i):
                tipe = re.findall('^HP\s+\S+\s+Switch\s+\S+',i)
                tipe = tipe[0]
                tipe = 'HP Switch'
                tipe_list.append(tipe)
            else:
                tipe = 'Other'
                tipe_list.append(tipe)
            break
        #to database
        try:
            print(version)#
        except:
            version = '-'
        try:
            print(devicename)#
        except:
            devicename = '-'
        try:
            print(model)#
        except:
            model = '-'
        try:
            print(iosversion)
        except:
            iosversion = '-'
        try:
            print(uptime)
        except:
            uptime = '-'
        try:
            print(list_card)
        except:
            list_card = '-'
        try:
            print(list_serial_number)
        except:
            list_serial_number = '-'
        try:
            print(total)
        except:
            total = '-'
        try:
            print(status)
        except:
            status = '-'
        try:
            print(memory_percentage)
        except:
            memory_percentage = '-'
        try:
            print(utils)
        except:
            utils = '-'
        try:
            print(psu)
        except:
            psu = '-'
        try:
            print(list_psu)
        except:
            list_psu = '-'
        try:
            print(list_psu_cond)
        except:
            list_psu_cond = '-'
        try:
            print(fan)
        except:
            fan = '-'
        try:
            print(list_fan)
        except:
            list_fan = '-'
        try:
            print(list_fan_cond_cp)
        except:
            list_fan_cond_cp = '-'
        try:
            print(list_temp)
        except:
            list_temp = '-'
        try:
            print(list_temp_cond)
        except:
            list_temp_cond = '-'
        try:
            print(eos_date)
        except:
            eos_date = '-'
        try:
            print(device_class_name)
        except:
            device_class_name = '-'
        try:
            print(eos_card)
        except:
            eos_card = '-'
        try:
            print(file_name)
        except:
            file_name = '-'
        try:
            print(list_card)
        except:
            list_card = '-'
        try:
            print(tipe_list)
        except:
            tipe_list = '-'    

        func_insert_dbreport_rs = insert_dbreport_rs(
                version, devicename, model, iosversion, uptime, confreg, date_eos, date_last,
                card, list_serial_number, list_hardware_description, date_list,
                total, process, interrupt, topcpu, status,utils, topproc,
                memory_status, psu, list_psu, list_psu_cond, fan, list_fan, list_fan_cond_cp,
                list_temp, list_temp_cond, eos_date, device_class_name, eos_card, file_name, list_card, tipe_list,
                sn, ip
        )
        execute_insert_dbreport_rs = func_insert_dbreport_rs.insert_dbreport_rs()
