import re
import requests
from netoprmgr.script.insert_dbreport_rs import insert_dbreport_rs
from netoprmgr.script.eosl_ip import eosl_ip



class aruba_5406RZL:
    def __init__(self,file):
        #variable constructor
        self.file = file
        #read all things in file
        try:
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
        except:
            try:
                read_file = open(self.file, 'r', encoding='latin-1')
                read_file_list = read_file.readlines()
            except:
                print('Error Codec!!!')
                pass
        for line in read_file_list:
            #get sn
            if re.findall('PID:.*,\s+SN:\s+(\S+)',line):
                sn = re.findall('PID:.*,\s+SN:\s+(\S+)',line)
                sn = sn[0]
                break
            else:
                sn = 'Tidak di temukan'
        for line in read_file_list:
            #get ip
            if re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line):
                ip = re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line)
                ip = ip[0]
                break
            else:
                ip = 'Tidak di temukan'
        for line in read_file_list:
            #SOFTWARE TABLE
            #get device name
            if re.findall('^hostname "(.*)"',line):
                devicename = re.findall('^hostname "(.*)"',line)
                devicename = devicename[0]
                # print(devicename)
                # input('device name')
                break
            else:
                devicename = self.file
        for line in read_file_list:
            #get device model
            if re.findall('Model\s+:\s+(.*5406\S+)',line):
                model = re.findall('Model\s+:\s+(.*5406\S+)',line)
                model = model[0]
                # print(model)
                # input('model')
                break                
        for line in read_file_list:
            #get ios version
            if re.findall('.*Software\s+Version\s+:\s+(.*\d+)',line):
                iosversion = re.findall('.*Software\s+Version\s+:\s+(.*\d+)',line)
                iosversion = iosversion[0]
                # print(iosversion)
                # input('iosversion')
                break
        for line in read_file_list:
            #get uptime
            if re.findall('\s+Uptime\s+:\s+(.*)\s+',line):
                uptime = re.findall('\s+Uptime\s+:\s+(.*)\s+',line)
                uptime = uptime[0]
                # print(uptime)
                # input('uptime')
                break
        for line in read_file_list:
            #SOFTWARE TABLE SUMMARY
            if re.findall('.*Software\s+Version\s+:\s+(.*\d+)',line):
                version = re.findall('.*Software\s+Version\s+:\s+(.*\d+)',line)
                version = version[0]
                # print(version)
                # input('version')
                break
        
        confreg = "-"
        status = "-"
        #Stack identification #ask
        stack_break = False
        stack_cond = False
        stack_list = []
        for line in read_file_list:
            if re.findall('\s+\d+\s+\S+\s+(HP\s+\S+\s+\S+\s+\S+)\s+\d+\s+\S+\s+',line):
                stack_break = True
                stack = re.findall('\s+\d+\s+\S+\s+(HP\s+\S+\s+\S+\s+\S+)\s+\d+\s+\S+\s+',line)
                stack = stack[0]
                stack_list.append(stack)
                # print(stack_list)
                # input(stack_list)
            #break loop
            if stack_break == True:
                break
        if stack == 0:
            stack_cond = False
        else:
            stack_cond = True
        # print(stack)
        # print(stack_cond)
        # input('stack')

        # print(stack_list)
        # input("STACK")
        
        #mengambil show show vsf detail
        #show ini akan digunakan untuk bagian hadware cpu dan juga memory
        list_vsf_detail = []
        vsf_detail = False
        for line in read_file_list:
            if 'show' in line and vsf_detail == True:
                break
            if 'show vsf detail' in line:
                vsf_detail = True
            if vsf_detail == True:
                list_vsf_detail.append(line)


        #variabel HARDWARE
        list_card = []
        list_serial_number = []
        list_hardware_description = []
        hardware_break = False
        for line in list_vsf_detail:
            #card PID
            if re.findall('Model\s+:\s+(.*5406\S+)',line):
                hardware_break = True
                card = re.findall('Model\s+:\s+(.*5406\S+)',line)
                card = card[0]
                card = card.strip()
                list_card.append(card)
                # print(list_card)
                # input('list card')
            #card serial number
            if re.findall('^Serial\s+Number\s+:\s+(\S+)\s+',line):
                serial_number = re.findall('^Serial\s+Number\s+:\s+(\S+)\s+',line)
                serial_number = serial_number[0]
                list_serial_number.append(serial_number)
                # print(list_serial_number)
                # input('serial number')
            # Description
            if re.findall('^Member\s+ID\s+:\s+(\d+)',line):
                hw = re.findall('^Member\s+ID\s+:\s+(\d+)',line)
                hw = hw[0]
                hw = str("Member ID " + hw)
                list_hardware_description.append(hw)

        #eos software table
        try:
            execute_eosl_ip = eosl_ip()
            resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/software/'+version)
            date = resp.json()
            if str(date) == "None":
                date_eos = 'Not Announced'
                date_last = 'Not Announced'
            else:
                date = date[0]
                date_eos = date["eos_maintenance"]
                date_last = date["last_day_of_support"]
        except:
            date_eos = 'Server EOSL Down'
            date_last = 'Server EOSL Down'

        #eos card table
        date_list = []
        for card in list_card:
            try:
                eosl_card = card
                eosl_card = re.findall('(\S+)',eosl_card)
                if len(eosl_card) == 0:
                    date = '-'
                else:
                    eosl_card = eosl_card[0]
                    execute_eosl_ip = eosl_ip()
                    resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/hardware/'+eosl_card)
                    date = resp.json()
                    if str(date) == "None":
                        date = 'Not Announced'
                    else:
                        date = date[0]
                        date = date["date"]
            except:
                date = 'Server EOSL Down'
            date_list.append(date)
        
        # print(model)
        # print(date_list)
        # print(list_card)
        # tulis = input("CARD")
        total = []
        status = []
        process = []
        interrupt = []
        topcpu = []
        for line in list_vsf_detail:
            #CPU
            #cpu
            if re.findall('CPU\s+Utilization\s+:\s+(\d+)%',line):
                # cpu_break = True
                cpu = re.findall('CPU\s+Utilization\s+:\s+(\d+)%',line)
                cpu = cpu[0]
                cpu = int(cpu)
                # print(total)
                if cpu < 21 :
                    cond = 'Low'
                    cond = status.append(cond)
                elif cpu < 81 :
                    cond = 'Medium'
                    cond = status.append(cond)
                else :
                    cond = 'High'
                    cond = status.append(cond)
                cpu = total.append(cpu)
                inter = "-"
                top = "-"
                interrupt.append(inter)
                topcpu.append(top)
            
            if re.findall('^Member\s+ID\s+:\s+(\d+)',line):
                # cpu_break = True
                proc = re.findall('^Member\s+ID\s+:\s+(\d+)',line)
                proc = proc[0]
                proc = str("Member ID " + proc)
                process.append(proc)
                # print(total)
                # print(status)
                # input('cpu')

        #Memory
        topproc = []
        memory_status = []
        utils = []
        for line in list_vsf_detail:
            #MEMORY
            #Memory Total
            if re.findall('Memory\s+-\s+Total\s+:\s+(.*)\s+bytes\s+',line):
                #total jutaan
                total_a = re.findall('Memory\s+-\s+Total\s+:\s+(\d+),\d+,\d+\s+bytes',line)
                total_a = total_a[0]
                #total ribuan
                total_b = re.findall('Memory\s+-\s+Total\s+:\s+\d+,(\d+),\d+\s+bytes',line)
                total_b = total_b[0]
                #total satuan
                total_c = re.findall('Memory\s+-\s+Total\s+:\s+\d+,\d+,(\d+)\s+bytes',line)
                total_c = total_c[0]
                memory_total = int(total_a+total_b+total_c)
                # print(memory_total)
                
            #Memory Free
            if re.findall('Free\s+:\s+(.*)\s+bytes',line):
                #free jutaan
                free_a = re.findall('Free\s+:\s+(\d+),\d+,\d+\s+bytes',line)
                free_a = free_a[0]
                #free ribuan
                free_b = re.findall('Free\s+:\s+\d+,(\d+),\d+\s+bytes',line)
                free_b = free_b[0]
                #free satuan
                free_c = re.findall('Free\s+:\s+\d+,\d+,(\d+)\s+bytes',line)
                free_c = free_c[0]
                memory_free = int(free_a+free_b+free_c)
                # print(memory_free)
                # input('freeMemory')
                #memory percentage
                memory_percentage = (memory_total-memory_free)/memory_total*100
                #memory status
                if float(memory_percentage)<21 :
                    m_stat='Low'
                    memory_status.append(m_stat)
                elif float(memory_percentage)<81 :
                    m_stat='Medium'
                    memory_status.append(m_stat)
                else:
                    m_stat='High'
                    memory_status.append(m_stat)
                memory_percentage=re.findall('(^.{5})*',str(memory_percentage))
                u_mem=memory_percentage[0]
                utils.append(u_mem)

            if re.findall('^Member\s+ID\s+:\s+(\d+)',line):
                proc = re.findall('^Member\s+ID\s+:\s+(\d+)',line)
                proc = proc[0]
                proc = str("Member ID " + proc)
                topproc.append(proc)
        
        # print(utils)
        # print(memory_status)
        # print(topproc)
        # input("Memory")
        #get environment
        list_psu_capture = []
        list_fan = []
        list_fan_cond_cp = []
        list_temp = []
        list_temp_cond = []
        list_psu = []
        list_psu_cond = []
        psu_line_start = 0
        psu_line_end = 0
        count_line=0

        #Env FAN
        read_file_list_fan  = []
        start_line_fan = False
        for line in read_file_list:
            if 'Fan Information' in line:
                start_line_fan = True
            if 'show tech' in line:
                break
            if start_line_fan == True:
                read_file_list_fan.append(line)

        for line in read_file_list_fan:
            if re.findall('VSF-(Member\s+\d+)',line):
                regex_member = re.findall('VSF-(Member\s+\d+)',line)
                member = regex_member[0]
                member = member.strip()
                list_fan.append(member+ ' FAN')
            
            if re.findall('(\d+)\s+\/\s+\d+\s+Fans\s+in\s+Failure\s+State',line):
                regex_failure = re.findall('(\d+)\s+\/\s+\d+\s+Fans\s+in\s+Failure\s+State',line)
                failure = regex_failure[0]
                failure = int(failure.strip())
                if failure == 0:
                    fan_cond = 'OK'
                    list_fan_cond_cp.append(fan_cond)
                else:
                    fan_cond = str(failure)+ ' Fan Failure'
                    list_fan_cond_cp.append(fan_cond)

        #Env Temperatur
        read_file_list_temp  = []
        start_line_temp = False
        for line in read_file_list:
            if 'System Air Temperatures' in line:
                start_line_temp = True
            if 'Fan Information' in line:
                break
            if start_line_temp == True:
                read_file_list_temp.append(line)

        for line in read_file_list_temp:
            if re.findall('VSF-(Member\s+\d+)',line):
                regex_member = re.findall('VSF-(Member\s+\d+)',line)
                member = regex_member[0]
                member = member.strip()
                list_temp.append(member+ ' Temp')

            if re.findall('\s+Chassis\s+(\d+)C.*', line):
                regex_temp_cond = re.findall('\s+Chassis\s+(\d+)C.*', line)
                temp_cond = regex_temp_cond[0]
                temp_cond = temp_cond.strip()
                list_temp_cond.append(temp_cond+'°C')
                # print('temp_condition')
                # print(temp_cond)
                #print(temp_cond)
        
        #Env Power Supply
        read_file_list_env  = []
        start_line_psu = False
        for line in read_file_list:
            if 'System Air Temperatures' in line and start_line_psu == False:
                break
            if 'Power Supply Status' in line:
                start_line_psu = True
            if start_line_psu == True:
                read_file_list_env.append(line)

        for line in read_file_list_env:
            if re.findall('(\d+)\s+\d+\s+\S+\s+\S+\s+\S+\s+.C\s+\S+',line):
                regex_psu = re.findall('(\d+)\s+\d+\s+\S+\s+\S+\s+\S+\s+.C\s+\S+',line)
                regex_nomor_psu = re.findall('\d+\s+(\d+)\s+\S+\s+\S+\s+\S+\s+.C\s+\S+',line)
                psu = regex_psu[0]
                psu = psu.strip()
                ps = regex_nomor_psu[0]
                ps = ps.strip()
                list_psu.append('Member '+psu+ ' PS ' + ps)
                # print('PSU')
                # print(list_psu)
                #print(temp)
            if re.findall('\d+\s+\d+\s+\S+\s+\S+\s+(\S+)\s+.C\s+\S+', line):
                regex_psu_cond = re.findall('\d+\s+\d+\s+\S+\s+\S+\s+(\S+)\s+.C\s+\S+', line)
                psu_cond = regex_psu_cond[0]
                psu_cond = psu_cond.strip()
                list_psu_cond.append(psu_cond)
                # print('psu cond')
                # print(list_psu_cond)
                # input()
                #print(temp_cond)

        #conditional env if null
        if len(list_fan) == 0:
            list_fan = ['-']
        if len(list_fan_cond_cp) == 0:
            list_fan_cond_cp = ['-']
        if len(list_temp) == 0:
            list_temp = ['-']
        if len(list_temp_cond) == 0:
            list_temp_cond = ['-']
        if len(list_psu) == 0:
            list_psu = ['-']
        if len(list_psu_cond) == 0:
            list_psu_cond = ['-']
        if len(list_fan_cond_cp) == 0:
            list_psu_cond = ['-']
            
        #variable convert to insert database
        eos_card = []
        eos_date = []
        if stack_cond == True:
            for stack in stack_list:
                for enum, card in enumerate(list_card):
                    if stack in card:
                        eos_card.append(card)
                        eos_date.append(date_list[enum])
        else:
            eos_card.append(list_card[0])
            eos_date.append(date_list[0])
        device_class_name = self.__class__.__name__
        file_name = self.file

        # print(eos_card)
        # tulas = input("EOS CARD")

        #to tipe devices
        tipe_list = []
        for i in eos_card:
            if re.findall('^CISCO\S+',i):
                tipe = re.findall('^CISCO\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^ASR\S+',i):
                tipe = re.findall('^ASR\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^ISR\S+',i):
                tipe = re.findall('^ISR\S+',i)
                tipe = tipe[0]
                tipe = 'Router'
                tipe_list.append(tipe)
            elif re.findall('^WS-\S+',i):
                tipe = re.findall('^WS-\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^C\d+\S+',i):
                tipe = re.findall('^C\d+\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^SG\S+',i):
                tipe = re.findall('^SG\S+',i)
                tipe = tipe[0]
                tipe = 'Switch'
                tipe_list.append(tipe)
            elif re.findall('^AIR-AP\S+',i):
                tipe = re.findall('^AIR-AP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-BR\S+',i):
                tipe = re.findall('^AIR-BR\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-CAP\S+',i):
                tipe = re.findall('^AIR-CAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-LAP\S+',i):
                tipe = re.findall('^AIR-LAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-SAP\S+',i):
                tipe = re.findall('^AIR-SAP\S+',i)
                tipe = tipe[0]
                tipe = 'Access Point (AP)'
                tipe_list.append(tipe)
            elif re.findall('^AIR-CT\S+',i):
                tipe = re.findall('^AIR-CT\S+',i)
                tipe = tipe[0]
                tipe = 'Wireless Controller (WLC)'
                tipe_list.append(tipe)
            elif re.findall('^ASA\S+',i):
                tipe = re.findall('^ASA\S+',i)
                tipe = tipe[0]
                tipe = 'Adaptive Security Appliance (ASA)'
                tipe_list.append(tipe)
            elif re.findall('^VG\S+',i):
                tipe = re.findall('^VG\S+',i)
                tipe = tipe[0]
                tipe = 'Voice Gateway (VG)'
                tipe_list.append(tipe)
            elif re.findall('^N\S+',i):
                tipe = re.findall('^N\S+',i)
                tipe = tipe[0]
                tipe = 'Nexus'
                tipe_list.append(tipe)
            elif re.findall('^HP\s+\S+\s+Switch\s+\S+',i):
                tipe = re.findall('^HP\s+\S+\s+Switch\s+\S+',i)
                tipe = tipe[0]
                tipe = 'HP Switch'
                tipe_list.append(tipe)
            else:
                tipe = 'Other'
                tipe_list.append(tipe)
            break
        #to database
        try:
            print(version)#
        except:
            version = '-'
        try:
            print(devicename)#
        except:
            devicename = '-'
        try:
            print(model)#
        except:
            model = '-'
        try:
            print(iosversion)
        except:
            iosversion = '-'
        try:
            print(uptime)
        except:
            uptime = '-'
        try:
            print(list_card)
        except:
            list_card = '-'
        try:
            print(list_serial_number)
        except:
            list_serial_number = '-'
        try:
            print(total)
        except:
            total = '-'
        try:
            print(status)
        except:
            status = '-'
        try:
            print(memory_percentage)
        except:
            memory_percentage = '-'
        try:
            print(utils)
        except:
            utils = '-'
        try:
            print(psu)
        except:
            psu = '-'
        try:
            print(list_psu)
        except:
            list_psu = '-'
        try:
            print(list_psu_cond)
        except:
            list_psu_cond = '-'
        try:
            print(fan)
        except:
            fan = '-'
        try:
            print(list_fan)
        except:
            list_fan = '-'
        try:
            print(list_fan_cond_cp)
        except:
            list_fan_cond_cp = '-'
        try:
            print(list_temp)
        except:
            list_temp = '-'
        try:
            print(list_temp_cond)
        except:
            list_temp_cond = '-'
        try:
            print(eos_date)
        except:
            eos_date = '-'
        try:
            print(device_class_name)
        except:
            device_class_name = '-'
        try:
            print(eos_card)
        except:
            eos_card = '-'
        try:
            print(file_name)
        except:
            file_name = '-'
        try:
            print(list_card)
        except:
            list_card = '-'
        try:
            print(tipe_list)
        except:
            tipe_list = '-'    

        func_insert_dbreport_rs = insert_dbreport_rs(
                version, devicename, model, iosversion, uptime, confreg, date_eos, date_last,
                card, list_serial_number, list_hardware_description, date_list,
                total, process, interrupt, topcpu, status,utils, topproc,
                memory_status, psu, list_psu, list_psu_cond, fan, list_fan, list_fan_cond_cp,
                list_temp, list_temp_cond, eos_date, device_class_name, eos_card, file_name, list_card, tipe_list,
                sn, ip
        )
        execute_insert_dbreport_rs = func_insert_dbreport_rs.insert_dbreport_rs()
        
        
