import re
import requests
from netoprmgr.script.insert_dbreport_fortigate import insert_dbreport_fortigate
from netoprmgr.script.eosl_ip import eosl_ip
import datetime
import numpy as np
import numpy as np

class fortigate_1000D:
    def __init__(self,file):
        #variable constructor
        self.file = file
        #read all things in file
        try:
            read_file = open(self.file, 'r')
            read_file_list = read_file.readlines()
        except:
            try:
                read_file = open(self.file, 'r', encoding='latin-1')
                read_file_list = read_file.readlines()
            except:
                print('Error Codec!!!')
                pass
        for line in read_file_list:
            #get sn
            if re.findall('PID:.*,\s+SN:\s+(\S+)',line):
                sn = re.findall('PID:.*,\s+SN:\s+(\S+)',line)
                sn = sn[0]
                break
            else:
                sn = 'Tidak di temukan'
        for line in read_file_list:
            #get ip
            if re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line):
                ip = re.findall('.*Reboot.*:\s+Static\s+IP\s+Addr\s+Set\s+(\S+)',line)
                ip = ip[0]
                break
            else:
                ip = 'Tidak di temukan'

        #SOFTWARE TABLE / BUG SUMMARY
        print("============================")
        print("SOFTWARE TABLE / BUG SUMMARY")
        print("============================")
        for line in read_file_list:
            #Device Name
            if re.findall('^Hostname:\s+(.*)',line):
                devicename = re.findall('^Hostname:\s+(.*)',line)
                devicename = devicename[0]
                devicename = devicename
                print("Devicename: "+devicename)
                break
            else:
                devicename = self.file

        for line in read_file_list:
            #Model ambil dari version kalimat awal
            if re.findall('^Version:\s+(\S+)\s+.*',line):
                model = re.findall('^Version:\s+(\S+)\s+.*',line)
                model = model[0]
                model = model
                print("Model: "+model)
                break

        for line in read_file_list:
            #Version
            if re.findall('^Version:\s+(.*)',line):
                iosversion = re.findall('^Version:\s+(.*)',line)
                iosversion = iosversion[0]
                iosversion = iosversion
                version = iosversion
                print("Version: "+version)
                break
        
        for line in read_file_list:
            #Confreg di ganti menjadi System part Number
            if re.findall('^System\s+Part-Number:\s+(.*)',line):
                confreg = re.findall('^System\s+Part-Number:\s+(.*)',line)
                confreg = confreg[0]
                confreg = confreg
                print("Confreg: "+confreg)
                break
        
        for line in read_file_list:
            #Uptime (Days)
            if re.findall('^Uptime:\s+(.*)',line):
                uptime = re.findall('^Uptime:\s+(.*)',line)
                uptime = uptime[0]
                print("Uptime: "+uptime)
                break
        
        print("\n")

        #SYSTEM ANALYSIS TABEL
        print("============================")
        print("SYSTEM ANALYSIS TABEL")
        print("============================")
        list_system = []
        list_system_value = []
        for line in read_file_list:
            #Version
            if re.findall('^(Version):\s+.*', line):
                ver = re.findall('^(Version):\s+.*',line)
                ver = ver[0]
                ver = ver
                list_system.append(ver)
                print("Version: "+ver)

                list_system_value.append(version)
                print("Version Value: "+version)
                break

        for line in read_file_list:
            #Serial Number
            if re.findall('^(Serial)-(Number):\s+(.*)', line):
                find = re.match('^(Serial)-(Number):\s+(.*)', line)
                serial = find.group(1)
                number = find.group(2)
                sn = serial+' '+number
                list_system.append(sn)
                print("Serial Number: "+sn)

                sn_value = find.group(3)
                list_system_value.append(sn_value)
                print("Serial Number Value: "+sn_value)
                break

        for line in read_file_list:
            #BIOS Version
            if re.findall('^(BIOS\s+version):\s+(.*)', line):
                find = re.match('^(BIOS\s+version):\s+(.*)', line)
                bios = find.group(1)
                list_system.append(bios)
                print("BIOS Version: "+bios)

                bios_value = find.group(2)
                list_system_value.append(bios_value)
                print("BIOS Version Value: "+bios_value)
                break
        
        for line in read_file_list:
            #System Part Number
            if re.findall('^(System\s+Part-Number):\s+.*', line):
                part_number = re.findall('^(System\s+Part-Number):\s+.*',line)
                part_number = part_number[0]
                part_number = part_number
                list_system.append(part_number)
                print("System Part Number: "+part_number)

                list_system_value.append(confreg)
                print("System Part Number Value: "+confreg)
                break
        
        for line in read_file_list:
            #Log Hardisk
            if re.findall('^(Log\s+hard\s+disk):\s+(.*)', line):
                find = re.match('^(Log\s+hard\s+disk):\s+(.*)', line)
                log_hd = find.group(1)
                list_system.append(log_hd)
                print("Log Hardisk: "+log_hd)

                log_hd_value = find.group(2)
                list_system_value.append(log_hd_value)
                print("Log Hardisk Value: "+log_hd_value)
                break
        
        for line in read_file_list:
            #Operation Mod
            if re.findall('^(Operation\s+Mode):\s+(.*)', line):
                find = re.match('^(Operation\s+Mode):\s+(.*)', line)
                operation = find.group(1)
                list_system.append(operation)
                print("Operation Mod: "+operation)

                operation_value = find.group(2)
                list_system_value.append(operation_value)
                print("Operation Mod Value: "+operation_value)
                break
        
        for line in read_file_list:
            #Current virtual domain
            if re.findall('^(Current\s+virtual\s+domain):\s+(.*)', line):
                find = re.match('^(Current\s+virtual\s+domain):\s+(.*)', line)
                current = find.group(1)
                list_system.append(current)
                print("Current virtual domain: "+current)

                current_value = find.group(2)
                list_system_value.append(current_value)
                print("Current virtual domain Value: "+current_value)
                break
        
        for line in read_file_list:
            #Max number of virtual domains
            if re.findall('^(Max\s+number\s+of\s+virtual\s+domains):\s+(\d+)', line):
                find = re.match('^(Max\s+number\s+of\s+virtual\s+domains):\s+(\d+)', line)
                max_vdomain = find.group(1)
                list_system.append(max_vdomain)
                print("Max number of virtual domains: "+max_vdomain)

                max_vdomain_value = find.group(2)
                list_system_value.append(max_vdomain_value)
                print("Max number of virtual domains Value: "+max_vdomain_value)
                break
        
        for line in read_file_list:
            #Virtual domains status
            if re.findall('^(Virtual\s+domains\s+status):\s+(.*)', line):
                find = re.match('^(Virtual\s+domains\s+status):\s+(.*)', line)
                vdomain_status = find.group(1)
                list_system.append(vdomain_status)
                print("Virtual domains status: "+vdomain_status)

                vdomain_status_value = find.group(2)
                list_system_value.append(vdomain_status_value)
                print("Virtual domains status Value: "+vdomain_status_value)
                break
        
        for line in read_file_list:
            #Virtual domain configuration
            if re.findall('^(Virtual\s+domain\s+configuration):\s+(.*)', line):
                find = re.match('^(Virtual\s+domain\s+configuration):\s+(.*)', line)
                vdomain_conf = find.group(1)
                list_system.append(vdomain_conf)
                print("Virtual domain configuration: "+vdomain_conf)

                vdomain_conf_value = find.group(2)
                list_system_value.append(vdomain_conf_value)
                print("Virtual domain configuration Value: "+vdomain_conf_value)
                break
        
        for line in read_file_list:
            #System time
            if re.findall('^(System\s+time):\s+(.*)', line):
                find = re.match('^(System\s+time):\s+(.*)', line)
                system_time = find.group(1)
                list_system.append(system_time)
                print("System time: "+system_time)

                system_time_value = find.group(2)
                list_system_value.append(system_time_value)
                print("System time Value: "+system_time_value)
                break
        
        for line in read_file_list:
            #Uptime
            if re.findall('^(Uptime):\s+(.*)', line):
                find = re.match('^(Uptime):\s+(.*)', line)
                uptime = find.group(1)
                list_system.append(uptime)
                print("Uptime: "+uptime)

                uptime_value = find.group(2)
                list_system_value.append(uptime_value)
                print("Uptime: "+uptime_value)

                #Latest Software Version
                latest_sw = 'Latest Software Version'
                list_system.append(latest_sw)
                print("Latest Software Version: "+latest_sw)

                latest_sw_value = 'Not Announced'
                list_system_value.append(latest_sw_value)
                print("Latest Software Version Value: "+latest_sw_value)
                break

        #EOS SOFTWARE
        try:
            execute_eosl_ip = eosl_ip()
            resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/software/'+version)
            date = resp.json()
            if str(date) == "None":
                date_eos_sw = 'Not Announced'
            else:
                date = date[0]
                date_eos_sw = date["eos_maintenance"]
        except:
            date_eos_sw = 'Server EOSL Down'
        
        eos_software = "Software EOS"
        list_system.append(eos_software)
        list_system_value.append(date_eos_sw)

        print(eos_software+": "+date_eos_sw)
            
        #EOS HARDWARE
        try:
            execute_eosl_ip = eosl_ip()
            resp = requests.get('http://'+execute_eosl_ip+'/api/v1/resources/hardware/'+model)
            date = resp.json()
            if str(date) == "None":
                date_eos_hw = 'Not Announced'
            else:
                date = date[0]
                date_eos_hw = date["date"]
        except:
            date_eos_hw = 'Server EOSL Down'
        
        eos_hardware = "Hardware EOS"
        list_system.append(eos_hardware)
        list_system_value.append(date_eos_hw)

        print(eos_hardware+": "+date_eos_hw)
        print("\n")
        
        #HIGH AVAILABILITY TABEL
        print("============================")
        print("HIGH AVAILABILITY TABEL")
        print("============================")
        list_ha = []
        list_ha_value = []
        for line in read_file_list:
            #HA Health Status
            if re.findall('^(HA\s+Health\s+Status):\s+(.*)', line):
                find = re.match('^(HA\s+Health\s+Status):\s+(.*)', line)
                ha = find.group(1)
                list_ha.append(ha)
                print("HA Health Status: "+ha)

                ha_value = find.group(2)
                list_ha_value.append(ha_value)
                print("HA Health Status Value: "+ha_value)
                break
        
        for line in read_file_list:
            #Model
            if re.findall('^(Model):\s+(.*)', line):
                find = re.match('^(Model):\s+(.*)', line)
                model = find.group(1)
                list_ha.append(model)
                print("Model: "+model)

                model_value = find.group(2)
                list_ha_value.append(model_value)
                print("Model Value: "+model_value)
                break
        
        for line in read_file_list:
            #Mode
            if re.findall('^(Mode):\s+(.*)', line):
                find = re.match('^(Mode):\s+(.*)', line)
                mode = find.group(1)
                list_ha.append(mode)
                print("Mode: "+mode)

                mode_value = find.group(2)
                list_ha_value.append(mode_value)
                print("Mode Value: "+mode_value)
                break
        
        for line in read_file_list:
            #Group
            if re.findall('^(Group):\s+(.*)', line):
                find = re.match('^(Group):\s+(.*)', line)
                group = find.group(1)
                list_ha.append(group)
                print("Group: "+group)

                group_value = find.group(2)
                list_ha_value.append(group_value)
                print("Group Value: "+group_value)
                break
        
        for line in read_file_list:
            #Debug
            if re.findall('^(Debug):\s+(.*)', line):
                find = re.match('^(Debug):\s+(.*)', line)
                debug = find.group(1)
                list_ha.append(debug)
                print("Debug: "+debug)

                debug_value = find.group(2)
                list_ha_value.append(debug_value)
                print("Debug Value: "+debug_value)
                break
        
        for line in read_file_list:
            #Cluster Uptime
            if re.findall('^(Cluster\s+Uptime):\s+(.*)', line):
                find = re.match('^(Cluster\s+Uptime):\s+(.*)', line)
                cluster_uptime = find.group(1)
                list_ha.append(cluster_uptime)
                print("Cluster Uptime: "+cluster_uptime)

                cluster_uptime_value = find.group(2)
                list_ha_value.append(cluster_uptime_value)
                print("Cluster Uptime Value: "+cluster_uptime_value)
                break

        for line in read_file_list:
            #Cluster state change time
            if re.findall('^(Cluster\s+state\s+change\s+time):\s+(.*)', line):
                find = re.match('^(Cluster\s+state\s+change\s+time):\s+(.*)', line)
                cluster_state = find.group(1)
                list_ha.append(cluster_state)
                print("Cluster state change time: "+cluster_state)

                cluster_state_value = find.group(2)
                list_ha_value.append(cluster_state_value)
                print("Cluster state change time Value: "+cluster_state_value)
                break
        
        for line in read_file_list:
            #ses_pickup
            if re.findall('^(ses_pickup):\s+(.*)', line):
                find = re.match('^(ses_pickup):\s+(.*)', line)
                ses_pickup = find.group(1)
                list_ha.append(ses_pickup)
                print("ses_pickup: "+ses_pickup)

                ses_pickup_value = find.group(2)
                list_ha_value.append(ses_pickup_value)
                print("ses_pickup Value: "+ses_pickup_value)
                break
        
        for line in read_file_list:
            #override
            if re.findall('^(override):\s+(.*)', line):
                find = re.match('^(override):\s+(.*)', line)
                override = find.group(1)
                list_ha.append(override)
                print("override: "+override)

                override_value = find.group(2)
                list_ha_value.append(override_value)
                print("override Value: "+override_value)
                break
        
        for line in read_file_list:
            #Primary
            if re.findall('^(Primary\s+):\s+(.*)', line):
                find = re.match('^(Primary\s+):\s+(.*)', line)
                primary = find.group(1)
                list_ha.append(primary)
                print("Primary: "+primary)

                primary_value = find.group(2)
                list_ha_value.append(primary_value)
                print("Primary Value: "+primary_value)
                break
        
        for line in read_file_list:
            #Secondary
            if re.findall('^(Secondary\s+):\s+(.*)', line):
                find = re.match('^(Secondary\s+):\s+(.*)', line)
                secondary = find.group(1)
                list_ha.append(secondary)
                print("Secondary: "+secondary)

                secondary_value = find.group(2)
                list_ha_value.append(secondary_value)
                print("Secondary Value: "+secondary_value)
                break
        
        for line in read_file_list:
            #number of vcluster
            if re.findall('^(number of vcluster):\s+(.*)', line):
                find = re.match('^(number of vcluster):\s+(.*)', line)
                num_cluster = find.group(1)
                list_ha.append(num_cluster)
                print("number of vcluster: "+num_cluster)

                num_clustery_value = find.group(2)
                list_ha_value.append(num_clustery_value)
                print("number of vcluster Value: "+num_clustery_value)
                break
        
        print("\n")

        #CPU UTILIZATION TABEL
        print("============================")
        print("CPU UTILIZATION TABEL")
        print("============================")
        list_cpu_state = []
        list_cpu_user = []
        list_cpu_system = []
        list_cpu_nice = []
        list_cpu_idle = []
        list_cpu_iowait = []
        list_cpu_irq = []
        list_cpu_softirq = []
        list_cpu_status = []
        for line in read_file_list:
            #CPU
            if re.findall('^(CPU|CPU\d+)\s+(states):\s+(\d+)%\s+user\s+(\d+)%\s+system\s+(\d+)%\s+nice\s+(\d+)%\s+idle\s+(\d+)%\s+iowait\s+(\d+)%\s+irq\s+(\d+)%\s+softirq', line):
                find = re.match('^(CPU|CPU\d+)\s+(states):\s+(\d+)%\s+user\s+(\d+)%\s+system\s+(\d+)%\s+nice\s+(\d+)%\s+idle\s+(\d+)%\s+iowait\s+(\d+)%\s+irq\s+(\d+)%\s+softirq', line)
                
                #CPU State
                cpu = find.group(1)
                state = find.group(2)
                cpu_state = cpu+" "+state
                list_cpu_state.append(cpu_state)
                print("CPU State: "+cpu_state)

                #CPU User
                cpu_user = find.group(3)
                list_cpu_user.append(cpu_user)
                print("CPU User: "+cpu_user)

                #CPU System
                cpu_system = find.group(4)
                list_cpu_system.append(cpu_system)
                print("CPU System: "+cpu_system)

                #CPU Nice
                cpu_nice = find.group(5)
                list_cpu_nice.append(cpu_nice)
                print("CPU Nice: "+cpu_nice)

                #CPU Idle
                cpu_idle = find.group(6)
                list_cpu_idle.append(cpu_idle)
                print("CPU Idle: "+cpu_idle)

                #CPU Iowait
                cpu_iowait = find.group(7)
                list_cpu_iowait.append(cpu_iowait)
                print("CPU Iowait: "+cpu_iowait)

                #CPU Irq
                cpu_irq = find.group(8)
                list_cpu_irq.append(cpu_irq)
                print("CPU Irq: "+cpu_irq)

                #CPU Softirq
                cpu_softirq = find.group(8)
                list_cpu_softirq.append(cpu_softirq)
                print("CPU Softirq: "+cpu_softirq)

                #CPU Status
                cpu_total = 100 - float(cpu_idle)
                if cpu_total<26 :
                    cpu_status = 'Low'
                elif cpu_total<76 :
                    cpu_status = 'Medium'
                else:
                    cpu_status = 'High'
                list_cpu_status.append(cpu_status)
                print("CPU Status: "+cpu_status)

                print(cpu_state+" "+cpu_user+"% user "+cpu_system+"% system "+cpu_nice+"% nice "+cpu_idle+"% idle "+cpu_iowait+"% iowait "+cpu_irq+"% irq "+cpu_softirq+"% softirq "+cpu_status+" CPU Status")
                print("\n")   

        #MEMORY UTILIZATION TABEL
        print("============================")
        print("MEMORY UTILIZATION TABEL")
        print("============================")
        for line in read_file_list:
            #Memory
            if re.findall('^Memory:\s+16449440k\s+total,\s+8998284k\s+used\s+\((\S+)%\), 4336436k\s+free\s+\((\S+)%\),\s+3114720k\s+freeable\s+\((\S+)%\)', line):
                find = re.match('^Memory:\s+16449440k\s+total,\s+8998284k\s+used\s+\((\S+)%\), 4336436k\s+free\s+\((\S+)%\),\s+3114720k\s+freeable\s+\((\S+)%\)', line)

                #Memory Used
                memory_used = find.group(1)
                print("Memory Used: "+memory_used)

                #Memory Free
                memory_free = find.group(2)
                print("Memory Free: "+memory_free)

                #Memory Freeable
                memory_freeable = find.group(3)
                print("Memory Freeable: "+memory_freeable)

                #Memory Status
                memory_used = float(memory_used)
                if memory_used<26 :
                    memory_status = 'Low'
                elif memory_used<76 :
                    memory_status = 'Medium'
                else:
                    memory_status = 'High'
                print("CPU Status: "+memory_status)
                break
        
        print("\n")

        #SESSION UTILIZATION TABEL
        print("============================")
        print("SESSION UTILIZATION TABEL")
        print("============================")
        list_session_type = []
        list_session_1 = []
        list_session_10 = []
        list_session_30 = []
        for line in read_file_list:
            #Average network usage
            if re.findall('^(Average.*):\s+(\d+\s+\/\s+\d+\s+kbps).*1\s+minute,\s+(\d+\s+\/\s+\d+\s+kbps).*10\s+minutes,\s+(\d+\s+\/\s+\d+\s+kbps).*30\s+minutes', line):
                find = re.match('^(Average.*):\s+(\d+\s+\/\s+\d+\s+kbps).*1\s+minute,\s+(\d+\s+\/\s+\d+\s+kbps).*10\s+minutes,\s+(\d+\s+\/\s+\d+\s+kbps).*30\s+minutes', line)
                
                #Session Type
                session_type = find.group(1)
                list_session_type.append(session_type)
                print("List Session Type: "+session_type)

                #Session 1 minutes
                session_1 = find.group(2)
                list_session_1.append(session_1)
                print("List Session 1 Minutes: "+session_1)

                #Session 10 minutes
                session_10 = find.group(3)
                list_session_10.append(session_10)
                print("List Session 10 Minutes: "+session_10)

                #Session 30 minutes
                session_30 = find.group(4)
                list_session_30.append(session_30)
                print("List Session 30 Minutes: "+session_30)
            
            #Average Other
            if re.findall('^(Average.*):\s+(\d+\s+sessions).*1\s+minute,\s+(\d+\s+sessions).*10\s+minutes,\s+(\d+\s+sessions).*30\s+minutes', line):
                find = re.match('^(Average.*):\s+(\d+\s+sessions).*1\s+minute,\s+(\d+\s+sessions).*10\s+minutes,\s+(\d+\s+sessions).*30\s+minutes', line)
                
                #Session Type
                session_type = find.group(1)
                list_session_type.append(session_type)
                print("List Session Type: "+session_type)

                #Session 1 minutes
                session_1 = find.group(2)
                list_session_1.append(session_1)
                print("List Session 1 Minutes: "+session_1)

                #Session 10 minutes
                session_10 = find.group(3)
                list_session_10.append(session_10)
                print("List Session 10 Minutes: "+session_10)

                #Session 30 minutes
                session_30 = find.group(4)
                list_session_30.append(session_30)
                print("List Session 30 Minutes: "+session_30)
        
        print("\n")

        #LICENSE TABEL
        print("============================")
        print("LICENSE TABEL")
        print("============================")
        list_lic_entitlement = []
        list_lic_contract_exp = []
        list_lic_last_update_schedule = []
        list_lic_last_update_attempt = []
        list_lic_result = []
        list_lic_status = []

        #Get AV Engine
        read_file_list_avengine  = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_avengine.append(line.strip())
            if 'AV Engine' in line:
                read_file_list_avengine .append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_avengine):
            if re.findall('^(AV\s+Engine)', line):
                entitlement = re.findall('^(AV\s+Engine)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("AV Engine: "+entitlement)
                
                if len(read_file_list_avengine) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_avengine[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_avengine[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_avengine[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_avengine[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_avengine[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_avengine[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_avengine[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_avengine[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
                
        print("\n")

        #Get Virus Definitions
        read_file_list_virusdev  = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_virusdev.append(line.strip())
            if 'Virus Definitions' in line:
                read_file_list_virusdev.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_virusdev):
            if re.findall('^(Virus\s+Definitions)', line):
                entitlement = re.findall('^(Virus\s+Definitions)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Virus Definitions: "+entitlement)
                
                if len(read_file_list_virusdev) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_virusdev[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_virusdev[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_virusdev[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_virusdev[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_virusdev[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_virusdev[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_virusdev[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_virusdev[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Extended set
        read_file_list_extenset  = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_extenset.append(line.strip())
            if 'Extended set' in line:
                read_file_list_extenset.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_extenset):
            if re.findall('^(Extended\s+set)', line):
                entitlement = re.findall('^(Extended\s+set)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Extended set: "+entitlement)
                
                if len(read_file_list_extenset) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_extenset[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_extenset[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_extenset[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_extenset[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_extenset[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_extenset[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_extenset[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_extenset[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Extreme set
        read_file_list_extremset  = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_extremset.append(line.strip())
            if 'Extreme set' in line:
                read_file_list_extremset.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_extremset):
            if re.findall('^(Extreme\s+set)', line):
                entitlement = re.findall('^(Extreme\s+set)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Extreme set: "+entitlement)
                
                if len(read_file_list_extremset) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_extremset[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_extremset[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_extremset[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_extremset[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_extremset[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_extremset[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_extremset[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_extremset[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Mobile Malware Definitions
        read_file_list_mobmal  = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_mobmal.append(line.strip())
            if 'Mobile Malware Definitions' in line:
                read_file_list_mobmal.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_mobmal):
            if re.findall('^(Mobile\s+Malware\s+Definitions)', line):
                entitlement = re.findall('^(Mobile\s+Malware\s+Definitions)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Mobile Malware Definitions: "+entitlement)
                
                if len(read_file_list_mobmal) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_mobmal[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_mobmal[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_mobmal[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_mobmal[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_mobmal[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_mobmal[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_mobmal[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_mobmal[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get IPS Attack Engine
        read_file_list_ipsattack  = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_ipsattack.append(line.strip())
            if 'IPS Attack Engine' in line:
                read_file_list_ipsattack.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_ipsattack):
            if re.findall('^(IPS\s+Attack\s+Engine)', line):
                entitlement = re.findall('^(IPS\s+Attack\s+Engine)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("IPS Attack Engine: "+entitlement)
                
                if len(read_file_list_ipsattack) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipsattack[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipsattack[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipsattack[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipsattack[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipsattack[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipsattack[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_ipsattack[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_ipsattack[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get IPS Config Script
        read_file_list_ipsconf  = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_ipsconf.append(line.strip())
            if 'IPS Config Script' in line:
                read_file_list_ipsconf.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_ipsconf):
            if re.findall('^(IPS\s+Config\s+Script)', line):
                entitlement = re.findall('^(IPS\s+Config\s+Script)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("IPS Config Script: "+entitlement)
                
                if len(read_file_list_ipsconf) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipsconf[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipsconf[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipsconf[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipsconf[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipsconf[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipsconf[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_ipsconf[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_ipsconf[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Attack Definitions
        read_file_list_attdef = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_attdef.append(line.strip())
            if 'Attack Definitions' in line:
                read_file_list_attdef.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_attdef):
            if re.findall('^(Attack\s+Definitions)', line):
                entitlement = re.findall('^(Attack\s+Definitions)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Attack Definitions: "+entitlement)
                
                if len(read_file_list_attdef) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_attdef[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_attdef[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_attdef[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_attdef[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_attdef[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_attdef[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_attdef[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_attdef[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Attack Extended Definitions
        read_file_list_attext = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_attext.append(line.strip())
            if 'Attack Extended Definitions' in line:
                read_file_list_attext.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_attext):
            if re.findall('^(Attack\s+Extended\s+Definitions)', line):
                entitlement = re.findall('^(Attack\s+Extended\s+Definitions)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Attack Extended Definitions: "+entitlement)
                
                if len(read_file_list_attext) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_attext[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_attext[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_attext[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_attext[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_attext[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_attext[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_attext[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_attext[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Application Definitions
        read_file_list_appdef = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_appdef.append(line.strip())
            if 'Application Definitions' in line:
                read_file_list_appdef.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_appdef):
            if re.findall('^(Application\s+Definitions)', line):
                entitlement = re.findall('^(Application\s+Definitions)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Application Definitions: "+entitlement)
                
                if len(read_file_list_appdef) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_appdef[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_appdef[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_appdef[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_appdef[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_appdef[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_appdef[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_appdef[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_appdef[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Industrial Attack Definitions
        read_file_list_indatt = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_indatt.append(line.strip())
            if 'Industrial Attack Definitions' in line:
                read_file_list_indatt.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_indatt):
            if re.findall('^(Industrial\s+Attack\s+Definitions)', line):
                entitlement = re.findall('^(Industrial\s+Attack\s+Definitions)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Industrial Attack Definitions: "+entitlement)
                
                if len(read_file_list_indatt) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_indatt[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_indatt[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_indatt[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_indatt[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_indatt[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_indatt[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_indatt[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_indatt[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get IPS Malicious URL Database
        read_file_list_ipsmal = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_ipsmal.append(line.strip())
            if 'IPS Malicious URL Database' in line:
                read_file_list_ipsmal.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_ipsmal):
            if re.findall('^(IPS\s+Malicious\s+URL\s+Database)', line):
                entitlement = re.findall('^(IPS\s+Malicious\s+URL\s+Database)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("IPS Malicious URL Database: "+entitlement)
                
                if len(read_file_list_ipsmal) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipsmal[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipsmal[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipsmal[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipsmal[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipsmal[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipsmal[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_ipsmal[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_ipsmal[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Flow-based Virus Definitions
        read_file_list_flowbas = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_flowbas.append(line.strip())
            if 'Flow-based Virus Definitions' in line:
                read_file_list_flowbas.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_flowbas):
            if re.findall('^(Flow-based\s+Virus\s+Definitions)', line):
                entitlement = re.findall('^(Flow-based\s+Virus\s+Definitions)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Flow-based Virus Definitions: "+entitlement)
                
                if len(read_file_list_flowbas) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_flowbas[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_flowbas[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_flowbas[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_flowbas[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_flowbas[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_flowbas[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_flowbas[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_flowbas[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Botnet Domain Database
        read_file_list_botnet = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_botnet.append(line.strip())
            if 'Botnet Domain Database' in line:
                read_file_list_botnet.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_botnet):
            if re.findall('^(Botnet\s+Domain\s+Database)', line):
                entitlement = re.findall('^(Botnet\s+Domain\s+Database)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Botnet Domain Database: "+entitlement)
                
                if len(read_file_list_botnet) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_botnet[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_botnet[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_botnet[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_botnet[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_botnet[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_botnet[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_botnet[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_botnet[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")

        #Get Internet-service Database Apps
        read_file_list_intapp = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_intapp.append(line.strip())
            if 'Internet-service Database Apps' in line:
                read_file_list_intapp.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_intapp):
            if re.findall('^(Internet-service\s+Database\s+Apps)', line):
                entitlement = re.findall('^(Internet-service\s+Database\s+Apps)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Internet-service Database Apps: "+entitlement)
                
                if len(read_file_list_intapp) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_intapp[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_intapp[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_intapp[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_intapp[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_intapp[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_intapp[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_intapp[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_intapp[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Internet-service Database Maps
        read_file_list_intmaps = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_intmaps.append(line.strip())
            if 'Internet-service Database Maps' in line:
                read_file_list_intmaps.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_intmaps):
            if re.findall('^(Internet-service\s+Database\s+Maps)', line):
                entitlement = re.findall('^(Internet-service\s+Database\s+Maps)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Internet-service Database Maps: "+entitlement)
                
                if len(read_file_list_intmaps) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_intmaps[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_intmaps[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_intmaps[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_intmaps[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_intmaps[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_intmaps[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_intmaps[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_intmaps[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Device and OS Identification
        read_file_list_deviden = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_deviden.append(line.strip())
            if 'Device and OS Identification' in line:
                read_file_list_deviden.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_deviden):
            if re.findall('^(Device\s+and\s+OS\s+Identification)', line):
                entitlement = re.findall('^(Device\s+and\s+OS\s+Identification)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Device and OS Identification: "+entitlement)
                
                if len(read_file_list_deviden) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_deviden[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_deviden[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_deviden[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_deviden[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_deviden[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_deviden[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_deviden[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_deviden[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get URL White list
        read_file_list_urlwhite = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_urlwhite.append(line.strip())
            if 'URL White list' in line:
                read_file_list_urlwhite.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_urlwhite):
            if re.findall('^(URL\s+White\s+list)', line):
                entitlement = re.findall('^(URL\s+White\s+list)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("URL White list: "+entitlement)
                
                if len(read_file_list_urlwhite) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_urlwhite[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_urlwhite[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_urlwhite[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_urlwhite[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_urlwhite[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_urlwhite[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_urlwhite[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_urlwhite[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get IP Geography DB
        read_file_list_ipgeo = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_ipgeo.append(line.strip())
            if 'IP Geography DB' in line:
                read_file_list_ipgeo.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_ipgeo):
            if re.findall('^(IP\s+Geography\s+DB)', line):
                entitlement = re.findall('^(IP\s+Geography\s+DB)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("IP Geography DB: "+entitlement)
                
                if len(read_file_list_ipgeo) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipgeo[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_ipgeo[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipgeo[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_ipgeo[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipgeo[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_ipgeo[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_ipgeo[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_ipgeo[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Certificate Bundle
        read_file_list_certbun = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_certbun.append(line.strip())
            if 'Certificate Bundle' in line:
                read_file_list_certbun.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_certbun):
            if re.findall('^(Certificate\s+Bundle)', line):
                entitlement = re.findall('^(Certificate\s+Bundle)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Certificate Bundle: "+entitlement)
                
                if len(read_file_list_certbun) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_certbun[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_certbun[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_certbun[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_certbun[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_certbun[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_certbun[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_certbun[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_certbun[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get Malicious Certificate DB
        read_file_list_malcert = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_malcert.append(line.strip())
            if 'Malicious Certificate DB' in line:
                read_file_list_malcert.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1
        
        line_read = 0
        for enum, line in enumerate(read_file_list_malcert):
            if re.findall('^(Malicious\s+Certificate\s+DB)', line):
                entitlement = re.findall('^(Malicious\s+Certificate\s+DB)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Malicious Certificate DB: "+entitlement)
                
                if len(read_file_list_malcert) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_malcert[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_malcert[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_malcert[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_malcert[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_malcert[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_malcert[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_malcert[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_malcert[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")

        #Get Modem List
        read_file_list_mademlist = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_mademlist.append(line.strip())
            if 'Modem List' in line:
                read_file_list_mademlist.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1

        line_read = 0
        for enum, line in enumerate(read_file_list_mademlist):
            if re.findall('^(Modem\s+List)', line):
                entitlement = re.findall('^(Modem\s+List)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("Modem List: "+entitlement)
                
                if len(read_file_list_mademlist) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_mademlist[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_mademlist[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_mademlist[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_mademlist[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_mademlist[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_mademlist[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_mademlist[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_mademlist[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        print("\n")
        
        #Get FDS Address
        read_file_list_fdsadd = []
        read_file_logic_check = False
        count_read_file = 0
        for line in read_file_list:
            if read_file_logic_check == True and re.findall('^\s*$', line):
                break
            if read_file_logic_check == True:
                read_file_list_fdsadd.append(line.strip())
            if 'FDS Address' in line:
                read_file_list_fdsadd.append(line.strip())
                read_file_logic_check = True
            count_read_file+=1

        line_read = 0
        for enum, line in enumerate(read_file_list_fdsadd):
            if re.findall('^(FDS\s+Address)', line):
                entitlement = re.findall('^(FDS\s+Address)', line)
                entitlement =  entitlement[0]
                list_lic_entitlement.append(entitlement)
                print("FDS Address: "+entitlement)
                
                if len(read_file_list_fdsadd) == 7:
                    if re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_fdsadd[line_read+3]):
                        contract_exp = re.findall('^Contract\s+Expiry\s+Date:\s+(.*)', read_file_list_fdsadd[line_read+3])
                        contract_exp =  contract_exp[0]
                        list_lic_contract_exp.append(contract_exp)
                        print("Contract Expiry Date: "+contract_exp)
                    
                    if re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_fdsadd[line_read+4]):
                        last_update_schedule = re.findall('^Last\s+Updated\s+using\s+\S+\s+update\s+on\s+(.*)', read_file_list_fdsadd[line_read+4])
                        last_update_schedule =  last_update_schedule[0]
                        list_lic_last_update_schedule.append(last_update_schedule)
                        print("Last Updated using scheduled update on: "+last_update_schedule)
                             
                    if re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_fdsadd[line_read+5]):
                        last_update_attempt = re.findall('^Last\s+Update\s+Attempt:\s+(.*)', read_file_list_fdsadd[line_read+5])
                        last_update_attempt =  last_update_attempt[0]
                        list_lic_last_update_attempt.append(last_update_attempt)
                        print("Last Update Attempt: "+last_update_attempt)                      
                        
                    if re.findall('^Result:\s+(.*)', read_file_list_fdsadd[line_read+6]):
                        result = re.findall('^Result:\s+(.*)', read_file_list_fdsadd[line_read+6])
                        result =  result[0]
                        list_lic_result.append(result)
                        print("Result: "+result)

                else:
                    contract_exp = 'n/a'
                    list_lic_contract_exp.append(contract_exp)
                    print("Contract Expiry Date: "+contract_exp)

                    last_update_schedule = 'n/a'
                    list_lic_last_update_schedule.append(last_update_schedule)
                    print("Last Updated using scheduled update on: "+last_update_schedule)

                    last_update_attempt = 'n/a'
                    list_lic_last_update_attempt.append(last_update_attempt)
                    print("Last Update Attempt: "+last_update_attempt)

                    result = 'n/a'
                    list_lic_result.append(result)
                    print("Result: "+result)
        
        #GET DATE NOW
        date_now = datetime.datetime.now()
        date_now_convert = date_now.date()

        #LICENSE STATUS
        for date in list_lic_contract_exp:
            if date == 'n/a':
                lic_status = "n/a"
                list_lic_status.append(lic_status)
            else:
                #Split String Spasi
                date = date.split()
                date = (date[1]+"-"+date[2]+"-"+date[3])

                #Convert String to Date
                date = datetime.datetime.strptime(date, "%b-%d-%Y")
                date = date.date()

                if date <= date_now_convert:
                    lic_status = "Expired"
                    list_lic_status.append(lic_status)
                else:
                    lic_status = "Not Expired"
                    list_lic_status.append(lic_status)

        #CERTIFICATE TABEL
        print("============================")
        print("CERTIFICATE TABEL")
        print("============================")
        list_cert_name = []
        list_cert_subject = []
        list_cert_issuer = []
        list_cert_validto = []
        list_cert_status = []
        for line in read_file_list:
            #Certificate Name
            if re.findall('^Name:\s+(.*)',line):
                cert_name = re.findall('^Name:\s+(.*)',line)
                cert_name = cert_name[0]
                cert_name = cert_name
                list_cert_name.append(cert_name)
                print("Certificate Name: "+cert_name)
            
            #Certificate Subject
            if re.findall('^Subject:\s+(.*)',line):
                cert_subject = re.findall('^Subject:\s+(.*)',line)
                cert_subject = cert_subject[0]
                cert_subject = cert_subject
                list_cert_subject.append(cert_subject)
                print("Certificate Subject: "+cert_subject)

            #Certificate Issuer
            if re.findall('^Issuer:\s+(.*)',line):
                cert_issuer = re.findall('^Issuer:\s+(.*)',line)
                cert_issuer = cert_issuer[0]
                cert_issuer = cert_issuer
                list_cert_issuer.append(cert_issuer)
                print("Certificate Issuer: "+cert_issuer)

            #Certificate Valid to
            if re.findall('^Valid\s+to:\s+(.*)',line):
                cert_validto = re.findall('^Valid\s+to:\s+(.*)',line)
                cert_validto = cert_validto[0]
                cert_validto = cert_validto
                list_cert_validto.append(cert_validto)
                print("Certificate Valid to: "+cert_validto)

                #GET DATE NOW
                date_now = datetime.datetime.now()
                date_now_convert = date_now.date()

                #CERTIFICATE STATUS
                #Split String Spasi
                cert_validto = cert_validto.split()
                cert_validto = (cert_validto[0])
                cert_validto = cert_validto.split('-')
                cert_validto = (cert_validto[1]+'-'+cert_validto[2]+'-'+cert_validto[0])

                #Convert String to Date
                cert_validto = datetime.datetime.strptime(cert_validto, "%m-%d-%Y")
                cert_validto = cert_validto.date()

                if cert_validto <= date_now_convert:
                    cert_status = "Expired"
                    list_cert_status.append(cert_status)
                else:
                    cert_status = "Not Expired"
                    list_cert_status.append(cert_status)
                print("Certificate Status: "+cert_status)
                print("\n")
        
        print("\n")

        device_class_name = self.__class__.__name__
        file_name = self.file

        #TIPE DEVICES SUMMARY TABLE
        tipe_list = []
        if 'FortiGate' in model_value:
            tipe = 'FortiGate'
            tipe_list.append(tipe)
            print("tipe: "+tipe)
        else:
            tipe = 'Other'
            tipe_list.append(tipe)
            print("tipe: "+tipe)
        
        func_insert_dbreport_fortigate = insert_dbreport_fortigate(
                devicename, model, version, iosversion, confreg, uptime, sn_value,
                list_system, list_system_value, date_eos_sw,
                list_ha, list_ha_value,
                list_cpu_state, list_cpu_user, list_cpu_system, list_cpu_nice, list_cpu_idle, list_cpu_iowait, list_cpu_irq, list_cpu_softirq, list_cpu_status,
                memory_used, memory_free, memory_freeable, memory_status,
                list_session_type, list_session_1, list_session_10, list_session_30,
                list_lic_entitlement, list_lic_contract_exp, list_lic_last_update_schedule, list_lic_last_update_attempt, list_lic_result, list_lic_status,
                list_cert_name, list_cert_subject, list_cert_issuer, list_cert_validto, list_cert_status,
                tipe_list, model_value,
                device_class_name,
                file_name
        )
        execute_insert_dbreport_fortigate = func_insert_dbreport_fortigate.insert_dbreport_fortigate()
        
        