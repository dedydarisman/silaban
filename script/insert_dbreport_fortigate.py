import sqlite3

class insert_dbreport_fortigate:
    def __init__(self,
                devicename, model, version, iosversion, confreg, uptime, sn_value,
                list_system, list_system_value, date_eos_sw,
                list_ha, list_ha_value,
                list_cpu_state, list_cpu_user, list_cpu_system, list_cpu_nice, list_cpu_idle, list_cpu_iowait, list_cpu_irq, list_cpu_softirq, list_cpu_status,
                memory_used, memory_free, memory_freeable, memory_status,
                list_session_type, list_session_1, list_session_10, list_session_30,
                list_lic_entitlement, list_lic_contract_exp, list_lic_last_update_schedule, list_lic_last_update_attempt, list_lic_result, list_lic_status,
                list_cert_name, list_cert_subject, list_cert_issuer, list_cert_validto, list_cert_status,
                tipe_list, model_value,
                device_class_name,
                file_name
    ):
        self.devicename = devicename
        self.model = model
        self.iosversion = iosversion
        self.uptime = uptime
        self.sn_value = sn_value
        self.confreg = confreg
        self.version = version
        self.list_system = list_system
        self.list_system_value = list_system_value
        self.date_eos_sw = date_eos_sw
        self.list_ha = list_ha
        self.list_ha_value = list_ha_value
        self.list_cpu_state = list_cpu_state
        self.list_cpu_user = list_cpu_user
        self.list_cpu_system = list_cpu_system
        self.list_cpu_nice = list_cpu_nice
        self.list_cpu_idle = list_cpu_idle
        self.list_cpu_iowait = list_cpu_iowait
        self.list_cpu_irq = list_cpu_irq
        self.list_cpu_softirq = list_cpu_softirq
        self.list_cpu_status = list_cpu_status
        self.memory_used = memory_used
        self.memory_free = memory_free
        self.memory_freeable = memory_freeable
        self.memory_status = memory_status
        self.list_session_type = list_session_type
        self.list_session_1 = list_session_1
        self.list_session_10 = list_session_10
        self.list_session_30 = list_session_30
        self.list_lic_entitlement = list_lic_entitlement
        self.list_lic_contract_exp = list_lic_contract_exp
        self.list_lic_last_update_schedule = list_lic_last_update_schedule
        self.list_lic_last_update_attempt = list_lic_last_update_attempt
        self.list_lic_result = list_lic_result
        self.list_lic_status = list_lic_status
        self.list_cert_name = list_cert_name
        self.list_cert_subject = list_cert_subject
        self.list_cert_issuer = list_cert_issuer
        self.list_cert_validto = list_cert_validto
        self.list_cert_status = list_cert_status
        self.tipe_list = tipe_list
        self.model_value = model_value
        self.device_class_name = device_class_name
        self.file_name = file_name


    def insert_dbreport_fortigate(self):
        #open db connection
        db = sqlite3.connect('pmdb_fortigate')
        cursor = db.cursor()

        #DB SOFTWARE TABLE
        try:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, self.model_value, self.version, self.uptime, self.confreg, self.version, self.date_eos_sw, self.date_eos_sw))
        except:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        try:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.version, self.date_eos_sw, self.date_eos_sw))
        except:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB SYSTEM TABLE
        for enum, system in enumerate(self.list_system):
            try:
                cursor.execute('''INSERT INTO systematable(devicename, system, system_value)
                        VALUES(?,?,?)''', (self.devicename, system, self.list_system_value[enum],))
            except:
                cursor.execute('''INSERT INTO systematable(devicename, system, system_value)
                        VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB HA TABLE
        for enum, ha in enumerate(self.list_ha):
            try:
                cursor.execute('''INSERT INTO hatable(devicename, ha, ha_value)
                        VALUES(?,?,?)''', (self.devicename, ha, self.list_ha_value[enum],))
            except:
                cursor.execute('''INSERT INTO hatable(devicename, device, mgmt_ip, configsync_ip, ha_state, ha_status)
                        VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #DB CPU SUMMARY TABLE
        for enum, state in enumerate(self.list_cpu_state):
            try:
                cursor.execute('''INSERT INTO cputable(devicename, cpu_state, cpu_user, cpu_system, cpu_nice, cpu_idle, cpu_iowait, cpu_irq, cpu_softirq, cpu_status)
                        VALUES(?,?,?,?,?,?,?,?,?,?)''', (self.devicename, state, self.list_cpu_user[enum], self.list_cpu_system[enum], self.list_cpu_nice[enum], self.list_cpu_idle[enum], self.list_cpu_iowait[enum], self.list_cpu_irq[enum], self.list_cpu_softirq[enum], self.list_cpu_status[enum],))
            except:
                cursor.execute('''INSERT INTO cputable(devicename, cpu_state, cpu_user, cpu_system, cpu_nice, cpu_idle, cpu_iowait, cpu_irq, cpu_softirq)
                        VALUES(?,?,?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))            
        
        #DB MEMORY SUMMARY TABLE
        try:
            cursor.execute('''INSERT INTO memtable(devicename, memory_used, memory_free, memory_freeable, memory_status)
                    VALUES(?,?,?,?,?)''', (self.devicename, self.memory_used, self.memory_free, self.memory_freeable, self.memory_status,))
        except:
            cursor.execute('''INSERT INTO memtable(devicename, memory_used, memory_free, memory_freeable)
                    VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB SESSION TABLE
        for enum, session in enumerate(self.list_session_type):
            try:
                cursor.execute('''INSERT INTO sessiontable(devicename, session_type, session_1, session_10, session_30)
                        VALUES(?,?,?,?,?)''', (self.devicename, session, self.list_session_1[enum], self.list_session_10[enum], self.list_session_30[enum],))
            except:
                cursor.execute('''INSERT INTO sessiontable(devicename, session_type, session_1, session_10, session_30)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB LICENSE SUMMARY TABLE
        for enum, entitlement in enumerate(self.list_lic_entitlement):
            try:
                cursor.execute('''INSERT INTO lictable(devicename, lic_entitlement, lic_contract_exp, lic_last_update_schedule, lic_last_update_attempt, lic_result, lic_status)
                        VALUES(?,?,?,?,?,?,?)''', (self.devicename, entitlement, self.list_lic_contract_exp[enum], self.list_lic_last_update_schedule[enum], self.list_lic_last_update_attempt[enum], self.list_lic_result[enum], self.list_lic_status[enum],))
            except:
                cursor.execute('''INSERT INTO lictable(devicename, lic_entitlement, lic_contract_exp, lic_last_update_schedule, lic_last_update_attempt, lic_result, lic_status)
                        VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB CERTIFICATE TABLE
        for enum, cert in enumerate(self.list_cert_name):
            try:
                cursor.execute('''INSERT INTO certtable(devicename, cert_name, cert_subject, cert_issuer, cert_validto, cert_status)
                        VALUES(?,?,?,?,?,?)''', (self.devicename, cert, self.list_cert_subject[enum], self.list_cert_issuer[enum], self.list_cert_validto[enum], self.list_cert_status[enum],))
            except:
                cursor.execute('''INSERT INTO certtable(devicename, cert_name, cert_subject, cert_issuer, cert_validto)
                        VALUES(?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #DB TIPE TABLE
        try:
            for enum, tipe in enumerate(self.tipe_list):
                cursor.execute('''INSERT INTO tipetable(tipe)
                        VALUES(?)''', (tipe,))
        except:
            cursor.execute('''INSERT INTO tipetable(tipe)
                    VALUES(?)''', (self.file_name+'-'+'error',))

        #DB LOG GENERATE REPORT
        try:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.devicename, self.model_value, self.device_class_name,))
        except:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',))
        
        #DB PORTAL TABLE
        try:
            cursor.execute('''INSERT INTO portaltable(devicename, model, card, sn, version)
                    VALUES(?,?,?,?,?)''', (self.devicename, self.model_value, self.model_value, self.sn_value, self.version,))
        except:
            cursor.execute('''INSERT INTO portaltable(devicename, model, card, sn, version)
                    VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',))
        
        db.commit()             
        db.close()