import sqlite3
import re
import requests

class insert_dbreport_rs:
    def __init__(self,
                version, devicename, model, iosversion, uptime, confreg, date_eos, date_last,
                card, list_serial_number, list_hardware_description, date_list,
                total, process, interrupt, topcpu, status,utils, topproc,
                memory_status, psu, list_psu, list_psu_cond, fan, list_fan, list_fan_cond_cp,
                list_temp, list_temp_cond, eos_date, device_class_name, eos_card, file_name, list_card, tipe_list,
                sn, ip
    ):
        self.version = version
        self.devicename = devicename
        self.model = model
        self.iosversion = iosversion
        self.uptime = uptime
        self.confreg = confreg
        self.date_eos = date_eos
        self.date_last = date_last
        self.card = card
        self.list_serial_number = list_serial_number
        self.list_hardware_description = list_hardware_description
        self.date_list = date_list
        self.total = total
        self.process = process
        self.interrupt = interrupt
        self.topcpu = topcpu
        self.status = status
        self.utils = utils
        self.topproc = topproc
        self.memory_status = memory_status
        self.psu = psu
        self.list_psu = list_psu
        self.list_psu_cond = list_psu_cond
        self.fan = fan
        self.list_fan = list_fan
        self.list_fan_cond_cp = list_fan_cond_cp
        self.list_temp = list_temp
        self.list_temp_cond = list_temp_cond
        self.eos_card = eos_card
        self.device_class_name = device_class_name
        self.file_name = file_name
        self.list_card = list_card
        self.eos_date = eos_date
        self.tipe_list = tipe_list
        self.sn = sn
        self.ip = ip

    def insert_dbreport_rs(self):
        #open db connection
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()
        #db tipe
        try:
            for enum, tipe in enumerate(self.tipe_list):
                cursor.execute('''INSERT INTO tipetable(tipe)
                        VALUES(?)''', (tipe,))
        except:
            cursor.execute('''INSERT INTO tipetable(tipe)
                    VALUES(?)''', (self.file_name+'-'+'error',))
        
        #db software
        try:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.devicename, self.model, self.version, self.uptime, self.confreg, self.version, self.date_eos, self.date_last))
        except:
            cursor.execute('''INSERT INTO swtable(devicename, model, iosversion, uptime, confreg, version, date_eos, date_last)
                    VALUES(?,?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        try:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.version, self.date_eos, self.date_last))
        except:
            cursor.execute('''INSERT INTO swsumtable(version, date_eos, date_last)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
        
        #db hardware
        try:
            cursor.execute('''INSERT INTO hwsumtable(model)
                    VALUES(?)''', (self.model,))
            count_sql = 0
        except:
            cursor.execute('''INSERT INTO hwsumtable(model)
                    VALUES(?)''', (self.file_name+'-'+'error',))
            count_sql = 0
        try:
            for enum, card in enumerate(self.list_card):
                cursor.execute('''INSERT INTO hwcardtable(devicename, model, card, sn, hwdscr, eosl_date)
                        VALUES(?,?,?,?,?,?)''', (self.devicename,self.model, card,self.list_serial_number[enum],self.list_hardware_description[enum], self.date_list[enum]))
        except:
            cursor.execute('''INSERT INTO hwcardtable(devicename, model, card, sn, hwdscr, eosl_date)
                    VALUES(?,?,?,?,?,?)''', (self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',))
        #db process cpu and memory
        # print(self.status)
        # tulas = input("DB STATUS")
        try:
            self.status + "String"
            try:
                cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                        VALUES(?,?,?,?,?,?,?)''', (self.devicename, self.model, self.total, self.process, self.interrupt, self.topcpu, self.status,))
            except:
                cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                        VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))            
            # print("Kena1")
        except:
            try:
                for enum, card in enumerate(self.process):
                        cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                            VALUES(?,?,?,?,?,?,?)''', (self.devicename, self.model, self.total[enum], self.process[enum], self.interrupt[enum], self.topcpu[enum], self.status[enum],))
            except:
                cursor.execute('''INSERT INTO cpusumtable(devicename, model, total, process, interrupt, topcpu, status)
                        VALUES(?,?,?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
            # print("Kena2")

        #memory
        try:
            self.topproc + "String"
            try:
                cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                        VALUES(?,?,?,?,?)''', (self.devicename, self.model, self.utils, self.topproc, self.memory_status,))
            except:
                cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
            # print("Kena1")
        except:
            try:
                for enum, card in enumerate(self.topproc):
                    cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                            VALUES(?,?,?,?,?)''', (self.devicename, self.model, self.utils[enum], self.topproc[enum], self.memory_status[enum],))
            except:
                cursor.execute('''INSERT INTO memsumtable(devicename, model, utils, topproc, status)
                        VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))
            # print("Kena2")
        
        # tulaa = input("KENAAA")
        
        #db environment
        try:
            count_sql = 0
            for psu in self.list_psu:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Power Supply',psu,self.list_psu_cond[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for psu in self.list_psu:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Power Supply',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
        try:
            count_sql = 0
            for fan in self.list_fan:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Fan',fan,self.list_fan_cond_cp[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for fan in self.list_fan:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Fan',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
        try:
            count_sql = 0
            for temp in self.list_temp:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.devicename,'Temperature',temp,self.list_temp_cond[count_sql],))
                count_sql+=1
        except:
            count_sql = 0
            for temp in self.list_temp:
                cursor.execute('''INSERT INTO envtable(devicename, system, item, status)
                        VALUES(?,?,?,?)''', (self.file_name+'-'+'error','Temperature',self.file_name+'-'+'error',self.file_name+'-'+'error',))
                count_sql+=1
        #db eos
        try:
            for enum, card in enumerate(self.eos_card):
                cursor.execute('''INSERT INTO eosltable(model, card, date)
                        VALUES(?,?,?)''', (self.model, card, self.eos_date[enum],))
        except:
            cursor.execute('''INSERT INTO eosltable(model, card, date)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error', self.file_name+'-'+'error',))

        #LOG Checking
        try:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.devicename, self.model, self.device_class_name,))
        except:
            cursor.execute('''INSERT INTO logtable(devicename, model, script)
                    VALUES(?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',))

        #Aset
        try:
            for enum, tipe in enumerate(self.tipe_list):
                cursor.execute('''INSERT INTO aset(devicename, model, sn, tipe, ip)
                        VALUES(?,?,?,?,?)''', (self.devicename, self.model, self.sn, tipe, self.ip,))
        except:
            cursor.execute('''INSERT INTO aset(devicename, model, sn, tipe, ip)
                    VALUES(?,?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',))
        
        #File Rating
        file_rating = 0
        missing_section = []
        if self.version == "-":
            file_rating+=1
            missing_section.append("version")
        if self.devicename == "-":
            file_rating+=1
            missing_section.append("devicename")
        if self.model == "-":
            file_rating+=1
            missing_section.append("model")
        if self.iosversion == "-":
            file_rating+=1
            missing_section.append("iosversion")
        if self.uptime == "-":
            file_rating+=1
            missing_section.append("uptime")
        if self.confreg == "-":
            file_rating+=1
            missing_section.append("confreg")
        if self.card == "-":
            file_rating+=1
            missing_section.append("card")
        if self.list_serial_number == "-":
            file_rating+=1
            missing_section.append("list_serial_number")
        if self.list_hardware_description == "-":
            file_rating+=1
            missing_section.append("list_hardware_description")
        if self.date_list == "-":
            file_rating+=1
            missing_section.append("date_list")
        if self.total == "-":
            file_rating+=1
            missing_section.append("total")
        if self.process == "-":
            file_rating+=1
            missing_section.append("process")
        if self.interrupt == "-":
            file_rating+=1
            missing_section.append("interrupt")
        if self.topcpu == "-":
            file_rating+=1
            missing_section.append("topcpu")
        if self.status == "-":
            file_rating+=1
            missing_section.append("status")
        if self.utils == "-":
            file_rating+=1
            missing_section.append("utils")
        if self.topproc == "-":
            file_rating+=1
            missing_section.append("topproc")
        if self.memory_status == "-":
            file_rating+=1
            missing_section.append("memory_status")
        if self.psu == "-":
            file_rating+=1
            missing_section.append("psu")
        if self.list_psu == "-":
            file_rating+=1
            missing_section.append("list_psu")
        if self.list_psu_cond == "-":
            file_rating+=1
            missing_section.append("list_psu_cond")
        if self.fan == "-":
            file_rating+=1
            missing_section.append("fan")
        if self.list_fan == "-":
            file_rating+=1
            missing_section.append("list_fan")
        if self.list_fan_cond_cp == "-":
            file_rating+=1
            missing_section.append("list_fan_cond_cp")
        if self.list_temp == "-":
            file_rating+=1
            missing_section.append("list_temp")
        if self.list_temp_cond == "-":
            file_rating+=1
            missing_section.append("list_temp_cond")
        if self.eos_card == "-":
            file_rating+=1
            missing_section.append("eos_card")
        if self.device_class_name == "-":
            file_rating+=1
            missing_section.append("device_class_name")
        if self.file_name == "-":
            file_rating+=1
            missing_section.append("file_name")
        if self.list_card == "-":
            file_rating+=1
            missing_section.append("list_card")
        if self.eos_date == "-":
            file_rating+=1
            missing_section.append("eos_date")
        if self.tipe_list == "-":
            file_rating+=1
            missing_section.append("tipe_list")
        join_missing_section = ",".join(missing_section)
        print(file_rating)
        print(join_missing_section)
        print(self.file_name)
        print(self.device_class_name)
        try:
            cursor.execute('''INSERT INTO filerating(filename, deviceclassname, missingsection, totalrating)
                    VALUES(?,?,?,?)''', (self.file_name, self.device_class_name, join_missing_section, file_rating,))
        except:
            cursor.execute('''INSERT INTO filerating(filename, deviceclassname, missingsection, totalrating)
                    VALUES(?,?,?,?)''', (self.file_name+'-'+'error', self.file_name+'-'+'error',self.file_name+'-'+'error',self.file_name+'-'+'error',))

        db.commit()             
        db.close()