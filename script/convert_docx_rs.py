from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor
from docx.enum.table import WD_ROW_HEIGHT_RULE


data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    tblHeader = OxmlElement("w:tblHeader")
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_rs:
    #@staticmethod
    def __init__(self, customer, review, prepared, pic, bulan, tahun):
        #variable constructor
        self.customer = customer
        self.review = review
        self.prepared = prepared
        self.pic = pic
        self.bulan = bulan
        self.tahun = tahun

    def convert_docx_rs(self):
        print('')
        print('Processing Document')

        #open db connection
        os.chdir(capture_path)
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()
        #using document docx module
        report_template = open(data_path+'content_dispusip.docx', 'rb')
        document = Document(report_template)

        red = RGBColor(255, 0, 0)

        #INVENTORI PERANGKAT JARINGAN
        p = document.add_paragraph('INVENTORI PERANGKAT JARINGAN')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('GRAFIK JENIS PERANGKAT JARINGAN')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_software = (capture_path+'/Jenis Perangkat Summary.png')
        document.add_picture(img_software, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        #TABEL RANGKUMAN INVENTORI
        #sql query
        cursor.execute('''SELECT tipe, COUNT(*) FROM aset GROUP BY tipe''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(tipe) FROM aset''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=3)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.4)
        hdr_cells[1].width = Cm(1.29)
        hdr_cells[2].width = Cm(2.75)

        hdr_cells[0].text = 'Jenis Perangkat'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Pesentase'      
        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.4)
            row_cells[1].width = Cm(1.29)
            row_cells[2].width = Cm(2.75)

            row_cells[1].height_rule = WD_ROW_HEIGHT.EXACTLY
            # row.height
            row_cells[1].height = Cm(0.8)
            row_cells[2].height = Cm(0.8)


            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[1]) 
            percentage_str = (str((row[1]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)

            row_cells[2].text = (percentage+'%')

        document.add_paragraph()
        p = document.add_paragraph('TABEL INVENTORI PERANGKAT JARINGAN')
        p.style = document.styles['Heading 2']

        #TABEL INVENTORY
        #sql query
        cursor.execute("SELECT devicename, model, sn, tipe, ip FROM aset ORDER by tipe ASC")
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.69)
        hdr_cells[1].width = Cm(3.75)
        hdr_cells[2].width = Cm(2.58)
        hdr_cells[3].width = Cm(3.67)
        hdr_cells[4].width = Cm(2.5)

        hdr_cells[0].text = 'Nama Perangkat'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Nomor Seri'
        hdr_cells[3].text = 'Jenis Perangkat'
        hdr_cells[4].text = 'IP Address'
        for row in records:
            row_cells = table.add_row().cells
            
            row_cells[0].width = Cm(3.69)
            row_cells[1].width = Cm(3.75)
            row_cells[2].width = Cm(2.58)
            row_cells[3].width = Cm(3.67)
            row_cells[4].width = Cm(2.5)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #TABEL INVENTORI Explanation   
        #Keterangan INVENTORI
        document.add_paragraph()
        data_aset = (
            ('Nama Perangkat', '=' ,'Kolom nama perangkat menunjukkan nama perangkat (hostname).'),
            ('Model', '=' , 'Model menunjukkan tipe perangkat.'),
            ('Nomor Seri', '=' , 'Kolom nomor seri mengacu pada identitas unik dari setiap modul yang ada pada perangkat keras.'),
            ('Jenis Perangkat', '=' , 'Jenis perangkat menunjukkan klasifikasi dari perangkat jaringan.'),
            ('IP Address', '=' , 'IP Address menunjukkan alamat IP dari perangkat jaringan.'),
            ('Total', '=' , 'Total menunjukkan total dari versi firmware yang digunakan.'),
            ('Persentase', '=' , 'Persentase menunjukkan persentase dari versi firmware.')
        )
        table_aset = document.add_table(rows=1, cols=3)
        table_aset.allow_autofit = True
        table_aset.style = 'Keterangan'
        hdr_cells = table_aset.rows[0].cells
        table_aset.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.19)
        hdr_cells[1].width = Cm(0.75)
        hdr_cells[2].width = Cm(11)

        hdr_cells[0].text = 'Keterangan'
        hdr_cells[1].text = ''
        hdr_cells[2].text = ''

        for data_sw1, data_sw2, data_sw3 in data_aset:
            row_cells = table_aset.add_row().cells
            row_cells[0].width = Cm(5.19)
            row_cells[1].width = Cm(0.75)
            row_cells[2].width = Cm(11)

            row_cells[0].text = data_sw1
            row_cells[1].text = data_sw2
            row_cells[2].text = data_sw3
       
        document.add_page_break()

        #ANALISIS FIRMWARE
        p = document.add_paragraph('ANALISIS FIRMWARE')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('GRAFIK FIRMWARE')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_software = (capture_path+'/Software Summary.png')
        document.add_picture(img_software, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('TABEL RANGKUMAN FIRMWARE')
        p.style = document.styles['Heading 2']

        #TABEL RANGKUMAN FIRMWARE
        #sql query
        cursor.execute('''SELECT version, COUNT(*), date_eos, date_last FROM swsumtable GROUP BY version''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.4)
        hdr_cells[1].width = Cm(1.29)
        hdr_cells[2].width = Cm(2.75)
        hdr_cells[3].width = Cm(5.25)
        hdr_cells[4].width = Cm(4.31)

        hdr_cells[0].text = 'Versi Firmware'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Pesentase'    
        hdr_cells[3].text = 'Akhir Pemeliharaan Firmware'
        hdr_cells[4].text = 'Tanggal Terakhir Dukungan'    
        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.4)
            row_cells[1].width = Cm(1.29)
            row_cells[2].width = Cm(2.75)
            row_cells[3].width = Cm(5.25)
            row_cells[4].width = Cm(4.31)

            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[1]) 
            percentage_str = (str((row[1]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)

            row_cells[2].text = (percentage+'%')

            if row[2] == 'Not Announced':
                row_cells[3].text = '-'
                run1 = row_cells[3].paragraphs[0].runs[0]
                run1.font.color.rgb = red
            else:
                row_cells[3].text = (row[2])

            if row[3] == 'Not Announced':
                row_cells[4].text = '-'
                run2 = row_cells[4].paragraphs[0].runs[0]
                run2.font.color.rgb = red
            else:
                row_cells[4].text = (row[3])
        
        document.add_paragraph()
        p = document.add_paragraph('TABEL FIRMWARE')
        p.style = document.styles['Heading 2']

        #TABEL FIRMWARE
        #sql query
        cursor.execute("SELECT devicename, model, iosversion, uptime FROM swtable ORDER by devicename ASC")
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.13)
        hdr_cells[1].width = Cm(3.31)
        hdr_cells[2].width = Cm(3)
        hdr_cells[3].width = Cm(7.5)

        hdr_cells[0].text = 'Nama Perangkat'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Versi Firmware'
        hdr_cells[3].text = 'Waktu Aktif'
        for row in records:
            row_cells = table.add_row().cells
            
            row_cells[0].width = Cm(3.13)
            row_cells[1].width = Cm(3.31)
            row_cells[2].width = Cm(3)
            row_cells[3].width = Cm(7.5)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])

            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

        #TABEL FIRMWARE Explanation   
        #Keterangan FIRMWARE
        document.add_paragraph()
        data_firmware = (
            ('Nama Perangkat', '=' ,'Kolom nama perangkat menunjukkan nama perangkat (hostname).'),
            ('Model', '=' , 'Model menunjukkan tipe perangkat.'),
            ('Waktu Aktif', '=' , 'Uptime menunjukkkan berapa lama perangkat aktif.'),
            ('Akhir Pemeliharaan Firmware', '=' , 'Tanggal terakhir teknisi utama (Principal) dapat merilis pemeliharaan perangkat lunak atau perbaikan bug untuk produk perangkat lunak. Setelah tanggal ini, teknisi utama (Principal) tidak akan lagi mengembangkan, memperbaiki, memelihara, atau menguji firmware produk.'),
            ('Tanggal Terakhir Dukungan', '=' , 'Tanggal terakhir untuk menerima layanan dan dukungan untuk produk. Setelah tanggal ini, semua layanan dukungan untuk produk tidak tersedia, dan produk menjadi usang.'),
            ('Versi Firmware', '=' , 'Versi firmware menunjukkan versi firmware / sistem operasi yang digunakan oleh perangkat.'),
            ('Total', '=' , 'Total menunjukkan total dari versi firmware yang digunakan.'),
            ('Persentase', '=' , 'Persentase menunjukkan persentase dari versi firmware.')
        )
        table_firmware = document.add_table(rows=1, cols=3)
        table_firmware.allow_autofit = True
        table_firmware.style = 'Keterangan'
        hdr_cells = table_firmware.rows[0].cells
        table_firmware.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.19)
        hdr_cells[1].width = Cm(0.75)
        hdr_cells[2].width = Cm(11)

        hdr_cells[0].text = 'Keterangan'
        hdr_cells[1].text = ''
        hdr_cells[2].text = ''

        for data_sw1, data_sw2, data_sw3 in data_firmware:
            row_cells = table_firmware.add_row().cells
            row_cells[0].width = Cm(5.19)
            row_cells[1].width = Cm(0.75)
            row_cells[2].width = Cm(11)

            row_cells[0].text = data_sw1
            row_cells[1].text = data_sw2
            row_cells[2].text = data_sw3
       
        document.add_page_break()
        
        #ANALISA PERANGKAT KERAS
        p = document.add_paragraph('ANALISA PERANGKAT KERAS')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('GRAFIK PERANGKAT KERAS')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_hardware = (capture_path+'/Hardware Summary.png')
        document.add_picture(img_hardware, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Hardware Summary Table')
        p.style = document.styles['Heading 2']

        #GRAFIK PERANGKAT KERAS
        #sql query
        cursor.execute("SELECT card, date, COUNT(*) FROM eosltable GROUP BY card")
        records = cursor.fetchall()
        cursor.execute("SELECT COUNT(card) FROM eosltable")
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Model'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Persentase'
        hdr_cells[3].text = 'Akhir Dukungan'

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(1.69)
        hdr_cells[2].width = Cm(2.5)
        hdr_cells[3].width = Cm(4.25)

        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(1.69)
            row_cells[2].width = Cm(2.5)
            row_cells[3].width = Cm(4.25)

            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[2])

            percentage_str = (str((row[2]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)

            row_cells[2].text = (percentage+'%')

            if row[1] == 'Not Announced':
                row_cells[3].text = '-'
                run1 = row_cells[3].paragraphs[0].runs[0]
                run1.font.color.rgb = red

            else:
                row_cells[3].text = (row[1])

        document.add_paragraph()
        
        p = document.add_paragraph('ANALISA KONDISI PERANGKAT KERAS')
        p.style = document.styles['Heading 2']
        #SHOW ENVIRONMENT
        #sql query
        cursor.execute("SELECT * FROM envtable ORDER by devicename ASC")
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Nama Perangkat'
        hdr_cells[1].text = 'Sistem'
        hdr_cells[2].text = 'Item'   
        hdr_cells[3].text = 'Status'

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(2.44)
        hdr_cells[2].width = Cm(7)
        hdr_cells[3].width = Cm(3.3)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(2.44)
            row_cells[2].width = Cm(7)
            row_cells[3].width = Cm(3.3)

            if row_check == row[1]:
                pass
            else:
                row_check = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[1] and iteration_check == False:
                row_cells[0].text = (row[1])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[2]) 
            row_cells[2].text = (row[3])
            row_cells[3].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        document.add_paragraph()
        p = document.add_paragraph('TABEL MODUL PERANGKAT KERAS')
        p.style = document.styles['Heading 2']

        #TABEL MODUL PERANGKAT KERAS
        #sql query
        cursor.execute("SELECT devicename, model, card, hwdscr, sn, eosl_date FROM hwcardtable ORDER by devicename ASC")
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=6)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Nama Perangkat'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Modul'
        hdr_cells[3].text = 'Deskripsi'
        hdr_cells[4].text = 'Nomor Seri'
        hdr_cells[5].text = 'Akhir Dukungan'
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            if row_check != row[0] and iteration_check == False:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)

                row_check = row[0]
                row_detail = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
                start_row1 = table.cell(count_row, 1)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])
                row_cells[4].text = (row[4])
                
                if row[5] == 'Not Announced':
                    row_cells[5].text = '-'
                    run1 = row_cells[5].paragraphs[0].runs[0]
                    run1.font.color.rgb = red
                else:
                    row_cells[5].text = (row[5])

                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                row_cells = table.add_row().cells
                row_check = row[0]
                row_detail = row[1]
                
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])
                row_cells[4].text = (row[4])

                if row[5] == 'Not Announced':
                    row_cells[5].text = '-'
                    run1 = row_cells[5].paragraphs[0].runs[0]
                    run1.font.color.rgb = red
                else:
                    row_cells[5].text = (row[5])

                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
                end_row1 = table.cell(count_row, 1)
                merge_row1 = start_row1.merge(end_row1)
                iteration_check == True
                count_row+=1

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #Hardware Summary Table, Hardware Condition Analysis and Hardware Card Table Explanation
        #Keterangan HARDWARE
        document.add_paragraph()
        data_hardware = (
            ('Nama Perangkat', '=' ,'Kolom nama perangkat menunjukkan nama perangkat (hostname).'),
            ('Model', '=' , 'Model menunjukkan tipe perangkat.'),
            ('Total', '=' , 'Total menunjukkan total dari model perangkat keras.'),
            ('Persentase', '=' , 'Persentase menunjukkan persentase dari model perangkat keras.'),
            ('Akhir Dukungan', '=' , 'Tanggal terakhir untuk menerima layanan dan dukungan untuk produk. Setelah tanggal ini, semua layanan dukungan untuk produk tidak tersedia, dan produk menjadi usang.'),
            ('Sistem', '=' , 'Sistem mengacu pada catu daya, kipas, dan suhu perangkat keras.'),
            ('Item', '=' , 'Item mengacu pada jenis atau model catu daya, kipas dan suhu perangkat keras.'),
            ('Status', '=' , 'Status mengacu pada jenis atau model catu daya, kipas dan suhu perangkat keras.'),
            ('Modul', '=' , 'Modul mengacu pada modul yang ada pada perangkat keras.'),
            ('Deskripsi', '=' , 'Deskripsi mendalam tentang model dan modul dari perangkat keras.'),
            ('Nomor Seri', '=' , 'Kolom nomor seri mengacu pada identitas unik dari setiap modul yang ada pada perangkat keras.')
        )
        table_hardware = document.add_table(rows=1, cols=3)
        table_hardware.allow_autofit = True
        table_hardware.style = 'Keterangan'
        hdr_cells = table_hardware.rows[0].cells
        table_hardware.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.19)
        hdr_cells[1].width = Cm(0.75)
        hdr_cells[2].width = Cm(11)

        hdr_cells[0].text = 'Keterangan'
        hdr_cells[1].text = ''
        hdr_cells[2].text = ''

        for data_hw1, data_hw2, data_hw3 in data_hardware:
            row_cells = table_hardware.add_row().cells
            row_cells[0].width = Cm(5.19)
            row_cells[1].width = Cm(0.75)
            row_cells[2].width = Cm(11)

            row_cells[0].text = data_hw1
            row_cells[1].text = data_hw2
            row_cells[2].text = data_hw3

        document.add_page_break()
        
        #ANALISA CPU (CENTRAL PROCESSING UNIT)
        p = document.add_paragraph('ANALISA CPU (CENTRAL PROCESSING UNIT)')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('GRAFIK CPU (CENTRAL PROCESSING UNIT)')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_cpu = (capture_path+'/CPU Summary.png')
        document.add_picture(img_cpu, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('TABEL RANGKUMAN CPU (CENTRAL PROCESSING UNIT)')
        p.style = document.styles['Heading 2']

        #TABEL RANGKUMAN CPU (CENTRAL PROCESSING UNIT)
        #sql query
        cursor.execute("SELECT devicename, model, total, process, interrupt, topcpu, status FROM cpusumtable ORDER by devicename ASC")
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=7)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Nama Perangkat'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Total'
        hdr_cells[3].text = 'Proses'
        hdr_cells[4].text = 'Gangguan (Interrupt)'
        hdr_cells[5].text = 'Proses Teratas'
        hdr_cells[6].text = 'Status'

        hdr_cells[0].width = Cm(3.82)
        hdr_cells[1].width = Cm(3.12)
        hdr_cells[2].width = Cm(1.59)
        hdr_cells[3].width = Cm(1.5)
        hdr_cells[4].width = Cm(2)
        hdr_cells[5].width = Cm(3)
        hdr_cells[6].width = Cm(1.75)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            if row_check != row[0] and iteration_check == False:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(3.82)
                row_cells[1].width = Cm(3.12)
                row_cells[2].width = Cm(1.59)
                row_cells[3].width = Cm(1.5)
                row_cells[4].width = Cm(2)
                row_cells[5].width = Cm(3)
                row_cells[6].width = Cm(1.75)

                row_check = row[0]
                row_detail = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
                start_row1 = table.cell(count_row, 1)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])
                row_cells[2].text = str(round(float(row[2]),2))+'%'

                if 'Member ID' in row[3] or '-' in row[3] or 'Slot' in row[3]:
                    row_cells[3].text = str(row[3])+'%'
                else:
                    row_cells[3].text = str(round(float(row[3]),2))+'%'

                if '-' in row[4]:
                    row_cells[4].text = str(row[4])+'%'
                else:
                    row_cells[4].text = str(round(float(row[4]),2))+'%'
                    
                row_cells[5].text = (row[5])
                row_cells[6].text = (row[6])

                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                row_cells = table.add_row().cells
                row_check = row[0]
                row_detail = row[1]
                
                row_cells[2].text = str(round(float(row[2]),2))+'%'

                try:
                    row[3] + 'String'
                    row_cells[3].text = str(row[3])+'%'

                except:
                    row_cells[3].text = str(round(float(row[3]),2))+'%'

                try:
                    row[4] + 'String'
                    row_cells[4].text = str(row[4])+'%'

                except:
                    row_cells[4].text = str(round(float(row[4]),2))+'%'
            

                row_cells[5].text = (row[5])
                row_cells[6].text = (row[6])

                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
                end_row1 = table.cell(count_row, 1)
                merge_row1 = start_row1.merge(end_row1)
                iteration_check == True
                count_row+=1

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT


        #TABEL RANGKUMAN CPU (CENTRAL PROCESSING UNIT) Explanation
        #Keterangan CPU
        document.add_paragraph()
        data_cpu = (
            ('Nama Perangkat', '=' ,'Kolom nama perangkat menunjukkan nama perangkat (hostname).'),
            ('Model', '=' , 'Model menunjukkan tipe perangkat.'),
            ('Total', '=' , 'Total menunjukkan total dari model perangkat keras.'),
            ('Proses', '=' , 'Menampilkan proses CPU (Central Processing Unit) yang berjalan.'),
            ('Gangguan (Interrupt)', '=' , 'Menampilkan angka dari proses interrupt (gangguan) CPU (Central Processing Unit).'),
            ('Proses Teratas', '=' , 'Menampilkan 3 proses teratas berdasarkan proses terbesar dari total CPU (Central Processing Unit).'),
            ('Status', '=' , 'Menunjukan total pemakaian CPU (Central Processing Unit) pada perangkat (Rendah = 0 - 20%; Sedang 21 - 80%; Tinggi = 81 - 100%)')
        )
        table_cpu = document.add_table(rows=1, cols=3)
        table_cpu.allow_autofit = True
        table_cpu.style = 'Keterangan'
        hdr_cells = table_cpu.rows[0].cells
        table_cpu.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.19)
        hdr_cells[1].width = Cm(0.75)
        hdr_cells[2].width = Cm(11)

        hdr_cells[0].text = 'Keterangan'
        hdr_cells[1].text = ''
        hdr_cells[2].text = ''

        for data_1, data_2, data_3 in data_cpu:
            row_cells = table_cpu.add_row().cells
            row_cells[0].width = Cm(5.19)
            row_cells[1].width = Cm(0.75)
            row_cells[2].width = Cm(11)

            row_cells[0].text = data_1
            row_cells[1].text = data_2
            row_cells[2].text = data_3

      
        document.add_page_break()
        
        #ANALISA PENYIMPANAN (MEMORY)
        p = document.add_paragraph('ANALISA PENYIMPANAN (MEMORY)')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('GRAFIK PENYIMPANAN (MEMORY)')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_memory = (capture_path+'/Memory Summary.png')
        document.add_picture(img_memory, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('TABEL RANGKUMAN PENYIMPANAN (MEMORY)')
        p.style = document.styles['Heading 2']

        #TABEL RANGKUMAN PENYIMPANAN (MEMORY)
        #sql query
        cursor.execute("SELECT devicename, model, utils, topproc, status FROM memsumtable ORDER by devicename ASC")
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        table.style = "PM"
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Nama Perangkat'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Penggunaan Penyimpanan (memory)'
        hdr_cells[3].text = 'Proses Teratas'
        hdr_cells[4].text = 'Status'

        hdr_cells[0].width = Cm(4.13)
        hdr_cells[1].width = Cm(4.06)
        hdr_cells[2].width = Cm(2.51)
        hdr_cells[3].width = Cm(4)
        hdr_cells[4].width = Cm(2)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            if row_check != row[0] and iteration_check == False:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.13)
                row_cells[1].width = Cm(4.06)
                row_cells[2].width = Cm(2.51)
                row_cells[3].width = Cm(4)
                row_cells[4].width = Cm(2)

                row_check = row[0]
                row_detail = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
                start_row1 = table.cell(count_row, 1)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])

                if '-' in row[2]:
                    row_cells[2].text = str(row[2])
                else:
                    row_cells[2].text = str(round(float(row[2]),2))+'%'

                    row_cells[3].text = (row[3])
                    row_cells[4].text = (row[4])

                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                row_cells = table.add_row().cells
                row_check = row[0]
                row_detail = row[1]

                if '-' in row[2]:
                    row_cells[2].text = str(row[2])
                else:
                    row_cells[2].text = str(round(float(row[2]),2))+'%'

                    row_cells[3].text = (row[3])
                    row_cells[4].text = (row[4])

                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
                end_row1 = table.cell(count_row, 1)
                merge_row1 = start_row1.merge(end_row1)
                iteration_check == True
                count_row+=1

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #TABEL RANGKUMAN PENYIMPANAN (MEMORY) Explanation
        #Keterangan Memory
        document.add_paragraph()
        data_memory = (
            ('Nama Perangkat', '=' ,'Kolom nama perangkat menunjukkan nama perangkat (hostname).'),
            ('Model', '=' , 'Model menunjukkan tipe perangkat.'),
            ('Penggunaan Penyimpanan (memory)', '=' , 'Penyimpanan (memory) prosesor atau penyimpanan (memory) utama menyimpan konfigurasi yang sedang berjalan dan tabel perutean. Firmware yang dijalankan dari penyimpanan (memory) utama.'),
            ('Proses Teratas', '=' , 'Menampilkan 3 proses teratas berdasarkan proses terbesar dari penggunaan penyimpanan (memory).'),
            ('Status', '=' , 'Menunjukan total pemakaian penyimpanan (memory) pada perangkat (Rendah = 0 - 20%; Sedang 21 - 80%; Tinggi = 81 - 100%)')
        )
        table_memory = document.add_table(rows=1, cols=3)
        table_memory.allow_autofit = True
        table_memory.style = 'Keterangan'
        hdr_cells = table_memory.rows[0].cells
        table_memory.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.19)
        hdr_cells[1].width = Cm(0.75)
        hdr_cells[2].width = Cm(11)

        hdr_cells[0].text = 'Keterangan'
        hdr_cells[1].text = ''
        hdr_cells[2].text = ''

        for data_1, data_2, data_3 in data_memory:
            row_cells = table_memory.add_row().cells
            row_cells[0].width = Cm(5.19)
            row_cells[1].width = Cm(0.75)
            row_cells[2].width = Cm(11)

            row_cells[0].text = data_1
            row_cells[1].text = data_2
            row_cells[2].text = data_3
        
        # document.add_page_break()

        #close database
        db.close()
        
        #save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('pm_rs.docx')
        print('Document has been saved to pm_rs.docx')