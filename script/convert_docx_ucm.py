from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def func_get_req(address):
    try:
        os.chdir(main_path)
        files = ['init', 'init.exe']
        for file in files:
            os.chmod(file, 0o0777)
        address = re.findall(".*(qr.*)", address)
        address = address[0]
        if platform == "win32":
            cmd = ("init ")
            qr = subprocess.check_output(cmd+address, shell=True)
        else:
            cmd = ("./init ")
            qr = subprocess.check_output(cmd+address, shell=True)

        qr = str(qr)
        qr = re.findall("b\W(.*)\Wn\W", qr)
        qr = qr[0]
        return qr
    except:
        return

def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    qr45 = func_get_req(docx_create+"qr45")
    tblHeader = OxmlElement(qr45)
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_ucm:
    #@staticmethod
    def __init__(self, customer, review, prepared, pic, bulan, tahun):
        #variable constructor
        self.customer = customer
        self.review = review
        self.prepared = prepared
        self.pic = pic
        self.bulan = bulan
        self.tahun = tahun

    def convert_docx_ucm(self):
        print('')
        print('Processing Document')

        #open db connection
        os.chdir(capture_path)
        db = sqlite3.connect('pmdb_ucm')
        cursor = db.cursor()
        #using document docx module
        report_template = open(data_path+'template_content.docx', 'rb')
        document = Document(report_template)

        red = RGBColor(255, 0, 0)
        
        #Software Analysis
        p = document.add_paragraph('Software Analysis of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Software Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_software = (capture_path+'/Software Summary UCM.png')
        document.add_picture(img_software, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Software Summary Table')
        p.style = document.styles['Heading 2']

        #SOFTWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT version, COUNT(*), date_eos, date_last FROM swsumtable GROUP BY version''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.4)
        hdr_cells[1].width = Cm(1.54)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(5.25)
        hdr_cells[4].width = Cm(4.56)

        hdr_cells[0].text = 'Version'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Percentage'    
        hdr_cells[3].text = 'End of Software Maintenance'
        hdr_cells[4].text = 'Last Date of Support'

        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.4)
            row_cells[1].width = Cm(1.54)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(5.25)
            row_cells[4].width = Cm(4.56)

            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[1])

            percentage_str = (str((row[1]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)

            row_cells[2].text = (percentage+'%')
            if row[2] == 'Not Announced':
                row_cells[3].text = '-'
                run1 = row_cells[3].paragraphs[0].runs[0]
                run1.font.color.rgb = red
            else:
                row_cells[3].text = (row[2])

            if row[3] == 'Not Announced':
                row_cells[4].text = '-'
                run2 = row_cells[4].paragraphs[0].runs[0]
                run2.font.color.rgb = red
            else:
                row_cells[4].text = (row[3])
        
        document.add_paragraph()
        p = document.add_paragraph('Software Table')
        p.style = document.styles['Heading 2']

        #SOFTWARE TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, version, uptime, date_eos FROM swtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.4)
        hdr_cells[1].width = Cm(4.79)
        hdr_cells[2].width = Cm(3.5)
        hdr_cells[3].width = Cm(1.91)
        hdr_cells[4].width = Cm(3.4)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'OS Version'
        hdr_cells[3].text = 'Uptime'
        hdr_cells[4].text = 'End of Support'
        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.4)
            row_cells[1].width = Cm(4.79)
            row_cells[2].width = Cm(3.5)
            row_cells[3].width = Cm(1.91)
            row_cells[4].width = Cm(3.4)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])

            if row[4] == 'Not Announced':
                row_cells[4].text = '-'
                run1 = row_cells[4].paragraphs[0].runs[0]
                run1.font.color.rgb = red
            else:
                row_cells[4].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #Software Summary Table and Software Table Explanation   
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed server. Device Names Device Name be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the product model type. The table is sorted by product type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('OS Version')
        p.style = document.styles['Title']
        p = document.add_paragraph('Many customers attempt to standardize on OS for like server platforms. Rather than attempting to do this across the network. Identify key functions and features with each server and plan OS accordingly.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Uptime (Days)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Uptime (days) column displays the uptime in increments of days. The information included in this column allows the customer to review routers that December have been reloaded outside of the normally scheduled change control window. It also identifies routers that are possible candidates for scheduled reloads as a means of re-capturing memory, especially if the router currently exhibits low memory conditions.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('End of Software Maintenance')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date that Cisco Engineering may release any software maintenance releases or bug fixes to the software product. After this date, Cisco Engineering will no longer develop, repair, maintain, or test the product software.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Last Date of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date to receive service and support for the product. After this date, all support services for the product are unavailable, and the product becomes obsolete.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Software Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Software analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #Hardware Analysis
        p = document.add_paragraph('Hardware Analysis of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Hardware Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #MANUAL
        document.add_paragraph()
        p = document.add_paragraph()
        run = p.add_run('Chart Manual')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_paragraph()
        p = document.add_paragraph('Hardware Summary Table')
        p.style = document.styles['Heading 2']

        #HARDWARE SUMMARY TABLE
        data_hwsumtable = (
            ('Isi Manual (Ex: UCS C240 M4)', 'Isi Manual (Ex: 1)'),
            ('Isi Manual (Ex: UCS C220 M4)', 'Isi Manual (Ex: 2)')
        )
        table_hwsum = document.add_table(rows=1, cols=2)
        table_hwsum.allow_autofit = True
        table_hwsum.style = 'PM'
        hdr_cells = table_hwsum.rows[0].cells
        table_hwsum.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(8.5)
        hdr_cells[1].width = Cm(3.44)

        hdr_cells[0].text = 'Model'
        hdr_cells[1].text = 'Total'

        for data_hwsumtable1, data_hwsumtable2 in data_hwsumtable:
            row_cells = table_hwsum.add_row().cells
            row_cells[0].width = Cm(8.5)
            row_cells[1].width = Cm(3.44)

            row_cells[0].text = data_hwsumtable1
            row_cells[1].text = data_hwsumtable2

            run1 = row_cells[0].paragraphs[0].runs[0]
            run2 = row_cells[1].paragraphs[0].runs[0]

            run1.font.color.rgb = red
            run2.font.color.rgb = red
        
        document.add_paragraph()
        p = document.add_paragraph('Hardware Table')
        p.style = document.styles['Heading 2']

        #HARDWARE CARD TABLE
        #sql query
        qr18 = func_get_req(docx_create+"qr18")
        cursor.execute(qr18)
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Device Type'
        hdr_cells[2].text = 'Hardware Type'
        hdr_cells[3].text = 'Serial Number'
        hdr_cells[4].text = 'End of Support'
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            row_cells = table.add_row().cells
            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                row_cells[0].text = ('')
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
            row_cells[1].text = (row[1])
            row_cells[2].text = ('Isi Manual')
            row_cells[3].text = ('Isi Manual')
            row_cells[4].text = ('-')

            run1 = row_cells[2].paragraphs[0].runs[0]
            run2 = row_cells[3].paragraphs[0].runs[0]
            run3 = row_cells[4].paragraphs[0].runs[0]

            run1.font.color.rgb = red
            run2.font.color.rgb = red
            run3.font.color.rgb = red
            count_row+=1
        
        #Hardware Summary Table, Hardware Condition Analysis and Hardware Card Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed server. Device Names Device Name be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Device Type')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Device Type column refers to the server device type. The table is sorted by server device type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Hardware Type')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Device Type column refers to the server device type. The table is sorted by server device type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Serial Number')
        p.style = document.styles['Title']
        p = document.add_paragraph('The serial number column refers to the unique identifier for each card in the router.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('End of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('End of Support column refers to the last date of support from the principals.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Hardware Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Hardware analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()
        
        #Processor Analysis
        p = document.add_paragraph('Processor Analysis of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('CPU Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)


        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_cpu = (capture_path+'/CPU Summary UCM.png')
        document.add_picture(img_cpu, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('CPU Summary Table')
        p.style = document.styles['Heading 2']

        #CPU SUMMARY TABLE
        #sql query
        qr19 = func_get_req(docx_create+"qr19")
        cursor.execute(qr19)
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(6.69)
        hdr_cells[1].width = Cm(2.75)
        hdr_cells[2].width = Cm(5.5)
        hdr_cells[3].width = Cm(2.08)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'CPU Utilization (%)'
        hdr_cells[2].text = 'Top Process'
        hdr_cells[3].text = 'Status'

        for row in records:

            row_cells = table.add_row().cells
            row_cells[0].width = Cm(6.69)
            row_cells[1].width = Cm(2.75)
            row_cells[2].width = Cm(5.5)
            row_cells[3].width = Cm(2.08)

            convert_float = float(row[1])
            convert_round = round(convert_float, 2)
            convert_str = str(convert_round)

            row_cells[0].text = (row[0])
            row_cells[1].text = (convert_str+'%')
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])

        #CPU Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('CPU Utilization (%)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Utilization CPU % column refers to the total CPU based upon the 5-second, 1-minute and 5-minute average.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Top Process')
        p.style = document.styles['Title']
        p = document.add_paragraph('List top 3 processes which using most significant CPU.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display status or severity of CPU Process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('CPU Analysis Summary')
        p.style = document.styles['Title']
        #Kondisi CPU
        qr20 = func_get_req(docx_create+"qr20")
        cursor.execute(qr20)
        records = cursor.fetchall()

        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<CPU analysis summary must be analyzed in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)
        
        else:
            p = document.add_paragraph('Processor Analysis found '+str(len(records))+' problems on all devices.')
            p.style = document.styles['No Spacing']

            # Tabel CPU UCM
            table = document.add_table(rows=1, cols=4)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].width = Cm(6.69)
            hdr_cells[1].width = Cm(2.75)
            hdr_cells[2].width = Cm(5.5)
            hdr_cells[3].width = Cm(2.08)

            hdr_cells[0].text = 'Device Name'
            hdr_cells[1].text = 'CPU Utilization (%)'
            hdr_cells[2].text = 'Top Process'
            hdr_cells[3].text = 'Status'

            for row in records:

                row_cells = table.add_row().cells
                row_cells[0].width = Cm(6.69)
                row_cells[1].width = Cm(2.75)
                row_cells[2].width = Cm(5.5)
                row_cells[3].width = Cm(2.08)

                convert_float = float(row[1])
                convert_round = round(convert_float, 2)
                convert_str = str(convert_round)

                row_cells[0].text = (row[0])
                row_cells[1].text = (convert_str+'%')
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])
 
        document.add_page_break()
        
        #Memory Analysis
        p = document.add_paragraph('Memory Analysis of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Memory Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_memory = (capture_path+'/Memory Summary UCM.png')
        document.add_picture(img_memory, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Memory Summary Table')
        p.style = document.styles['Heading 2']

        #MEMORY SUMMARY TABLE
        #sql query
        qr21 = func_get_req(docx_create+"qr21")
        cursor.execute(qr21)
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(4.25)
        hdr_cells[2].width = Cm(6.42)
        hdr_cells[3].width = Cm(2.08)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Memory Utilization (%)'
        hdr_cells[2].text = 'Top Process'
        hdr_cells[3].text = 'Status'

        for row in records:

            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(4.25)
            row_cells[2].width = Cm(6.42)
            row_cells[3].width = Cm(2.08)

            convert_float = float(row[1])
            convert_round = round(convert_float, 2)
            convert_str = str(convert_round)

            row_cells[0].text = (row[0])
            row_cells[1].text = (convert_str+'%')
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
        
        #Memory Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Memory Utilization (%)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Utilization memory % column refers to the total CPU based upon the 5-second, 1-minute and 5-minute average.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Top Process')
        p.style = document.styles['Title']
        p = document.add_paragraph('List top 3 processes which using most significant memory.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Display status or severity of Memory Process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)
        
        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Memory Analysis Summary')
        p.style = document.styles['Title']
        #Kondisi Memory
        qr22 = func_get_req(docx_create+"qr22")
        cursor.execute(qr22)
        records = cursor.fetchall()

        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<Memory analysis must be summarized in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)
        
        else:
            p = document.add_paragraph('Memory Analysis found '+str(len(records))+' problems on all devices.')
            p.style = document.styles['No Spacing']
        
            # Tabel Memory UCM
            table = document.add_table(rows=1, cols=4)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].width = Cm(4.25)
            hdr_cells[1].width = Cm(4.25)
            hdr_cells[2].width = Cm(6.42)
            hdr_cells[3].width = Cm(2.08)

            hdr_cells[0].text = 'Device Name'
            hdr_cells[1].text = 'Memory Utilization (%)'
            hdr_cells[2].text = 'Top Process'
            hdr_cells[3].text = 'Status'

            for row in records:

                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)
                row_cells[1].width = Cm(4.25)
                row_cells[2].width = Cm(6.42)
                row_cells[3].width = Cm(2.08)

                convert_float = float(row[1])
                convert_round = round(convert_float, 2)
                convert_str = str(convert_round)

                row_cells[0].text = (row[0])
                row_cells[1].text = (convert_str+'%')
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])

        document.add_page_break()
        
        #Disk Analysis
        p = document.add_paragraph('Disk Analysis of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Disk Summary Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        #Disk Summary Table
        #sql query
        qr23 = func_get_req(docx_create+"qr23")
        cursor.execute(qr23)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=7)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Disk Name'
        hdr_cells[2].text = 'Disk Total'
        hdr_cells[3].text = 'Disk Free'
        hdr_cells[4].text = 'Disk Used'
        hdr_cells[5].text = 'Disk Percentage'
        hdr_cells[6].text = 'Disk Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1

        for row in records:
            row_cells = table.add_row().cells

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5]+'%')
            row_cells[6].text = (row[6])
            
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #Disk Analysis Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Disk Name')
        p.style = document.styles['Title']
        p = document.add_paragraph('Name of disk partition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Disk Total')
        p.style = document.styles['Title']
        p = document.add_paragraph('Total size of disk partition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Disk Free')
        p.style = document.styles['Title']
        p = document.add_paragraph('Count of free space on disk partition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Disk Used')
        p.style = document.styles['Title']
        p = document.add_paragraph('Count of disk usage on disk partition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Disk Percentage')
        p.style = document.styles['Title']
        p = document.add_paragraph('Percentage of disk usage.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Disk Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-30%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 31-90%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 91-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Disk Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Disk analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()
        
        #Backup Generate Report
        p = document.add_paragraph('Backup Status of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Scheduler Backup Summary Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #Backup Summary Table
        #sql query
        qr24 = func_get_req(docx_create+"qr24")
        cursor.execute(qr24)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(4.2)
        hdr_cells[1].width = Cm(3.25)
        hdr_cells[2].width = Cm(3.25)
        hdr_cells[3].width = Cm(3.25)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Scheduled Backup'
        hdr_cells[2].text = 'Manual Backup During PM'
        hdr_cells[3].text = 'Manual Export CDR During PM'

        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.2)
            row_cells[1].width = Cm(3.25)
            row_cells[2].width = Cm(3.25)
            row_cells[3].width = Cm(3.25)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = 'Isi Yes/No'
            row_cells[3].text = 'Isi Yes/No'

            run1 = row_cells[2].paragraphs[0].runs[0]
            run2 = row_cells[3].paragraphs[0].runs[0]
            run1.font.color.rgb = red
            run2.font.color.rgb = red
        
        document.add_paragraph()
        p = document.add_paragraph('Summary of Last Backup')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #Backup Summary Table
        #sql query
        qr25 = func_get_req(docx_create+"qr25")
        cursor.execute(qr25)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=6)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3)
        hdr_cells[1].width = Cm(3)
        hdr_cells[2].width = Cm(2)
        hdr_cells[3].width = Cm(2)
        hdr_cells[4].width = Cm(5)
        hdr_cells[5].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Last Date Backup'
        hdr_cells[2].text = 'Type Backup'
        hdr_cells[3].text = 'Status Backup'
        hdr_cells[4].text = 'Feature(s) Backed Up'
        hdr_cells[5].text = 'Feature(s) Failed'

        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(3)
            row_cells[1].width = Cm(3)
            row_cells[2].width = Cm(2)
            row_cells[3].width = Cm(2)
            row_cells[4].width = Cm(5)
            row_cells[5].width = Cm(2)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5])

        #Backup Analysis Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Scheduled Backup')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status of scheduler backup. Scheduler backup has been enabled or not.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Manual Backup During PM')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status of manual backup activities during preventive maintenance. Manual backup has been done or not.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Manual Export CDR During PM')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status of manual export CDR (Call Data Records) during preventive maintenance. Manual export CDR has been done or not.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Last Date Backup')
        p.style = document.styles['Title']
        p = document.add_paragraph('Information of last backup attempt date.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Type Backup')
        p.style = document.styles['Title']
        p = document.add_paragraph('Method of backup. Is it scheduled backup or manual backup.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)
        
        p = document.add_paragraph('Status Backup')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status of last backup.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Feature(s) Backed Up')
        p.style = document.styles['Title']
        p = document.add_paragraph('List all features that has successfully backed up.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Feature(s) Failed')
        p.style = document.styles['Title']
        p = document.add_paragraph('List all features that failed to be backed up.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Backup Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Backup analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #Certificate Generate Report
        p = document.add_paragraph('Certificate Status of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Certificate Summary Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        #Certificate Summary Table
        #sql query
        qr26 = func_get_req(docx_create+"qr26")
        cursor.execute(qr26)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=6)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Certificate Name'
        hdr_cells[2].text = 'From Date'
        hdr_cells[3].text = 'To Date'
        hdr_cells[4].text = 'Method'
        hdr_cells[5].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1

        for row in records:
            row_cells = table.add_row().cells

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #Certificate Analysis Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Certificate Name')
        p.style = document.styles['Title']
        p = document.add_paragraph('Name of certificate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('From Date')
        p.style = document.styles['Title']
        p = document.add_paragraph('Start date of validity.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('To Date')
        p.style = document.styles['Title']
        p = document.add_paragraph('Last date of validity.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Method')
        p.style = document.styles['Title']
        p = document.add_paragraph('Type of certificate, Self Signed or CA Signed.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status expiration date of certificate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Certificate Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Certificate analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #Database Replication Generate Report
        p = document.add_paragraph('Database Replication Status of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Database Replication Summary Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        #Certificate Summary Table
        #sql query
        qr27 = func_get_req(docx_create+"qr27")
        cursor.execute(qr27)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(4.94)
        hdr_cells[1].width = Cm(6.25)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(3.62)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Node'
        hdr_cells[2].text = 'Replication Code'
        hdr_cells[3].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.94)
            row_cells[1].width = Cm(6.25)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(3.62)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            count_row+=1

        #Database Replication Analysis Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Replication Code')
        p.style = document.styles['Title']
        p = document.add_paragraph('Value of replication status: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('0 means initialization state')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('1 means the number of replicate is incorrect')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('2 means replication is good')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('3 means mismatch table')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('4 means setup failed / drop')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status of database replication.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Database Replication Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Database replication analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #License Replication Generate Report
        p = document.add_paragraph('License Information of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('License of Cisco Unified Communication Manager')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        p = document.add_paragraph()
        run = p.add_run('Isi Manual dari Cisco Prime License Manager')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        #kondisi unutk license ccx tidak ada
        qr28 = func_get_req(docx_create+"qr28")
        cursor.execute(qr28)
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                p = document.add_paragraph('Device Name')
                p.style = document.styles['Title']
                p.paragraph_format.space_before = Pt(6)
                p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('License Analysis Summary')
                p.style = document.styles['Title']
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('<License analysis must be summarized in here>')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)

            else:
                p = document.add_paragraph('License of Cisco Unified Contact Center Express')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #License Summary Table
                #sql query
                qr29 = func_get_req(docx_create+"qr29")
                cursor.execute(qr29)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.25)
                hdr_cells[1].width = Cm(4.25)
                hdr_cells[2].width = Cm(6.44)
                hdr_cells[3].width = Cm(2.05)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'License Name'
                hdr_cells[2].text = 'License Details'
                hdr_cells[3].text = 'License Count'

                row_check = 'ludesdeveloper'
                row_detail = 'ludesdeveloper'
                iteration_check = False

                count_row = 1
                for row in records:
                    if row_check != row[0] and iteration_check == False:
                        row_cells = table.add_row().cells
                        row_cells[0].width = Cm(4.25)
                        row_cells[1].width = Cm(4.25)
                        row_cells[2].width = Cm(6.44)
                        row_cells[3].width = Cm(2.05)

                        row_check = row[0]
                        row_detail = row[1]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                        start_row1 = table.cell(count_row, 1)

                        row_cells[0].text = (row[0])
                        row_cells[1].text = (row[1])
                        row_cells[2].text = (row[2])
                        row_cells[3].text = (row[3])

                        row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                        row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                        row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
                        row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
                        row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                        iteration_check == True
                        count_row+=1

                    elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                        row_cells = table.add_row().cells
                        row_check = row[0]
                        row_detail = row[1]
                        
                        row_cells[2].text = (row[2])
                        row_cells[3].text = (row[3])

                        row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                        end_row1 = table.cell(count_row, 1)
                        merge_row1 = start_row1.merge(end_row1)
                        iteration_check == True
                        count_row+=1
                
                p = document.add_paragraph('Device Name')
                p.style = document.styles['Title']
                p.paragraph_format.space_before = Pt(6)
                p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('License Name')
                p.style = document.styles['Title']
                p = document.add_paragraph('Name of CCX license.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('License Details')
                p.style = document.styles['Title']
                p = document.add_paragraph('Detail of CCX license.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('License Count')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count or enabled license of CCX license.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('License Analysis Summary')
                p.style = document.styles['Title']
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('<License analysis must be summarized in here>')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #Kondisi jika CUCM tidak ada
        qr30 = func_get_req(docx_create+"qr30")
        cursor.execute(qr30)
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                pass

            else:
                #IP Phone Summary Replication Generate Report
                p = document.add_paragraph('Cisco IP Phone Summary of Collaboration Device')
                p.style = document.styles['Heading 1']

                p = document.add_paragraph('Device Count Summary')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Phone Registered Summary Table
                #sql query
                qr31 = func_get_req(docx_create+"qr31")
                cursor.execute(qr31)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=6)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(6.62)
                hdr_cells[1].width = Cm(2.07)
                hdr_cells[2].width = Cm(2.5)
                hdr_cells[3].width = Cm(2)
                hdr_cells[4].width = Cm(2)
                hdr_cells[5].width = Cm(1.8)

                hdr_cells[0].text = 'Device Type'
                hdr_cells[1].text = 'Total Registered Device'
                hdr_cells[2].text = 'Total Unregistered Device'
                hdr_cells[3].text = 'Total Unknown Device'
                hdr_cells[4].text = 'Total Rejected Device'
                hdr_cells[5].text = 'Total Device'

                list_regis = []
                list_unreg= []
                list_unk = []
                list_rej = []
                litt_total = []

                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(6.62)
                    row_cells[1].width = Cm(2.07)
                    row_cells[2].width = Cm(2.5)
                    row_cells[3].width = Cm(2)
                    row_cells[4].width = Cm(2)
                    row_cells[5].width = Cm(1.8)

                    regis = row[1]
                    list_regis.append(regis)
                    unreg = row[2]
                    list_unreg.append(unreg)
                    unk = row[3]
                    list_unk.append(unk)
                    rej = row[4]
                    list_rej.append(rej)
                    total = row[5]
                    litt_total.append(total)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (str(row[1]))
                    row_cells[2].text = (str(row[2]))
                    row_cells[3].text = (str(row[3]))
                    row_cells[4].text = (str(row[4]))
                    row_cells[5].text = (str(row[5]))
                
                all_regis = sum(list_regis)
                all_unreg = sum(list_unreg)
                all_unk = sum(list_unk)
                all_rej = sum(list_rej)
                all_total = sum(litt_total)

                row_cells[0].text = 'SUB TOTAL'
                row_cells[1].text = (str(all_regis))
                row_cells[2].text = (str(all_unreg))
                row_cells[3].text = (str(all_unk))
                row_cells[4].text = (str(all_rej))
                row_cells[5].text = (str(all_total))

                run0 = row_cells[0].paragraphs[0].runs[0]
                run1 = row_cells[1].paragraphs[0].runs[0]
                run2 = row_cells[2].paragraphs[0].runs[0]
                run3 = row_cells[3].paragraphs[0].runs[0]
                run4 = row_cells[4].paragraphs[0].runs[0]
                run5 = row_cells[5].paragraphs[0].runs[0]

                run0.bold = True
                run1.bold = True
                run2.bold = True
                run3.bold = True
                run4.bold = True
                run5.bold = True

                document.add_paragraph()
                p = document.add_paragraph('Device Firmware Information')
                p.style = document.styles['Heading 2']

                #sql query
                qr32 = func_get_req(docx_create+"qr32")
                cursor.execute(qr32)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.5)
                hdr_cells[1].width = Cm(4.5)
                hdr_cells[2].width = Cm(4.5)
                hdr_cells[3].width = Cm(3.5)

                hdr_cells[0].text = 'Device Type'
                hdr_cells[1].text = 'Device Firmware'
                hdr_cells[2].text = 'Cisco Latest Firmware'
                hdr_cells[3].text = 'End of Support'

                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.5)
                    row_cells[1].width = Cm(4.5)
                    row_cells[2].width = Cm(4.5)
                    row_cells[3].width = Cm(3.5)

                    row_cells[0].text = (row[0])
                    row_cells[1].text = (row[1])
                    row_cells[2].text = ('Isi Manual')
                    row_cells[3].text = ('Isi Manual (YYYY-MM-DD)')

                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                    run1 = row_cells[2].paragraphs[0].runs[0]
                    run2 = row_cells[3].paragraphs[0].runs[0]

                    run1.font.color.rgb = red
                    run2.font.color.rgb = red

                p = document.add_paragraph('Device Type')
                p.style = document.styles['Title']
                p.paragraph_format.space_before = Pt(6)
                p = document.add_paragraph('The Device Type column refers to the device type of Cisco IP Phone in Call Manager.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Registered Device')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total Registered Device refers to total registered device for specific type in Call Manager.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Unregistered Device')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total Unregistered Device refers to total unregistered device for specific type in Call Manager.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Unknown Device')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total Unknown Device refers to total unknown device for specific type in Call Manager.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Rejected Device')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total Rejected Device refers to total unknown device for specific type in Call Manager.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Device')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total Device refers to total device for specific type in Call Manager.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Device Firmware')
                p.style = document.styles['Title']
                p = document.add_paragraph('The Device Firmware column refers to the device firmware of Cisco IP Phone for specific type in Call Manager.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Cisco Latest Firmware')
                p.style = document.styles['Title']
                p = document.add_paragraph('The Cisco Latest Firmware column refers to the latest firmware of Cisco IP Phone for specific type in Cisco.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('End of Support')
                p.style = document.styles['Title']
                p = document.add_paragraph('Information regarding last date of support of phone firmware.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Cisco IP Phone Analysis Summary')
                p.style = document.styles['Title']
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('<Cisco IP Phone analysis must be summarized in here>')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)

                document.add_page_break()

        #Kondisi jika CCX tidak ada
        qr33 = func_get_req(docx_create+"qr33")
        cursor.execute(qr33)
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                pass

            else:
                #Application CCX
                p = document.add_paragraph('Information of Cisco Unified Contact Center Express')
                p.style = document.styles['Heading 1']

                p = document.add_paragraph('Application Count Summary')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Application Total Table
                #sql query
                qr34 = func_get_req(docx_create+"qr34")
                cursor.execute(qr34)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=2)
                table.allow_autofit = True
                table.style = 'Total'
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.3)
                hdr_cells[1].width = Cm(5.25)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Total CCX Aplication'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.3)
                    row_cells[1].width = Cm(5.25)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = str(row[1])
                    count_row+=1

                document.add_paragraph()

                p = document.add_paragraph('Application Information')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Application Summary Table
                #sql query
                qr35 = func_get_req(docx_create+"qr35")
                cursor.execute(qr35)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.3)
                hdr_cells[1].width = Cm(8.3)
                hdr_cells[2].width = Cm(2.2)
                hdr_cells[3].width = Cm(2.2)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Application Name'
                hdr_cells[2].text = 'Status Enable'
                hdr_cells[3].text = 'Number of Session'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.3)
                    row_cells[1].width = Cm(8.3)
                    row_cells[2].width = Cm(2.2)
                    row_cells[3].width = Cm(2.2)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                    count_row+=1

                document.add_paragraph()

                p = document.add_paragraph('Prompt Count Summary')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Prompt Total Table
                #sql query
                qr36 = func_get_req(docx_create+"qr36")
                cursor.execute(qr36)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=2)
                table.allow_autofit = True
                table.style = 'Total'
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.3)
                hdr_cells[1].width = Cm(5.25)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Total CCX Prompt'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.3)
                    row_cells[1].width = Cm(5.25)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = str(row[1])
                    count_row+=1

                document.add_paragraph()

                p = document.add_paragraph('Script Count Summary')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Script Total Table
                #sql query
                qr37 = func_get_req(docx_create+"qr37")
                cursor.execute(qr37)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=2)
                table.allow_autofit = True
                table.style = 'Total'
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.3)
                hdr_cells[1].width = Cm(5.25)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Total CCX Script'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.3)
                    row_cells[1].width = Cm(5.25)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = str(row[1])
                    count_row+=1

                document.add_paragraph()

                p = document.add_paragraph('Script Information')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Script Summary Table
                #sql query
                qr38 = func_get_req(docx_create+"qr38")
                cursor.execute(qr38)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=2)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.3)
                hdr_cells[1].width = Cm(5.5)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Script Name'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.3)
                    row_cells[1].width = Cm(5.5)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = (row[1])

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                    count_row+=1

                document.add_paragraph()

                p = document.add_paragraph('Triger Count Summary')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Triger Total Table
                #sql query
                qr39 = func_get_req(docx_create+"qr39")
                cursor.execute(qr39)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=2)
                table.allow_autofit = True
                table.style = 'Total'
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.3)
                hdr_cells[1].width = Cm(5.25)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Total CCX Trigger'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.3)
                    row_cells[1].width = Cm(5.25)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = str(row[1])
                    count_row+=1

                document.add_paragraph()

                p = document.add_paragraph('Triger Information')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Triger Summary Table
                #sql query
                qr40 = func_get_req(docx_create+"qr40")
                cursor.execute(qr40)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=5)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.3)
                hdr_cells[1].width = Cm(2.2)
                hdr_cells[2].width = Cm(6.1)
                hdr_cells[3].width = Cm(2.2)
                hdr_cells[4].width = Cm(2.2)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Triger Number'
                hdr_cells[2].text = 'Application Name'
                hdr_cells[3].text = 'Status Enable'
                hdr_cells[4].text = 'Number of Session'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.3)
                    row_cells[1].width = Cm(2.2)
                    row_cells[2].width = Cm(6.1)
                    row_cells[3].width = Cm(2.2)
                    row_cells[4].width = Cm(2.2)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])
                    row_cells[4].text = (row[4])

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                    row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                    count_row+=1
                
                p = document.add_paragraph('Device Name')
                p.style = document.styles['Title']
                p.paragraph_format.space_before = Pt(6)
                p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Application Name')
                p.style = document.styles['Title']
                p = document.add_paragraph('Name of CCX application.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Status Enable')
                p.style = document.styles['Title']
                p = document.add_paragraph('Status of CCX application. Is it enabled or not.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Number of Session')
                p.style = document.styles['Title']
                p = document.add_paragraph('Number of session that configured on CCX application.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Script Name')
                p.style = document.styles['Title']
                p = document.add_paragraph('Name of CCX script.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Triger Number')
                p.style = document.styles['Title']
                p = document.add_paragraph('Extension number that has been set as trigger number/pilot number of CCX application.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total CCX Aplication')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of CCX application.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total CCX Prompt')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of CCX prompt.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total CCX Script')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of CCX script.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total CCX Trigger')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of CCX trigger.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Information of Cisco Unified Contact Center Express Analysis Summary')
                p.style = document.styles['Title']
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('<Information of Cisco Unified Contact Center Express analysis must be summarized in here>')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)

                document.add_page_break()

        #Kondisi jika CUCN Tidak ada
        qr41 = func_get_req(docx_create+"qr41")
        cursor.execute(qr41)
        records = cursor.fetchall()

        for row in records:
            if row[0] == 0:
                pass

            else:
                #Mailbox CUCN
                p = document.add_paragraph('Information of Cisco Unity Connection')
                p.style = document.styles['Heading 1']

                p = document.add_paragraph('Mailbox Count Summary')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Mailbox Total Table
                #sql query
                qr42 = func_get_req(docx_create+"qr42")
                cursor.execute(qr42)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=5)
                table.allow_autofit = True
                table.style = 'Total'
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Total User Mailbox'
                hdr_cells[2].text = 'Total Message Inbox'
                hdr_cells[3].text = 'Total Message Deleted'
                hdr_cells[4].text = 'Total All Message'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = str(row[1])
                    row_cells[2].text = str(row[2])
                    row_cells[3].text = str(row[3])
                    row_cells[4].text = str(row[4])
                    count_row+=1

                document.add_paragraph()

                p = document.add_paragraph('User ID Count Summary')
                p.style = document.styles['Heading 2']
                p.paragraph_format.space_before = Pt(6)

                #Non Mailbox Count User ID Summary
                #sql query
                qr43 = func_get_req(docx_create+"qr43")
                cursor.execute(qr43)
                records = cursor.fetchall()
                #add to document
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                table.style = 'Total'
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(3.4)
                hdr_cells[1].width = Cm(3.4)
                hdr_cells[2].width = Cm(3.4)
                hdr_cells[3].width = Cm(3.4)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Total Mailbox User ID'
                hdr_cells[2].text = 'Total Non Mailbox User ID'
                hdr_cells[3].text = 'All Total User ID'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(3.4)
                    row_cells[1].width = Cm(3.4)
                    row_cells[2].width = Cm(3.4)
                    row_cells[3].width = Cm(3.4)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)
                    row_cells[1].text = (str(row[1]))
                    row_cells[2].text = (str(row[3]))
                    row_cells[3].text = (str(row[2]))

                    count_row+=1

                p = document.add_paragraph('Device Name')
                p.style = document.styles['Title']
                p.paragraph_format.space_before = Pt(6)
                p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total User Mailbox')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of user mailbox.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Message Inbox')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of message on inbox of every user mailbox.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Message Deleted ')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of deleted message of every user mailbox.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total All Message')
                p.style = document.styles['Title']
                p = document.add_paragraph('Sum of message on inbox and deleted.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Mailbox User ID')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of user ID that has privileged to mailbox.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total Non Mailbox User ID')
                p.style = document.styles['Title']
                p = document.add_paragraph('Total count of user ID that does not has privileged to mailbox.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Total All User ID')
                p.style = document.styles['Title']
                p = document.add_paragraph('Sum of user that has privileged to mailbox and user that does not has privileged to mailbox.')
                p.style = document.styles['No Spacing']
                p.paragraph_format.space_after = Pt(6)

                p = document.add_paragraph('Information of Cisco Unity Connection Analysis Summary')
                p.style = document.styles['Title']
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('<Information of Cisco Unity Connection analysis must be summarized in here>')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)

                document.add_page_break()

        #Log Summary Replication Generate Report
        p = document.add_paragraph('Log Analysis of Collaboration Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Alert and Log Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #Alert and Log Summary Table
        #sql query
        qr44 = func_get_req(docx_create+"qr44")
        cursor.execute(qr44)
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=3)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.5)
        hdr_cells[1].width = Cm(10.75)
        hdr_cells[2].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Alert and Log Events'
        hdr_cells[2].text = 'Severity'

        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(3.5)
            row_cells[1].width = Cm(10.75)
            row_cells[2].width = Cm(2)

            row_cells[0].text = (row[1])
            row_cells[1].text = (row[2])
            row_cells[2].text = 'Isi Low/Medium/High'

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            run = row_cells[2].paragraphs[0].runs[0]
            run.font.color.rgb = red
        
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed server. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Alert and Log')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Alert and Log events refer to the logging process of the device which indicates incident or informational information. This log could be an alert for the administrator to mitigate problem.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Severity')
        p.style = document.styles['Title']
        p = document.add_paragraph('The severity tables indicate the importance of the log. High severity means it would cause downtime or disruption to network. Medium severity means this log may lead to network disruption in the future. Low severity means these logs are for information only.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Alert and Log Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Alert and log analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)
        
        db.close()
        
        #save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('pm_ucm.docx')
        print('Document has been saved to pm_ucm.docx')