import sqlite3

class dbreport_ucm:
    def __init__(self):
        #DESTROY PMDB_UCM
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE tipetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE hwtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swsumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE cputable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE memtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE disktable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE backuptable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE certtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE reptable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE alerttable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE tipetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE logtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE firmwaretable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE phonetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE exttable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE lictable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE ccxapptable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE ccxscripttable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE ccxpromptable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE ccxtrigertable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE cucnmessagetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE cucnuseridtable''')
            db.commit()
            db.close()
        except:
            pass
        
        #OPEN DB PMBD_UCM
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE tipetable(id INTEGER PRIMARY KEY, tipe TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, iosversion TEXT, uptime TEXT, confreg TEXT, version TEXT, date_eos TEXT, date_last TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE hwtable(id INTEGER PRIMARY KEY, devicename TEXT, device_type TEXT,
                hardware_type TEXT, sn TEXT, eos_hardware TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swsumtable(id INTEGER PRIMARY KEY, version TEXT, date_eos TEXT, date_last TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE cputable(id INTEGER PRIMARY KEY, devicename TEXT,
                cpu TEXT, top_cpu TEXT, cpu_status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE memtable(id INTEGER PRIMARY KEY, devicename TEXT,
                memory TEXT, top_memory TEXT, memory_status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE disktable(id INTEGER PRIMARY KEY, devicename TEXT,
                disk_name TEXT, disk_total TEXT, disk_free TEXT, disk_used TEXT,
                disk_percentage TEXT, disk_status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE backuptable(id INTEGER PRIMARY KEY, devicename TEXT,
                last_date_backup TEXT, type_backup TEXT, status_backup TEXT, schedule_backup TEXT,
                fitur_backup TEXT, failed_backup TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE certtable(id INTEGER PRIMARY KEY, devicename TEXT,
                certificate_name TEXT, from_date TEXT, to_date TEXT,
                cert_method TEXT, cert_status TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE reptable(id INTEGER PRIMARY KEY, devicename TEXT, node TEXT,
                replication TEXT, rep_status TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE alerttable(id INTEGER PRIMARY KEY, devicename TEXT,
                alert_log TEXT, severity TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE logtable(id INTEGER PRIMARY KEY, devicename TEXT, model TEXT, script TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE firmwaretable(id INTEGER PRIMARY KEY, devicetype TEXT, firmware TEXT, latest TEXT, eos_phone TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE phonetable(id INTEGER PRIMARY KEY, devicename TEXT, devicetype TEXT, status_phone TEXT, desc_phone TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE exttable(id INTEGER PRIMARY KEY, devicename TEXT, ext_phone TEXT, status_ext TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE lictable(id INTEGER PRIMARY KEY, devicename TEXT, lic_name TEXT, lic_detail TEXT, lic_count TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE ccxapptable(id INTEGER PRIMARY KEY, devicename TEXT, app_name TEXT, app_enable TEXT, app_ses TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE ccxscripttable(id INTEGER PRIMARY KEY, devicename TEXT, script_name TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE ccxpromptable(id INTEGER PRIMARY KEY, devicename TEXT, promp_name TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE ccxtrigertable(id INTEGER PRIMARY KEY, devicename TEXT, triger_no TEXT, triger_name TEXT, triger_enable TEXT, triger_ses TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE cucnmessagetable(id INTEGER PRIMARY KEY, devicename TEXT, message_user TEXT, message TEXT, message_inbox TEXT, message_delete TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_ucm')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE cucnuseridtable(id INTEGER PRIMARY KEY, devicename TEXT, userid TEXT)
            ''')
            db.close()
        except:
            pass

