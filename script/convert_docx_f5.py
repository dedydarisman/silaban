from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def func_get_req(address):
    try:
        os.chdir(main_path)
        files = ['init', 'init.exe']
        for file in files:
            os.chmod(file, 0o0777)
        address = re.findall(".*(qr.*)", address)
        address = address[0]
        if platform == "win32":
            cmd = ("init ")
            qr = subprocess.check_output(cmd+address, shell=True)
        else:
            cmd = ("./init ")
            qr = subprocess.check_output(cmd+address, shell=True)

        qr = str(qr)
        qr = re.findall("b\W(.*)\Wn\W", qr)
        qr = qr[0]
        return qr
    except:
        return

def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    qr45 = func_get_req(docx_create+"qr45")
    tblHeader = OxmlElement(qr45)
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_f5:
    #@staticmethod
    def __init__(self, customer, review, prepared, pic, bulan, tahun):
        #variable constructor
        self.customer = customer
        self.review = review
        self.prepared = prepared
        self.pic = pic
        self.bulan = bulan
        self.tahun = tahun

    def convert_docx_f5(self):
        print('')
        print('Processing Document')

        #open db connection
        db = sqlite3.connect('pmdb_f5')
        cursor = db.cursor()

        #using document docx module
        report_template = open(data_path+'template_content.docx', 'rb')
        document = Document(report_template)

        red = RGBColor(255, 0, 0)
        
        #Software Analysis
        p = document.add_paragraph('Software Analysis of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Software Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_software = (capture_path+'/Software Summary F5.png')
        document.add_picture(img_software, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Software Summary Table')
        p.style = document.styles['Heading 2']

        #SOFTWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT version, COUNT(*), date_eos, date_last FROM swsumtable GROUP BY version''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.4)
        hdr_cells[1].width = Cm(1.54)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(5.25)
        hdr_cells[4].width = Cm(4.56)

        hdr_cells[0].text = 'Version'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Percentage'    
        hdr_cells[3].text = 'End of Software Maintenance'
        hdr_cells[4].text = 'Last Date of Support'

        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.4)
            row_cells[1].width = Cm(1.54)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(5.25)
            row_cells[4].width = Cm(4.56)

            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[1])

            percentage_str = (str((row[1]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)

            row_cells[2].text = (percentage+'%')

            if row[2] == 'Not Announced':
                row_cells[3].text = '-'
                run1 = row_cells[3].paragraphs[0].runs[0]
                run1.font.color.rgb = red
            else:
                row_cells[3].text = (row[2])

            if row[3] == 'Not Announced':
                row_cells[4].text = '-'
                run2 = row_cells[4].paragraphs[0].runs[0]
                run2.font.color.rgb = red
            else:
                row_cells[4].text = (row[3])
        
        document.add_paragraph()
        p = document.add_paragraph('Software Table')
        p.style = document.styles['Heading 2']

        #SOFTWARE TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, version, confreg, uptime FROM swtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.4)
        hdr_cells[1].width = Cm(4.79)
        hdr_cells[2].width = Cm(3.5)
        hdr_cells[3].width = Cm(1.91)
        hdr_cells[4].width = Cm(3.4)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Version'
        hdr_cells[3].text = 'Build'
        hdr_cells[4].text = 'Uptime'
        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.4)
            row_cells[1].width = Cm(4.79)
            row_cells[2].width = Cm(3.5)
            row_cells[3].width = Cm(1.91)
            row_cells[4].width = Cm(3.4)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #Software Summary Table and Software Table Explanation   
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed server. Device Names Device Name be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the product model type. The table is sorted by product type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Version')
        p.style = document.styles['Title']
        p = document.add_paragraph('Many customers attempt to standardize on OS for like server platforms. Rather than attempting to do this across the network. Identify key functions and features with each server and plan OS accordingly.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Build')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Build column refers to the F5 build number software version.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Uptime (Days)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Uptime (days) column displays the uptime in increments of days. The information included in this column allows the customer to review routers that December have been reloaded outside of the normally scheduled change control window. It also identifies routers that are possible candidates for scheduled reloads as a means of re-capturing memory, especially if the router currently exhibits low memory conditions.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('End of Software Maintenance')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date that Cisco Engineering may release any software maintenance releases or bug fixes to the software product. After this date, Cisco Engineering will no longer develop, repair, maintain, or test the product software.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Last Date of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date to receive service and support for the product. After this date, all support services for the product are unavailable, and the product becomes obsolete.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Software Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Software analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #Hardware Analysis
        p = document.add_paragraph('Hardware Analysis of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Hardware Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_hardware = (capture_path+'/Hardware Summary F5.png')
        document.add_picture(img_hardware, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Hardware Summary Table')
        p.style = document.styles['Heading 2']

        #HARDWARE SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT tipe, date, COUNT(*) FROM eosltable GROUP BY tipe''')
        records = cursor.fetchall()
        cursor.execute('''SELECT COUNT(tipe) FROM eosltable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Model'
        hdr_cells[1].text = 'Total'
        hdr_cells[2].text = 'Percentage'
        hdr_cells[3].text = 'End of Support'

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(1.69)
        hdr_cells[2].width = Cm(2.5)
        hdr_cells[3].width = Cm(4.25)

        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(1.69)
            row_cells[2].width = Cm(2.5)
            row_cells[3].width = Cm(4.25)

            row_cells[0].text = (row[0])
            row_cells[1].text = str(row[2])

            percentage_str = (str((row[2]/total)*100))

            convert_float = float(percentage_str)
            convert_round = round(convert_float, 1)
            percentage = str(convert_round)


            row_cells[2].text = (percentage+'%')

            if 'Isi Manual' in row[1]:
                row_cells[3].text = '-'
                run1 = row_cells[3].paragraphs[0].runs[0]
                run1.font.color.rgb = red
            else:
                row_cells[3].text = (row[1])

        document.add_paragraph()
        
        p = document.add_paragraph('Hardware Condition Analysis')
        p.style = document.styles['Heading 2']
        #SHOW ENVIRONMENT
        #sql query
        cursor.execute('''SELECT * FROM envtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=4)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'System'
        hdr_cells[2].text = 'Item'   
        hdr_cells[3].text = 'Status'

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(2.44)
        hdr_cells[2].width = Cm(7)
        hdr_cells[3].width = Cm(3.3)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(2.44)
            row_cells[2].width = Cm(7)
            row_cells[3].width = Cm(3.3)

            if row_check == row[1]:
                pass
            else:
                row_check = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[1] and iteration_check == False:
                row_cells[0].text = (row[1])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[2]) 
            row_cells[2].text = (row[3])
            row_cells[3].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        document.add_paragraph()
        p = document.add_paragraph('Hardware Card Table')
        p.style = document.styles['Heading 2']

        #HARDWARE CARD TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, tipe, sn, eosl_date FROM hwcardtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Type'
        hdr_cells[3].text = 'Serial Number'
        hdr_cells[4].text = 'End of Support'
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1
        for row in records:
            if row_check != row[0] and iteration_check == False:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)

                row_check = row[0]
                row_detail = row[1]
                iteration_check = False
                start_row = table.cell(count_row, 0)
                start_row1 = table.cell(count_row, 1)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])

                if 'Isi Manual' in row[4]:
                    row_cells[4].text = '-'
                    run1 = row_cells[4].paragraphs[0].runs[0]
                    run1.font.color.rgb = red

                else:
                    row_cells[4].text = (row[4])

                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_detail in row[1] and iteration_check == False:
                row_cells = table.add_row().cells
                row_check = row[0]
                row_detail = row[1]
                
                row_cells[2].text = (row[2])
                row_cells[3].text = (row[3])

                if 'Isi Manual' in row[4]:
                    row_cells[4].text = '-'
                    run1 = row_cells[4].paragraphs[0].runs[0]
                    run1.font.color.rgb = red

                else:
                    row_cells[4].text = (row[4])

                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)
                end_row1 = table.cell(count_row, 1)
                merge_row1 = start_row1.merge(end_row1)
                iteration_check == True
                count_row+=1

            # row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            # row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        
        #Hardware Summary Table, Hardware Condition Analysis and Hardware Card Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the switch model type. The table is sorted by switch type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Total')
        p.style = document.styles['Title']
        p = document.add_paragraph('Total count of hardware model.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Percentage')
        p.style = document.styles['Title']
        p = document.add_paragraph('Percentage of hardware model.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('End of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date to receive service and support for the product. After this date, all support services for the product are unavailable, and the product becomes obsolete.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('System')
        p.style = document.styles['Title']
        p = document.add_paragraph('System refers to power supply, fan and temperature of hardware.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Item')
        p.style = document.styles['Title']
        p = document.add_paragraph('Item refers to type or model of power supply, fan and temperature.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status refers to condition or status of power supply, fan and temperature.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Type')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Type column refers to the product model type. The table is sorted by product type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Serial Number')
        p.style = document.styles['Title']
        p = document.add_paragraph('The serial number column refers to the unique identifier for each card in the switch.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Hardware Analysis Summary')
        p.style = document.styles['Title']  
        
        #Konidis Hardware Analis
        cursor.execute('''SELECT tipe, date, COUNT(*) FROM eosltable where DATE(date) \u003c= DATE('now') GROUP BY tipe''')
        records = cursor.fetchall()
        qr9 = "SELECT COUNT(tipe) FROM eosltable"
        cursor.execute('''SELECT COUNT(tipe) FROM eosltable''')
        total = cursor.fetchall()
        total = (str(total))
        total = re.sub("\D", "", total)
        total = int(total)

        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<Hardware analysis must be summarized in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)

        else:
            p = document.add_paragraph('Hardware Analysis found hardware that has exceeded end of support. List of models is explained below:')
            p.style = document.styles['No Spacing']
            # add to document
            table = document.add_table(rows=1, cols=3)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].text = 'Model'
            hdr_cells[1].text = 'Total'
            hdr_cells[2].text = 'End of Support'

            hdr_cells[0].width = Cm(4.25)
            hdr_cells[1].width = Cm(1.69)
            hdr_cells[2].width = Cm(4.25)

            for row in records:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.25)
                row_cells[1].width = Cm(1.69)
                row_cells[2].width = Cm(4.25)

                row_cells[0].text = (row[0])
                row_cells[1].text = str(row[2])
                row_cells[2].text = (row[1])

        document.add_page_break()
        
        #Processor Analysis
        p = document.add_paragraph('Processor Analysis of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('CPU Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)


        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_cpu = (capture_path+'/CPU Summary F5.png')
        document.add_picture(img_cpu, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('CPU Summary Table')
        p.style = document.styles['Heading 2']

        #CPU SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, model, cpu_current, cpu_30d, cpu_status FROM cputable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Model'
        hdr_cells[2].text = 'Current (%)'
        hdr_cells[3].text = '30 Days (%)'
        hdr_cells[4].text = 'Status'

        for row in records:

            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            convert_float2 = float(row[2])
            convert_round2 = round(convert_float2, 2)
            convert_str2 = str(convert_round2)

            convert_float3 = float(row[3])
            convert_round3 = round(convert_float3, 2)
            convert_str3 = str(convert_round3)

            row_cells[0].text = (row[0])
            row_cells[1].text = (row[1])
            row_cells[2].text = (convert_str2+'%')
            row_cells[3].text = (convert_str3+'%')
            row_cells[4].text = (row[4])

        #CPU Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Model column refers to the switch model type. The table is sorted by switch type, making comparisons a simple process.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)        

        p = document.add_paragraph('Current (%)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Current (%) column refers to the actual CPU utilization in current condition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('30 Days (%)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The 30 Days (%) column refers to the average CPU utilization in 30 days.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('CPU Analysis Summary')
        p.style = document.styles['Title']
        #Kondisi CPU
        cursor.execute('''SELECT devicename, model, cpu_current, cpu_30d, cpu_status FROM cputable WHERE cpu_status = 'High' ORDER by devicename ASC''')
        records = cursor.fetchall()

        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<CPU analysis summary must be analyzed in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)
        
        else:
            p = document.add_paragraph('Processor Analysis found '+str(len(records))+' problems on all devices.')
            p.style = document.styles['No Spacing']

            # Tabel CPU UCM
            table = document.add_table(rows=1, cols=5)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].width = Cm(5.94)
            hdr_cells[1].width = Cm(4.75)
            hdr_cells[2].width = Cm(2.25)
            hdr_cells[3].width = Cm(2.25)
            hdr_cells[4].width = Cm(2)

            hdr_cells[0].text = 'Device Name'
            hdr_cells[1].text = 'Model'
            hdr_cells[2].text = 'Current (%)'
            hdr_cells[3].text = '30 Days (%)'
            hdr_cells[4].text = 'Status'

            for row in records:

                row_cells = table.add_row().cells
                row_cells[0].width = Cm(5.94)
                row_cells[1].width = Cm(4.75)
                row_cells[2].width = Cm(2.25)
                row_cells[3].width = Cm(2.25)
                row_cells[4].width = Cm(2)

                convert_float2 = float(row[2])
                convert_round2 = round(convert_float2, 2)
                convert_str2 = str(convert_round2)

                convert_float3 = float(row[3])
                convert_round3 = round(convert_float3, 2)
                convert_str3 = str(convert_round3)

                row_cells[0].text = (row[0])
                row_cells[1].text = (row[1])
                row_cells[2].text = (convert_str2+'%')
                row_cells[3].text = (convert_str3+'%')
                row_cells[4].text = (row[4])
 
        document.add_page_break()
        
        #Memory Analysis
        p = document.add_paragraph('Memory Analysis of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Memory Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_memory = (capture_path+'/Memory Summary F5.png')
        document.add_picture(img_memory, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_paragraph()
        p = document.add_paragraph('Memory Summary Table')
        p.style = document.styles['Heading 2']

        #MEMORY SUMMARY TABLE
        #sql query
        cursor.execute('''SELECT devicename, memory_name, memory_current, memory_30d, memory_status FROM memtable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Memory Description'
        hdr_cells[2].text = 'Current (%)'
        hdr_cells[3].text = '30 Days (%)'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            convert_float2 = float(row[2])
            convert_round2 = round(convert_float2, 2)
            convert_str2 = str(convert_round2)

            convert_float3 = float(row[3])
            convert_round3 = round(convert_float3, 2)
            convert_str3 = str(convert_round3)

            row_cells[1].text = (row[1])
            row_cells[2].text = (convert_str2+'%')
            row_cells[3].text = (convert_str3+'%')
            row_cells[4].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #Memory Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Memory Description')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Memory Description column refers to the type of memory allocation in the devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)        

        p = document.add_paragraph('Current (%)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Current (%) column refers to the actual memory utilization in current condition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('30 Days (%)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The 30 Days (%) column refers to the average memory utilization in 30 days.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Memory Analysis Summary')
        p.style = document.styles['Title']
        #Kondisi Memory
        cursor.execute('''SELECT devicename, memory_name, memory_current, memory_30d, memory_status FROM memtable WHERE memory_status = 'High' ORDER by devicename ASC''')
        records = cursor.fetchall()

        if len(records) == 0:
            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<Memory analysis must be summarized in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)
        
        else:
            p = document.add_paragraph('Memory Analysis found '+str(len(records))+' problems on all devices.')
            p.style = document.styles['No Spacing']
        
            # Tabel Memory F5
            table = document.add_table(rows=1, cols=5)
            table.alignment = WD_TABLE_ALIGNMENT.CENTER
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].width = Cm(5.94)
            hdr_cells[1].width = Cm(4.75)
            hdr_cells[2].width = Cm(2.25)
            hdr_cells[3].width = Cm(2.25)
            hdr_cells[4].width = Cm(2)

            hdr_cells[0].text = 'Device Name'
            hdr_cells[1].text = 'Memory Description'
            hdr_cells[2].text = 'Current (%)'
            hdr_cells[3].text = '30 Days (%)'
            hdr_cells[4].text = 'Status'

            iteration_check = False
            row_check = 'ludesdeveloper'
            count_row = 1       
            for row in records:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(5.94)
                row_cells[1].width = Cm(4.75)
                row_cells[2].width = Cm(2.25)
                row_cells[3].width = Cm(2.25)
                row_cells[4].width = Cm(2)

                if row_check == row[0]:
                    pass
                else:
                    row_check = row[0]
                    iteration_check = False
                    start_row = table.cell(count_row, 0)
                if row_check == row[0] and iteration_check == False:
                    row_cells[0].text = (row[0])
                    iteration_check = True
                else:
                    end_row = table.cell(count_row, 0)
                    merge_row = start_row.merge(end_row)

                convert_float2 = float(row[2])
                convert_round2 = round(convert_float2, 2)
                convert_str2 = str(convert_round2)

                convert_float3 = float(row[3])
                convert_round3 = round(convert_float3, 2)
                convert_str3 = str(convert_round3)

                row_cells[1].text = (row[1])
                row_cells[2].text = (convert_str2+'%')
                row_cells[3].text = (convert_str3+'%')
                row_cells[4].text = (row[4])

                row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                count_row+=1

        document.add_page_break()
        
        #THROUGHPUT ANALYSIS
        p = document.add_paragraph('Throughput Analysis of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Throughput Summary Table')
        p.style = document.styles['Heading 2']

        #THROUGHPUT SUMMARY TABLE
        #sql query throughputbitstable
        cursor.execute('''SELECT devicename, throughput_bits_name, throughput_bits_current, throughput_bits_30d, throughput_bits_status FROM throughputbitstable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Throughput (bits)'
        hdr_cells[2].text = 'Current'
        hdr_cells[3].text = '30 Days'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            #Tulisan jadi Merah
            run = row_cells[4].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1
        
        document.add_paragraph()
        
        #sql query throughputssltable
        cursor.execute('''SELECT devicename, throughput_ssl_name, throughput_ssl_current, throughput_ssl_30d, throughput_ssl_status FROM throughputssltable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'SSL Transcation'
        hdr_cells[2].text = 'Current'
        hdr_cells[3].text = '30 Days'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            #Tulisan jadi Merah
            run = row_cells[4].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        document.add_paragraph()
        
        #sql query throughputpackettable
        cursor.execute('''SELECT devicename, throughput_packet_name, throughput_packet_current, throughput_packet_30d, throughput_packet_status FROM throughputpackettable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Throughput (packets)'
        hdr_cells[2].text = 'Current'
        hdr_cells[3].text = '30 Days'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            #Tulisan jadi Merah
            run = row_cells[4].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #Throughput Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Throughput (bits)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Throughput (bits) column refers to the total throughput in and out of the BIG-IP system collected from all interfaces in bits.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)      

        p = document.add_paragraph('SSL Transcation')
        p.style = document.styles['Title']
        p = document.add_paragraph('The SSL Transaction column refers to number of SSL (Secure Sockets Layer) Transactions per Second.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)   

        p = document.add_paragraph('Throughput (packets)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Throughput (bits) column refers to the total throughput in and out of the BIG-IP system collected from all interfaces in packets.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)  

        p = document.add_paragraph('Current')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Current column refers to the actual throughput packets in current condition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('30 Days')
        p.style = document.styles['Title']
        p = document.add_paragraph('The 30 Days column refers to the average throughput packets in 30 days.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Throughput Analysis Summary')
        p.style = document.styles['Title']

        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Throughput analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #CONNECTIONS ANALYSIS
        p = document.add_paragraph('Connection Analysis of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Connection Summary Table')
        p.style = document.styles['Heading 2']

        #CONNECTIONS SUMMARY TABLE
        #sql query connectiontable
        cursor.execute('''SELECT devicename, connection_name, connection_current, connection_30d, connection_status FROM connectiontable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Active Connection'
        hdr_cells[2].text = 'Current'
        hdr_cells[3].text = '30 Days'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            #Tulisan jadi Merah
            run = row_cells[4].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1
        
        document.add_paragraph()
        
        #sql query newconnectiontable
        cursor.execute('''SELECT devicename, newconnection_name, newconnection_current, newconnection_30d, newconnection_status FROM newconnectiontable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Total New Connection (/sec)'
        hdr_cells[2].text = 'Current'
        hdr_cells[3].text = '30 Days'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            #Tulisan jadi Merah
            run = row_cells[4].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        document.add_paragraph()
        
        #sql query httprequeststable
        cursor.execute('''SELECT devicename, httprequests_name, httprequests_current, httprequests_30d, httprequests_status FROM httprequeststable ORDER by devicename ASC''')
        records = cursor.fetchall()
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(5.94)
        hdr_cells[1].width = Cm(4.75)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'HTTP Request (/sec)'
        hdr_cells[2].text = 'Current'
        hdr_cells[3].text = '30 Days'
        hdr_cells[4].text = 'Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(5.94)
            row_cells[1].width = Cm(4.75)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            #Tulisan jadi Merah
            run = row_cells[4].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #Connection Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Active Connection')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Active Connection column refers to the active connection in device.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)      

        p = document.add_paragraph('Total New Connection (/sec)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Total New Connection (/sec) column refers to the total new connection in device per second.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)   

        p = document.add_paragraph('HTTP Request (/sec)')
        p.style = document.styles['Title']
        p = document.add_paragraph('The HTTP Request (/sec) column refers to the HTTP Request connection in device per second.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)  

        p = document.add_paragraph('Current')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Current column refers to the actual connection or request in current condition.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('30 Days')
        p.style = document.styles['Title']
        p = document.add_paragraph('The 30 Days column refers to the average connection or request in 30 days.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Status threshold: ')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-20%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 21-80%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 81-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Connection Analysis Summary')
        p.style = document.styles['Title']

        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Connection analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #LTM ANALYSIS
        #sql query LTM
        cursor.execute('''SELECT devicename, ltm_tipe,
                sum(case when availability = 'available' then 1 else 0 end) AS Available,
                sum(case when availability = 'unavailable' then 1 else 0 end) AS Unavailable,
                sum(case when availability = 'offline' then 1 else 0 end) AS Offline,
                sum(case when availability = 'unknown' then 1 else 0 end) AS Unknown,
                sum(case when availability = 'available' and state LIKE 'disabled%' then 1 else 0 end) AS AvDisable,
                sum(case when availability = 'unavailable' and state LIKE 'disabled%' then 1 else 0 end) AS UnaDisable,
                sum(case when availability = 'offline' and state LIKE 'disabled%' then 1 else 0 end) AS OffDisable,
                sum(case when availability = 'unknown' and state LIKE 'disabled%' then 1 else 0 end) AS UnkDisable,
                count(*) AS Total
                FROM ltmtable
                GROUP BY ltm_tipe, devicename
                ORDER BY devicename, ltm_tipe DESC''')
        records = cursor.fetchall() 

        if len(records) == 0:
            p = document.add_paragraph('LTM Analysis of F5 Device')
            p.style = document.styles['Heading 1']

            p = document.add_paragraph('Not Found LTM')
            p.style = document.styles['Heading 2']

        else:
            p = document.add_paragraph('LTM Analysis of F5 Device')
            p.style = document.styles['Heading 1']

            p = document.add_paragraph('LTM Summary Table')
            p.style = document.styles['Heading 2']

            #LTM SUMMARY TABLE
            table = document.add_table(rows=1, cols=7)
            table.allow_autofit = True
            qr46 = func_get_req(docx_create+"qr46")
            table.style = qr46
            hdr_cells = table.rows[0].cells
            set_repeat_table_header(table.rows[0])
            table.rows[0].height = Cm(1.2)

            hdr_cells[0].width = Cm(4.13)
            hdr_cells[1].width = Cm(3.34)
            hdr_cells[2].width = Cm(1.58)
            hdr_cells[3].width = Cm(1.64)
            hdr_cells[4].width = Cm(1.87)
            hdr_cells[5].width = Cm(1.45)
            hdr_cells[6].width = Cm(1.68)

            hdr_cells[0].text = 'Device Name'
            hdr_cells[1].text = 'LTM'
            hdr_cells[2].text = 'Total'
            hdr_cells[3].text = 'Available'
            hdr_cells[4].text = 'Unavailable'
            hdr_cells[5].text = 'Offline'
            hdr_cells[6].text = 'Unknown'

            iteration_check = False
            row_check = 'ludesdeveloper'
            count_row = 1       
            for row in records:
                row_cells = table.add_row().cells
                row_cells[0].width = Cm(4.13)
                row_cells[1].width = Cm(3.34)
                row_cells[2].width = Cm(1.58)
                row_cells[3].width = Cm(1.64)
                row_cells[4].width = Cm(1.87)
                row_cells[5].width = Cm(1.45)
                row_cells[6].width = Cm(1.68)

                if row_check == row[0]:
                    pass
                else:
                    row_check = row[0]
                    iteration_check = False
                    start_row = table.cell(count_row, 0)
                if row_check == row[0] and iteration_check == False:
                    row_cells[0].text = (row[0])
                    iteration_check = True
                else:
                    end_row = table.cell(count_row, 0)
                    merge_row = start_row.merge(end_row)

                row_cells[1].text = (row[1])
                row_cells[2].text = (str(row[10]))

                if row[2] == 0:
                    row_cells[3].text = (str(row[2]))
                elif row[6] == 0:
                    row_cells[3].text = (str(row[2]))
                else:
                    row_cells[3].text = (str(row[2])+' (' +str(row[6])+ ' Disabled)')

                if row[3] == 0:
                    row_cells[4].text = (str(row[3]))
                elif row[7] == 0:
                    row_cells[4].text = (str(row[3]))
                else:
                    row_cells[4].text = (str(row[3])+' (' +str(row[7])+ ' Disabled)')
                
                if row[4] == 0:
                    row_cells[5].text = (str(row[4]))
                elif row[8] == 0:
                    row_cells[5].text = (str(row[4]))
                else:
                    row_cells[5].text = (str(row[4])+ ' (' +str(row[8])+ ' Disabled)')
                
                if row[5] == 0:
                    row_cells[6].text = (str(row[5]))
                elif row[9] == 0:
                    row_cells[6].text = (str(row[5]))
                else:
                    row_cells[6].text = (str(row[5])+ ' (' +str(row[9])+ ' Disabled)')

                row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                count_row+=1
            
            document.add_paragraph()
            
            #sql query LTM Virtual Server
            p = document.add_paragraph('Virtual Server Summary Table')
            p.style = document.styles['Heading 2']

            cursor.execute('''SELECT devicename, ltm_name, availability, state FROM ltmtable WHERE ltm_tipe = 'Virtual Server'
                    GROUP BY ltm_name
                    ORDER BY ltm_name ASC''')
            records = cursor.fetchall()

            if len(records) > 25:
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('Copy data Virtual Server summary from file f5.xlxs')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)
                document.add_paragraph()

            else:
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.13)
                hdr_cells[1].width = Cm(5)
                hdr_cells[2].width = Cm(2.25)
                hdr_cells[3].width = Cm(3.5)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Virtual Server Name'
                hdr_cells[2].text = 'Availability'
                hdr_cells[3].text = 'State'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1       
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.13)
                    row_cells[1].width = Cm(5)
                    row_cells[2].width = Cm(2.25)
                    row_cells[3].width = Cm(3.5)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)

                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                    count_row+=1
                
                document.add_paragraph()
            
            #sql query LTM Pool
            p = document.add_paragraph('Pool Summary Table')
            p.style = document.styles['Heading 2']

            cursor.execute('''SELECT devicename, ltm_name, availability, state FROM ltmtable WHERE ltm_tipe = 'Pool'
                    GROUP BY ltm_name
                    ORDER BY ltm_name ASC''')
            records = cursor.fetchall()

            if len(records) > 25:
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('Copy data Pool Summary from file f5.xlxs')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)
                document.add_paragraph()
            
            else:
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.13)
                hdr_cells[1].width = Cm(5)
                hdr_cells[2].width = Cm(2.25)
                hdr_cells[3].width = Cm(3.5)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Pool Name'
                hdr_cells[2].text = 'Availability'
                hdr_cells[3].text = 'State'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1       
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.13)
                    row_cells[1].width = Cm(5)
                    row_cells[2].width = Cm(2.25)
                    row_cells[3].width = Cm(3.5)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)

                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                    count_row+=1
                
                document.add_paragraph()

            # #sql query LTM Node
            p = document.add_paragraph('Node Summary Table')
            p.style = document.styles['Heading 2']

            cursor.execute('''SELECT devicename, ltm_name, availability, state FROM ltmtable WHERE ltm_tipe = 'Node'
                    GROUP BY ltm_name
                    ORDER BY ltm_name ASC''')
            records = cursor.fetchall()

            if len(records) > 25:
                p = document.add_paragraph()
                p.style = document.styles['No Spacing']
                run = p.add_run('Copy data Node Summary from file f5.xlxs')
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)
                document.add_paragraph()
            
            else:
                table = document.add_table(rows=1, cols=4)
                table.allow_autofit = True
                qr46 = func_get_req(docx_create+"qr46")
                table.style = qr46
                hdr_cells = table.rows[0].cells
                set_repeat_table_header(table.rows[0])
                table.rows[0].height = Cm(1.2)

                hdr_cells[0].width = Cm(4.13)
                hdr_cells[1].width = Cm(5)
                hdr_cells[2].width = Cm(2.25)
                hdr_cells[3].width = Cm(3.5)

                hdr_cells[0].text = 'Device Name'
                hdr_cells[1].text = 'Node Name'
                hdr_cells[2].text = 'Availability'
                hdr_cells[3].text = 'State'

                iteration_check = False
                row_check = 'ludesdeveloper'
                count_row = 1       
                for row in records:
                    row_cells = table.add_row().cells
                    row_cells[0].width = Cm(4.13)
                    row_cells[1].width = Cm(5)
                    row_cells[2].width = Cm(2.25)
                    row_cells[3].width = Cm(3.5)

                    if row_check == row[0]:
                        pass
                    else:
                        row_check = row[0]
                        iteration_check = False
                        start_row = table.cell(count_row, 0)
                    if row_check == row[0] and iteration_check == False:
                        row_cells[0].text = (row[0])
                        iteration_check = True
                    else:
                        end_row = table.cell(count_row, 0)
                        merge_row = start_row.merge(end_row)

                    row_cells[1].text = (row[1])
                    row_cells[2].text = (row[2])
                    row_cells[3].text = (row[3])

                    row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
                    row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

                    count_row+=1
                
                document.add_paragraph()

            #LTM Summary Table Explanation
            p = document.add_paragraph('Device Name')
            p.style = document.styles['Title']
            p.paragraph_format.space_before = Pt(6)
            p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('LTM')
            p.style = document.styles['Title']
            p = document.add_paragraph('The LTM column refers to the LTM objects on the devices.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)      

            p = document.add_paragraph('Total')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Total column refers to the total LTM objects on the devices.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)   

            p = document.add_paragraph('Available')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Available column refers to the total LTM objects which available on the devices.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)  

            p = document.add_paragraph('Unavailable')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Unavailable column refers to the total LTM objects which unavailable on the devices.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('Offline')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Offline column refers to the total LTM objects which offline on the devices.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('Unknown')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Unknown column refers to the total LTM objects which unknown on the devices.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('Disabled')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Disabled column refers to the total LTM objects which disabled on the devices.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('Virtual Server Name')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Virtual Server Name column refers to Virtual Server name.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('Pool Name')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Pool Server Name column refers to Pool name.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('Node Name')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Node Server Name column refers to Node name.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('Availability')
            p.style = document.styles['Title']
            p = document.add_paragraph('The Availability column refers to the LTM objects status.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('State')
            p.style = document.styles['Title']
            p = document.add_paragraph('The State column refers to the enabled and disabled status of the LTM objects.')
            p.style = document.styles['No Spacing']
            p.paragraph_format.space_after = Pt(6)

            p = document.add_paragraph('LTM Analysis Summary')
            p.style = document.styles['Title']

            p = document.add_paragraph()
            p.style = document.styles['No Spacing']
            run = p.add_run('<LTM analysis must be summarized in here>')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)

            document.add_page_break()

        #HA ANALYSIS
        p = document.add_paragraph('High Availability of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('HA Summary Table')
        p.style = document.styles['Heading 2']

        cursor.execute('''SELECT devicename, device, mgmt_ip, configsync_ip, ha_state, ha_status FROM hatable''')
        records = cursor.fetchall() 

        table = document.add_table(rows=1, cols=6)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(4.13)
        hdr_cells[1].width = Cm(5)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(3.5)
        hdr_cells[4].width = Cm(2.83)
        hdr_cells[5].width = Cm(2.83)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'HA Hostname'
        hdr_cells[2].text = 'Mgmt IP'
        hdr_cells[3].text = 'Configsync IP'
        hdr_cells[4].text = 'Device HA State'
        hdr_cells[5].text = 'Status HA'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.13)
            row_cells[1].width = Cm(5)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(3.5)
            row_cells[4].width = Cm(2.83)
            row_cells[5].width = Cm(2.83)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5])

            #Tulisan jadi Merah
            run = row_cells[5].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1
        
        document.add_paragraph()
        
        #sql query HA SYNC STATUS
        p = document.add_paragraph('HA Sync Summary Table')
        p.style = document.styles['Heading 2']

        cursor.execute('''SELECT devicename, sync_status, action FROM synchatable''')
        records = cursor.fetchall() 

        table = document.add_table(rows=1, cols=3)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(4.13)
        hdr_cells[1].width = Cm(5)
        hdr_cells[2].width = Cm(9)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Sync Status'
        hdr_cells[2].text = 'Recommended Action'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.13)
            row_cells[1].width = Cm(5)
            row_cells[2].width = Cm(9)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            row_cells[2].vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1
        
        document.add_paragraph()

        #sql query TRAFFIC HA
        p = document.add_paragraph('Traffic Group Summary Table')
        p.style = document.styles['Heading 2']

        cursor.execute('''SELECT devicename, traffic_group, device_traffic, traffic_status, next_active, previous_active, active_reason FROM traffichatable''')
        records = cursor.fetchall() 

        table = document.add_table(rows=1, cols=7)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(4.13)
        hdr_cells[1].width = Cm(5.58)
        hdr_cells[2].width = Cm(4.13)
        hdr_cells[3].width = Cm(1.54)
        hdr_cells[4].width = Cm(1.41)
        hdr_cells[5].width = Cm(1.72)
        hdr_cells[6].width = Cm(1.56)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Traffic Group'
        hdr_cells[2].text = 'Traffic Hostname'
        hdr_cells[3].text = 'Status Traffic'
        hdr_cells[4].text = 'Next Active'
        hdr_cells[5].text = 'Previous Active'
        hdr_cells[6].text = 'Active Reason'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.13)
            row_cells[1].width = Cm(5.58)
            row_cells[2].width = Cm(4.13)
            row_cells[3].width = Cm(1.54)
            row_cells[4].width = Cm(1.41)
            row_cells[5].width = Cm(1.72)
            row_cells[6].width = Cm(1.56)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5])
            row_cells[6].text = (row[6])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #LTM Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('HA Hostname')
        p.style = document.styles['Title']
        p = document.add_paragraph('The HA Hostname column refers to the Hostname of the HA devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)      

        p = document.add_paragraph('Traffic Hostname')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Traffic Hostname column refers to the Hostname of the Traffic devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)   

        p = document.add_paragraph('Mgmt IP')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Mgmt IP column refers to the Management IP Address of the devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)  

        p = document.add_paragraph('Configsync IP')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Configsync IP column refers to the Configsync IP Address of the devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Device HA State')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Device HA State column refers to the Device HA State of the devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status HA')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Status HA column refers to the HA Status of the devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Sync Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Sync Status column refers to the Sync Status of the HA devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Recommended Action')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Recommended column refers to the Sync Status of the HA devices')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Traffic Group')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Traffic Group column refers to the traffic group name of the HA devices.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status Traffic')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Status column refers to the HA device traffic status.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Next Active')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Next Active column refers to the Next Active status of HA devices in traffic group.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Previous Active')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Previous Active column refers to the Previous Active status of HA devices in traffic group.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Active Reason')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Active Reason column refers to reason the HA devices being active.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('High Availability Analysis Summary')
        p.style = document.styles['Title']

        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<High Availability analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #CERTIFICATE ANALYSIS
        p = document.add_paragraph('Certificate of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Certificate Summary Table')
        p.style = document.styles['Heading 2']

        cursor.execute('''SELECT devicename, cert_name, cn, expired_date, cert_status FROM certtable ORDER by cert_name ASC''')
        records = cursor.fetchall() 

        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        table.autofit = False
        table.allow_autofit = False

        hdr_cells[0].width = Cm(2.91)
        hdr_cells[1].width = Cm(4.3)
        hdr_cells[2].width = Cm(5.98)
        hdr_cells[3].width = Cm(2)
        hdr_cells[4].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Certificate Name'
        hdr_cells[2].text = 'CN'
        hdr_cells[3].text = 'Expired Date'
        hdr_cells[4].text = 'Certificate Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(2.91)
            row_cells[1].width = Cm(4.3)
            row_cells[2].width = Cm(5.98)
            row_cells[3].width = Cm(2)
            row_cells[4].width = Cm(2)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1
        
        #Certificate Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Certificate Name')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Certificate Name column refers to the SSL Certificate Name.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)      

        p = document.add_paragraph('CN')
        p.style = document.styles['Title']
        p = document.add_paragraph('The CN column refers to the Canonical Name of the SSL Certificate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)   

        p = document.add_paragraph('Expired Date')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Expired Date column refers to the certificate expired date.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)  

        p = document.add_paragraph('Certificate Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Certificate Status column refers to status of the certificate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Certificate Analysis Summary')
        p.style = document.styles['Title']

        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Certificate analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #LICENSE AND MODULE ANALYSIS
        p = document.add_paragraph('License and Module of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('License and Module Summary Table')
        p.style = document.styles['Heading 2']

        cursor.execute('''SELECT devicename, lic_version, register_key, active_module, lic_on, svc_date, lic_status FROM lictable''')
        records = cursor.fetchall() 

        table = document.add_table(rows=1, cols=7)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(4.13)
        hdr_cells[1].width = Cm(1.81)
        hdr_cells[2].width = Cm(2.26)
        hdr_cells[3].width = Cm(4.24)
        hdr_cells[4].width = Cm(2.1)
        hdr_cells[5].width = Cm(2.07)
        hdr_cells[6].width = Cm(1.53)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Licensed Version'
        hdr_cells[2].text = 'Registration Key'
        hdr_cells[3].text = 'Active Module'
        hdr_cells[4].text = 'Licensed On'
        hdr_cells[5].text = 'Service Check Date'
        hdr_cells[6].text = 'License Status'

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.13)
            row_cells[1].width = Cm(1.81)
            row_cells[2].width = Cm(2.26)
            row_cells[3].width = Cm(4.24)
            row_cells[4].width = Cm(2.1)
            row_cells[5].width = Cm(2.07)
            row_cells[6].width = Cm(1.53)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1])
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3])
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5])
            row_cells[6].text = (row[6])

            #Tulisan jadi Merah
            run = row_cells[6].paragraphs[0].runs[0]
            run.font.color.rgb = red

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[2].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[3].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[4].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[5].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[6].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[3].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #License and Module Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The Device Name column refers to the DNS or host name of the analyzed router. Device Names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Licensed Version')
        p.style = document.styles['Title']
        p = document.add_paragraph('Licensed Version column refers to the version of the license installed.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)      

        p = document.add_paragraph('Registration Key')
        p.style = document.styles['Title']
        p = document.add_paragraph('Registration Key column refers to the registration key of the licenses.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)   

        p = document.add_paragraph('Active Module')
        p.style = document.styles['Title']
        p = document.add_paragraph('Active Module column refers to the active module of the licenses.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)  

        p = document.add_paragraph('Licensed On')
        p.style = document.styles['Title']
        p = document.add_paragraph('Licensed On column refers to the date when the license installed.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Service Check Date')
        p.style = document.styles['Title']
        p = document.add_paragraph('Service Check Date column refers to the date the license was last activated or the date the service contract for the device expires, whichever date is earlier.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('License Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('License Status column refers to the status of the licenses.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('License and Module Analysis Summary')
        p.style = document.styles['Title']

        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<License and Module analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()
        
        #Log Analysis
        p = document.add_paragraph('Log Analysis of F5 Device')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('Log Summary Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)
        
        #Log Summary Table
        #sql query
        cursor.execute('''SELECT devicename FROM swtable ORDER by devicename ASC''')
        records = cursor.fetchall()

        #add to document
        table = document.add_table(rows=1, cols=3)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)

        hdr_cells[0].width = Cm(3.7)
        hdr_cells[1].width = Cm(11.3)
        hdr_cells[2].width = Cm(2)

        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'Log Event'
        hdr_cells[2].text = 'Severity'

        for row in records:
            row_cells = table.add_row().cells

            row_cells[0].width = Cm(3.7)
            row_cells[1].width = Cm(11.3)
            row_cells[2].width = Cm(2)

            row_cells[0].text = (row[0])
            row_cells[1].text = '-'
            row_cells[2].text = '-'

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            run1 = row_cells[1].paragraphs[0].runs[0]
            run2 = row_cells[2].paragraphs[0].runs[0]

            run1.font.color.rgb = red
            run2.font.color.rgb = red

        #Log Analysis Summary Table Explanation
        p = document.add_paragraph('Device Name')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('The device name column refers to the DNS or host name of the analyzed switch. Device names may be represented in either uppercase or lowercase, which may affect sorting.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Log Event')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Log events refer to the logging process of the device which indicates incident or informational information. This log could be an alert for the administrator to mitigate problem.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Severity')
        p.style = document.styles['Title']
        p = document.add_paragraph('The severity tables indicate the importance of the log. high severity means it would cause downtime or disruption to network. Medium severity means this log may lead to network disruption in the future. Low severity means the logs are for information only.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Log Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Log analysis summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        db.close()
        
        #save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('pm_f5.docx')
        print('Document has been saved to pm_f5.docx')