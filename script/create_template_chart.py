import xlsxwriter

def create_data_template():
    #Software Summary
    version = ['16.12.02','16.12.03','16.12.04','16.12.05','16.12.06',]
    total_v = ['1','2','3','1','2',]

    wb = xlsxwriter.Workbook('template_chart.xlsx')
    ws = wb.add_worksheet('Software Summary')

    ws.write(0,0,'Version')
    ws.write(0,1,'Total')

    count_row=1
    for i in version:
        ws.write(count_row,0,i)
        count_row+=1

    count_row=1
    for i in total_v:
        ws.write(count_row,1,i)
        count_row+=1
    
    #Hardware Summary
    model = ['C9300-24T','C9300-24T','C9300-24T','C9300-24T','C9300-24T',]
    total_m = ['1','2','3','1','2',]

    ws = wb.add_worksheet('Hardware Summary')

    ws.write(0,0,'Model')
    ws.write(0,1,'Total')

    count_row=1
    for i in model:
        ws.write(count_row,0,i)
        count_row+=1

    count_row=1
    for i in total_m:
        ws.write(count_row,1,i)
        count_row+=1
    
    #CPU Summary
    devicename = ['IDDRCSENTULCS','IDDRCSENTULCS','IDDRCSENTULCS','IDDRCSENTULCS','IDDRCSENTULCS',]
    total_cpu = ['20','20','30','10','20',]

    ws = wb.add_worksheet('CPU Summary')

    ws.write(0,0,'Devicename')
    ws.write(0,1,'Total')

    count_row=1
    for i in devicename:
        ws.write(count_row,0,i)
        count_row+=1

    count_row=1
    for i in total_cpu:
        ws.write(count_row,1,i)
        count_row+=1
    
    #Memory Summary
    devicename = ['IDDRCSENTULCS','IDDRCSENTULCS','IDDRCSENTULCS','IDDRCSENTULCS','IDDRCSENTULCS',]
    total_memory = ['20','20','30','10','20',]

    ws = wb.add_worksheet('Memory Summary')

    ws.write(0,0,'Devicename')
    ws.write(0,1,'Total')

    count_row=1
    for i in devicename:
        ws.write(count_row,0,i)
        count_row+=1

    count_row=1
    for i in total_memory:
        ws.write(count_row,1,i)
        count_row+=1

    wb.close()