from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor
from datetime import datetime

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path, 'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def func_get_req(address):
    try:
        os.chdir(main_path)
        files = ['init', 'init.exe']
        for file in files:
            os.chmod(file, 0o0777)
        address = re.findall(".*(qr.*)", address)
        address = address[0]
        if platform == "win32":
            cmd = ("init ")
            qr = subprocess.check_output(cmd+address, shell=True)
        else:
            cmd = ("./init ")
            qr = subprocess.check_output(cmd+address, shell=True)

        qr = str(qr)
        qr = re.findall("b\W(.*)\Wn\W", qr)
        qr = qr[0]
        return qr
    except:
        return

def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    qr45 = func_get_req(docx_create+"qr45")
    tblHeader = OxmlElement(qr45)
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_cover:
    # @staticmethod
    def __init__(self, customer, review, prepared, pic, bulan, tahun, report_type):
        # variable constructor
        self.customer = customer
        self.review = review
        self.prepared = prepared
        self.pic = pic
        self.bulan = bulan
        self.tahun = tahun
        self.report_type = report_type

    def convert_docx_cover(self):
        print('')
        print('Processing Document')
        os.chdir(capture_path)

        # using document docx module
        report_template = open(data_path+'cover_dispusip.docx', 'rb')
        document = Document(report_template)

        #JUDUL
        p = document.add_paragraph('PEMANFAATAN')
        p.style = document.styles['Heading 4']

        p = document.add_paragraph('PERANGKAT JARINGAN')
        p.style = document.styles['Heading 4']

        p = document.add_paragraph(self.bulan + ' ' + self.tahun)
        p.style = document.styles['Heading 4']

        document.add_page_break()

        # save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('pm_cover.docx')
        print('Document has been saved to pm_cover.docx')
