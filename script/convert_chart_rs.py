import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt; plt.rcdefaults()
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
import matplotlib.patches as mpatches
from numpy.random import rand
import numpy as np
import sqlite3
import re
import pkg_resources
import os
import xlrd

class convert_chart_rs:
    @staticmethod
    def convert_chart_rs():
        #open db connection
        db = sqlite3.connect('pmdb_rs')
        cursor = db.cursor()
        
        #SOFTWARE SUMMARY
        #sql query
        cursor.execute('''SELECT tipe, COUNT(*) FROM aset GROUP BY tipe''')
        count_version = cursor.fetchall()
        cursor.execute('''SELECT COUNT(tipe) FROM aset''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create list
        percentage=[]
        labels=[]
        for row in count_version:
            try:
                percentage.append((row[1]/int(total))*100)
                labels.append(row[0])
            except:
                pass
        #create chart

        plt.style.use("fivethirtyeight")

        if len(percentage) == 1:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'bold',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, labels=labels, pctdistance=0, labeldistance=-0.2, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=1)
            plt.setp(autotexts, size=12)
            for t in text:
                t.set_horizontalalignment('center')
            plt.title('Jenis Perangkat')
            plt.axis('equal')
            plt.tight_layout()

        elif len(percentage) < 25:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=2)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(1.6,1.3), loc="upper center")
            plt.title('Jenis Perangkat', pad=100.0)
        else:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=3)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(0.5,-1.1), loc="lower center", ncol=5)
            plt.title('Pemanfaatan Firmware', pad=220.0)
        #save image
        print('Saving Image')
        plt.savefig('Jenis Perangkat Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Jenis Perangkat Summary.png')

        #SOFTWARE SUMMARY
        #sql query
        cursor.execute('''SELECT version, COUNT(*) FROM swsumtable GROUP BY version''')
        count_version = cursor.fetchall()
        cursor.execute('''SELECT COUNT(version) FROM swsumtable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create list
        percentage=[]
        labels=[]
        for row in count_version:
            try:
                percentage.append((row[1]/int(total))*100)
                labels.append(row[0])
            except:
                pass
        #create chart

        plt.style.use("fivethirtyeight")

        if len(percentage) == 1:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'bold',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, labels=labels, pctdistance=0, labeldistance=-0.2, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=1)
            plt.setp(autotexts, size=12)
            for t in text:
                t.set_horizontalalignment('center')
            plt.title('Pemanfaatan Firmware')
            plt.axis('equal')
            plt.tight_layout()

        elif len(percentage) < 25:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=2)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(1.6,1.3), loc="upper center")
            plt.title('Pemanfaatan Firmware', pad=100.0)
        else:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=3)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(0.5,-1.1), loc="lower center", ncol=5)
            plt.title('Pemanfaatan Firmware', pad=220.0)
        #save image
        print('Saving Image')
        plt.savefig('Software Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Software Summary.png')


        #HARDWARE SUMMARY
        #sql query
        cursor.execute('''SELECT card, COUNT(*) FROM eosltable GROUP BY card''')
        count_model = cursor.fetchall()
        cursor.execute('''SELECT COUNT(card) FROM eosltable''')
        total = cursor.fetchall()
        total=(str(total))
        total=re.sub("\D", "", total)
        total=int(total)
        #create list
        percentage=[]
        labels=[]
        for row in count_model:
            try:
                percentage.append((row[1]/int(total))*100)
                labels.append(row[0])
            except:
                pass

        plt.style.use("fivethirtyeight")

        if len(percentage) == 1:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'bold',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, labels=labels, pctdistance=0, labeldistance=-0.2, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=1)
            plt.setp(autotexts, size=12)
            for t in text:
                t.set_horizontalalignment('center')
            plt.title('Pemanfaatan Perangkat Keras')
            plt.axis('equal')
            plt.tight_layout()

        elif len(percentage) < 25:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=2)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(1.6,1.3), loc="upper center")
            plt.title('Pemanfaatan Perangkat Keras', pad=100.0)
        else:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=3)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(0.5,-1.1), loc="lower center", ncol=5)
            plt.title('Pemanfaatan Perangkat Keras', pad=220.0)
        #save image
        print('Saving Image')
        plt.savefig('Hardware Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Hardware Summary.png')


        #CPU Summary
        #sql query
        cursor.execute('''SELECT devicename, total , process  FROM cpusumtable''')
        cpu = cursor.fetchall()
        cursor.execute('''SELECT COUNT(devicename) FROM cpusumtable''')
        total = cursor.fetchall()
        total = total[0]
        total = total [0]
        #create list
        devicename=[]
        cpu_total=[]
        for row in cpu:
            try:
                cpu_total.append(float(row[1]))
                if 'Member'in row[2] or 'Slot' in row[2]:
                    devicename.append(row[0]+'-'+row[2])
                else:
                    devicename.append(row[0])
            except:
                pass
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        # print(devicename)
        # print(cpu_total)
        # tulis = input("yooo")
        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(total))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, cpu_total, color=data)
            plt.title("Pemanfaatan CPU (Central Processing Unit)")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)
        
        elif len(devicename) < 19:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Pemanfaatan CPU (Central Processing Unit)")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0
            
            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        else:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Pemanfaatan CPU (Central Processing Unit)")
            plt.xticks([])

            #Create Legend Ke Bawah
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(0.5, -0.05), loc='upper center',
            fancybox=True, shadow=True, ncol=5)

        #save image
        print('Saving Image')
        plt.savefig('CPU Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to CPU Summary.png')

        #Memory Summary
        #sql query
        cursor.execute('''SELECT devicename, utils, topproc FROM memsumtable''')
        memory = cursor.fetchall()
        cursor.execute('''SELECT COUNT(devicename) FROM memsumtable''')
        total = cursor.fetchall()
        total = total[0]
        total = total [0]
        #create list
        devicename=[]
        memory_total=[]
        for row in memory:
            try:
                memory_total.append(float(row[1]))
                if 'Member'in row[2] or 'Slot' in row[2]:
                    devicename.append(row[0]+'-'+row[2])
                else:
                    devicename.append(row[0])
            except:
                pass
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(total))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, memory_total, color=data)
            plt.title("Pemanfaatan Penyimpanan (Memory)")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)
        
        elif len(devicename) < 19:
            plt.bar(x_pos, memory_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Pemanfaatan Penyimpanan (Memory)")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0
            
            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        else:
            plt.bar(x_pos, memory_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Pemanfaatan Penyimpanan (Memory)")
            plt.xticks([])

            #Create Legend Ke Bawah
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(0.5, -0.05), loc='upper center',
            fancybox=True, shadow=True, ncol=5)

        #save image
        print('Saving Image')
        plt.savefig('Memory Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Memory Summary.png')

        #close database
        db.close()

    @staticmethod
    def reconvert_chart():
        
        capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
        capture_path = os.path.join(capture_path,'capture/')

        #Software Summary
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        support_dir = (capture_path+'/template_chart.xlsx')
        book = xlrd.open_workbook(support_dir)
        first_sheet = book.sheet_by_index(0)
        cell = first_sheet.cell(0,0)

        version = []
        percentage = []
        total = []
        count_total = 1
        for i in range(first_sheet.nrows):
            try:
                total.append(int(first_sheet.row_values(count_total)[1]))
                count +=1
            except:
                pass

        subtotal = (sum(total))

        count_data = 1
        for i in range(first_sheet.nrows):
            try:
                version.append(first_sheet.row_values(count_data)[0])
                percentage.append(int(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
                count_data +=1
            except:
                pass

        plt.style.use("fivethirtyeight")

        if len(percentage) == 1:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'bold',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, labels=labels, pctdistance=0, labeldistance=-0.2, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=1)
            plt.setp(autotexts, size=12)
            for t in text:
                t.set_horizontalalignment('center')
            plt.title('Software Utilization')
            plt.axis('equal')
            plt.tight_layout()

        elif len(percentage) < 25:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=2)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(1.6,1.3), loc="upper center")
            plt.title('Software Utilization', pad=100.0)
        else:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=3)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(0.5,-1.1), loc="lower center", ncol=5)
            plt.title('Software Utilization', pad=220.0)
        #save image
        print('Saving Image')
        plt.savefig('Software Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Software Summary.png')
            
        #Hardware Summary
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        support_dir = (capture_path+'/template_chart.xlsx')
        book = xlrd.open_workbook(support_dir)
        first_sheet = book.sheet_by_index(1)
        cell = first_sheet.cell(0,0)

        model = []
        percentage = []
        total = []
        count_total = 1
        for i in range(first_sheet.nrows):
            try:
                total.append(int(first_sheet.row_values(count_total)[1]))
                count +=1
            except:
                pass

        subtotal = (sum(total))

        count_data = 1
        for i in range(first_sheet.nrows):
            try:
                model.append(first_sheet.row_values(count_data)[0])
                percentage.append(int(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
                count_data +=1
            except:
                pass

        plt.style.use("fivethirtyeight")

        if len(percentage) == 1:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'bold',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, labels=labels, pctdistance=0, labeldistance=-0.2, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=1)
            plt.setp(autotexts, size=12)
            for t in text:
                t.set_horizontalalignment('center')
            plt.title('Hardware Utilization')
            plt.axis('equal')
            plt.tight_layout()

        elif len(percentage) < 25:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=2)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(1.6,1.3), loc="upper center")
            plt.title('Hardware Utilization', pad=100.0)
        else:
            font = {'family' : 'DejaVu Sans',
                    'weight' : 'normal',
                    'size'   : 12}
            plt.rc('font', **font)

            wedges, text, autotexts = plt.pie(percentage, pctdistance=0.9, textprops=dict(color="w"), autopct='%1.1f%%', shadow=True, startangle=90, radius=3)
            plt.setp(autotexts, size=10)
            plt.legend(labels, bbox_to_anchor=(0.5,-1.1), loc="lower center", ncol=5)
            plt.title('Hardware Utilization', pad=220.0)
        #plt.title("Hardware Summary")
        #save image
        print('Saving Image')
        plt.savefig('Hardware Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Hardware Summary.png')

        #CPU Summary
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        support_dir = (capture_path+'/template_chart.xlsx')
        book = xlrd.open_workbook(support_dir)
        first_sheet = book.sheet_by_index(1)
        cell = first_sheet.cell(0,0)

        devicename = []
        percentage = []
        total = []
        count_total = 1
        for i in range(first_sheet.nrows):
            try:
                total.append(int(first_sheet.row_values(count_total)[1]))
                count +=1
            except:
                pass

        subtotal = (sum(total))

        count_data = 1
        for i in range(first_sheet.nrows):
            try:
                devicename.append(first_sheet.row_values(count_data)[0])
                percentage.append(float(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
                count_data +=1
            except:
                pass
        print (devicename)
        print (percentage)
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(subtotal))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, cpu_total, color=data)
            plt.title("CPU Utilization")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)
        
        elif len(devicename) < 19:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("CPU Utilization")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0
            
            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        else:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("CPU Utilization")
            plt.xticks([])

            #Create Legend Ke Bawah
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(0.5, -0.05), loc='upper center',
            fancybox=True, shadow=True, ncol=5)

        #save image
        print('Saving Image')
        plt.savefig('CPU Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to CPU Summary.png')

        #Memory Summary
        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        support_dir = (capture_path+'/template_chart.xlsx')
        book = xlrd.open_workbook(support_dir)
        first_sheet = book.sheet_by_index(1)
        cell = first_sheet.cell(0,0)

        devicename = []
        percentage = []
        total = []
        count_total = 1
        for i in range(first_sheet.nrows):
            try:
                total.append(int(first_sheet.row_values(count_total)[1]))
                count +=1
            except:
                pass

        subtotal = (sum(total))

        count_data = 1
        for i in range(first_sheet.nrows):
            try:
                devicename.append(first_sheet.row_values(count_data)[0])
                percentage.append(float(first_sheet.row_values(count_data)[1])/int(subtotal)*100)
                count_data +=1
            except:
                pass
        print (devicename)
        print (percentage)
        #create chart
        data = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']

        my_cmap = cm.get_cmap('jet')
        my_norm = Normalize(vmin=0, vmax=int(subtotal))
        plt.style.use('ggplot')
        plt.ylim(0, 100)
        x_pos = [i for i, _ in enumerate(devicename)]
        if len(devicename) == 1:
            plt.bar(x_pos, cpu_total, color=data)
            plt.title("Memory Utilization")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)
        
        elif len(devicename) < 19:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Memory Utilization")
            plt.xticks([])

            #Create Legend Ke Samping
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0
            
            plt.legend(handles=list_patch,bbox_to_anchor=(1, 1), loc=2,)

        else:
            plt.bar(x_pos, cpu_total, color=data, width=(x_pos[1]-x_pos[0])*1)
            plt.title("Memory Utilization")
            plt.xticks([])

            #Create Legend Ke Bawah
            list_patch = []
            count_color = 0
            for name in devicename:
                list_patch.append(mpatches.Patch(color=data[count_color], label=name))
                count_color += 1
                if count_color > 4:
                    count_color = 0

            plt.legend(handles=list_patch,bbox_to_anchor=(0.5, -0.05), loc='upper center',
            fancybox=True, shadow=True, ncol=5)

        #save image
        print('Saving Image')
        plt.savefig('Memory Summary.png', bbox_inches='tight')
        plt.close()
        print('Image saved to Memory Summary.png')


