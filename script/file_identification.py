import re

from netoprmgr.device_templates.cisco.cisco_None import cisco_None
from netoprmgr.device_templates.cisco.cisco_1905 import cisco_1905
from netoprmgr.device_templates.cisco.cisco_1921 import cisco_1921
from netoprmgr.device_templates.cisco.cisco_1941 import cisco_1941
from netoprmgr.device_templates.cisco.cisco_2801 import cisco_2801
from netoprmgr.device_templates.cisco.cisco_2811 import cisco_2811
from netoprmgr.device_templates.cisco.cisco_2821 import cisco_2821
from netoprmgr.device_templates.cisco.cisco_2901 import cisco_2901
from netoprmgr.device_templates.cisco.cisco_2911 import cisco_2911
from netoprmgr.device_templates.cisco.cisco_C1000X import cisco_C1000X
from netoprmgr.device_templates.cisco.cisco_C3750X import cisco_C3750X
from netoprmgr.device_templates.cisco.cisco_C4500X import cisco_C4500X
from netoprmgr.device_templates.cisco.cisco_C4506 import cisco_C4506
from netoprmgr.device_templates.cisco.cisco_C4507R import cisco_C4507R
from netoprmgr.device_templates.cisco.cisco_C4507RE import cisco_C4507RE
from netoprmgr.device_templates.cisco.cisco_C4900M import cisco_C4900M
from netoprmgr.device_templates.cisco.cisco_C4948 import cisco_C4948
from netoprmgr.device_templates.cisco.cisco_C6504 import cisco_C6504
from netoprmgr.device_templates.cisco.cisco_C6506 import cisco_C6506
from netoprmgr.device_templates.cisco.cisco_C2960 import cisco_C2960
from netoprmgr.device_templates.cisco.cisco_C2960C import cisco_C2960C
from netoprmgr.device_templates.cisco.cisco_C2960CX import cisco_C2960CX
from netoprmgr.device_templates.cisco.cisco_C2960L import cisco_C2960L
from netoprmgr.device_templates.cisco.cisco_C2960S import cisco_C2960S
from netoprmgr.device_templates.cisco.cisco_C2960X import cisco_C2960X
from netoprmgr.device_templates.cisco.cisco_C2960XR import cisco_C2960XR
from netoprmgr.device_templates.cisco.cisco_C3560 import cisco_C3560
from netoprmgr.device_templates.cisco.cisco_C3560C import cisco_C3560C
from netoprmgr.device_templates.cisco.cisco_C3560CG import cisco_C3560CG
from netoprmgr.device_templates.cisco.cisco_C3560CX import cisco_C3560CX
from netoprmgr.device_templates.cisco.cisco_C3560G import cisco_C3560G
from netoprmgr.device_templates.cisco.cisco_C3560V2 import cisco_C3560V2
from netoprmgr.device_templates.cisco.cisco_C3560X import cisco_C3560X
from netoprmgr.device_templates.cisco.cisco_C3650_E import cisco_C3650_E
from netoprmgr.device_templates.cisco.cisco_C3650 import cisco_C3650
from netoprmgr.device_templates.cisco.cisco_C3750 import cisco_C3750
from netoprmgr.device_templates.cisco.cisco_C3750E import cisco_C3750E
from netoprmgr.device_templates.cisco.cisco_C3750G import cisco_C3750G
from netoprmgr.device_templates.cisco.cisco_C3750V2 import cisco_C3750V2
from netoprmgr.device_templates.cisco.cisco_C3850 import cisco_C3850
from netoprmgr.device_templates.cisco.cisco_C6509 import cisco_C6509
from netoprmgr.device_templates.cisco.cisco_C6513 import cisco_C6513
from netoprmgr.device_templates.cisco.cisco_C6807 import cisco_C6807
from netoprmgr.device_templates.cisco.cisco_C6800IA import cisco_C6800IA
from netoprmgr.device_templates.cisco.cisco_C6880 import cisco_C6880
from netoprmgr.device_templates.cisco.cisco_C9200L import cisco_C9200L
from netoprmgr.device_templates.cisco.cisco_C9200 import cisco_C9200
from netoprmgr.device_templates.cisco.cisco_C9300 import cisco_C9300
from netoprmgr.device_templates.cisco.cisco_C9500 import cisco_C9500
from netoprmgr.device_templates.cisco.cisco_2921 import cisco_2921
from netoprmgr.device_templates.cisco.cisco_2951 import cisco_2951
from netoprmgr.device_templates.cisco.cisco_3825 import cisco_3825
from netoprmgr.device_templates.cisco.cisco_3845 import cisco_3845
from netoprmgr.device_templates.cisco.cisco_3925 import cisco_3925
from netoprmgr.device_templates.cisco.cisco_3945 import cisco_3945
from netoprmgr.device_templates.cisco.cisco_ASA5505 import cisco_ASA5505
from netoprmgr.device_templates.cisco.cisco_ASA5508 import cisco_ASA5508
from netoprmgr.device_templates.cisco.cisco_ASA5510 import cisco_ASA5510
from netoprmgr.device_templates.cisco.cisco_FPR4KSM import cisco_FPR4KSM
from netoprmgr.device_templates.cisco.cisco_ASA5512 import cisco_ASA5512
from netoprmgr.device_templates.cisco.cisco_ASA5515 import cisco_ASA5515
from netoprmgr.device_templates.cisco.cisco_ASA5516 import cisco_ASA5516
from netoprmgr.device_templates.cisco.cisco_ASA5520 import cisco_ASA5520
from netoprmgr.device_templates.cisco.cisco_ASA5540 import cisco_ASA5540
from netoprmgr.device_templates.cisco.cisco_ISR4451 import cisco_ISR4451
from netoprmgr.device_templates.cisco.cisco_ISR4331 import cisco_ISR4331
from netoprmgr.device_templates.cisco.cisco_ISR4351 import cisco_ISR4351
from netoprmgr.device_templates.cisco.cisco_ISR4321 import cisco_ISR4321
from netoprmgr.device_templates.cisco.cisco_ISR4221 import cisco_ISR4221
from netoprmgr.device_templates.cisco.cisco_ASA5585 import cisco_ASA5585
from netoprmgr.device_templates.cisco.cisco_ASR9K import cisco_ASR9K
from netoprmgr.device_templates.cisco.cisco_ASR902 import cisco_ASR902
from netoprmgr.device_templates.cisco.cisco_ISR4431 import cisco_ISR4431
from netoprmgr.device_templates.cisco.cisco_N3K_C3172TQ import cisco_N3K_C3172TQ
from netoprmgr.device_templates.cisco.cisco_N3K_C3548P import cisco_N3K_C3548P
from netoprmgr.device_templates.cisco.cisco_N5K_C5596T import cisco_N5K_C5596T
from netoprmgr.device_templates.cisco.cisco_N5K_C5596UP import cisco_N5K_C5596UP
from netoprmgr.device_templates.cisco.cisco_N7K_C7009 import cisco_N7K_C7009
from netoprmgr.device_templates.cisco.cisco_N7K_C7010 import cisco_N7K_C7010
from netoprmgr.device_templates.cisco.cisco_WLC_2504 import cisco_WLC_2504
from netoprmgr.device_templates.cisco.cisco_WLC_3504 import cisco_WLC_3504
from netoprmgr.device_templates.cisco.cisco_WLC_5508 import cisco_WLC_5508
from netoprmgr.device_templates.cisco.cisco_WLC_5520 import cisco_WLC_5520
from netoprmgr.device_templates.cisco.cisco_WLC_9800 import cisco_WLC_9800
from netoprmgr.device_templates.cisco.cisco_WLC_WISM2 import cisco_WLC_WISM2
from netoprmgr.device_templates.cisco.cisco_N9K_C9372PX import cisco_N9K_C9372PX
from netoprmgr.device_templates.cisco.cisco_N9K_C93108TC_EX import cisco_N9K_C93108TC_EX
from netoprmgr.device_templates.cisco.cisco_N9K_C9348GC import cisco_N9K_C9348GC
from netoprmgr.device_templates.cisco.cisco_N9K_C93108TC_FX import cisco_N9K_C93108TC_FX
from netoprmgr.device_templates.cisco.cisco_N9K_C93180YC_EX import cisco_N9K_C93180YC_EX
from netoprmgr.device_templates.cisco.cisco_N9K_C93128TX import cisco_N9K_C93128TX
from netoprmgr.device_templates.cisco.cisco_N9K_C93216TC_FX import cisco_N9K_C93216TC_FX
from netoprmgr.device_templates.cisco.cisco_N9K_C9504 import cisco_N9K_C9504
from netoprmgr.device_templates.cisco.cisco_N9K_C9508 import cisco_N9K_C9508
from netoprmgr.device_templates.cisco.cisco_VG import cisco_VG
from netoprmgr.device_templates.cisco.cisco_N7K_C7710 import cisco_N7K_C7710
from netoprmgr.device_templates.cisco.cisco_N7K_C7004 import cisco_N7K_C7004
from netoprmgr.device_templates.cisco.cisco_AP2802 import cisco_AP2802
from netoprmgr.device_templates.cisco.cisco_AIRBR1310G import cisco_AIRBR1310G
from netoprmgr.device_templates.cisco.cisco_C3550 import cisco_C3550
from netoprmgr.device_templates.cisco.cisco_AP1815 import cisco_AP1815
from netoprmgr.device_templates.cisco.cisco_SAP2602E import cisco_SAP2602E
from netoprmgr.device_templates.cisco.cisco_SG300 import cisco_SG300
from netoprmgr.device_templates.cisco.cisco_AP1832 import cisco_AP1832
from netoprmgr.device_templates.cisco.cisco_AP1852 import cisco_AP1852
from netoprmgr.device_templates.cisco.cisco_CAP702 import cisco_CAP702
from netoprmgr.device_templates.cisco.cisco_CAP1602 import cisco_CAP1602
from netoprmgr.device_templates.cisco.cisco_SAP1602I import cisco_SAP1602I
from netoprmgr.device_templates.cisco.cisco_CAP1702 import cisco_CAP1702
from netoprmgr.device_templates.cisco.cisco_CAP2602 import cisco_CAP2602
from netoprmgr.device_templates.cisco.cisco_CAP2702 import cisco_CAP2702
from netoprmgr.device_templates.cisco.cisco_CAP3602 import cisco_CAP3602
from netoprmgr.device_templates.cisco.cisco_LAP1142 import cisco_LAP1142
from netoprmgr.device_templates.cisco.cisco_LAP1252 import cisco_LAP1252
from netoprmgr.device_templates.cisco.cisco_LAP1261 import cisco_LAP1261
from netoprmgr.device_templates.cisco.cisco_LAP1262 import cisco_LAP1262
from netoprmgr.device_templates.cisco.cisco_APC9120 import cisco_APC9120
from netoprmgr.device_templates.cisco.cisco_APC9105 import cisco_APC9105
from netoprmgr.device_templates.cisco.cisco_APC9115 import cisco_APC9115
from netoprmgr.device_templates.cisco.cisco_N2K_C2348UPQ import cisco_N2K_C2348UPQ
from netoprmgr.device_templates.cisco.cisco_N7K_C7702 import cisco_N7K_C7702
from netoprmgr.device_templates.cisco.cisco_N7K_C7706 import cisco_N7K_C7706
from netoprmgr.device_templates.cisco.cisco_SM_X_ES3D import cisco_SM_X_ES3D
from netoprmgr.device_templates.cisco.cisco_ASA5525 import cisco_ASA5525
from netoprmgr.device_templates.cisco.cisco_N3K_C3064PQ import cisco_N3K_C3064PQ
from netoprmgr.device_templates.cisco.cisco_N3K_C3048TP import cisco_N3K_C3048TP
from netoprmgr.device_templates.cisco.cisco_N3K_C3064TQ import cisco_N3K_C3064TQ
from netoprmgr.device_templates.cisco.cisco_CAP1532 import cisco_CAP1532
from netoprmgr.device_templates.cisco.cisco_C4503 import cisco_C4503
from netoprmgr.device_templates.cisco.cisco_C6832 import cisco_C6832
from netoprmgr.device_templates.cisco.cisco_ASA5555 import cisco_ASA5555
from netoprmgr.device_templates.cisco.cisco_SG220 import cisco_SG220
from netoprmgr.device_templates.cisco.cisco_SG250 import cisco_SG250
from netoprmgr.device_templates.cisco.cisco_CUCM_PUB import cisco_CUCM_PUB
from netoprmgr.device_templates.cisco.cisco_CUCM_SUB import cisco_CUCM_SUB
from netoprmgr.device_templates.cisco.cisco_CUC_PUB import cisco_CUC_PUB
from netoprmgr.device_templates.cisco.cisco_CUC_SUB import cisco_CUC_SUB
from netoprmgr.device_templates.cisco.cisco_IMP_PUB import cisco_IMP_PUB
from netoprmgr.device_templates.cisco.cisco_IMP_SUB import cisco_IMP_SUB
from netoprmgr.device_templates.cisco.cisco_CCX_PUB import cisco_CCX_PUB
from netoprmgr.device_templates.cisco.cisco_CCX_SUB import cisco_CCX_SUB
from netoprmgr.device_templates.cisco.cisco_N540 import cisco_N540
from netoprmgr.device_templates.cisco.cisco_ISRC881 import cisco_ISRC881
from netoprmgr.device_templates.aruba.aruba_5406RZL import aruba_5406RZL
from netoprmgr.device_templates.aruba.aruba_3810M import aruba_3810M
from netoprmgr.device_templates.aruba.aruba_7210 import aruba_7210
from netoprmgr.device_templates.cisco.cisco_ASR1000 import cisco_ASR1000
from netoprmgr.device_templates.aruba.aruba_HPE_5130 import aruba_HPE_5130
from netoprmgr.device_templates.f5.f5_BIG_IP import f5_BIG_IP
from netoprmgr.device_templates.fortigate.fortigate_1000D import fortigate_1000D

class file_identification:
    def __init__(self,file):
        self.file=file

    def file_identification(self):
        #count_file = 1
        #for file in self.files:
        executing_print = ('Beginning Execution')
        try:
            #print('')
            #print('File '+str(count_file)+' of '+str(len(self.files)))
            #print('Processing File :')
            #print(file)
            if '.py' in self.file:
                pass
            elif 'pmdb' in self.file:
                pass
            elif '.zip' in self.file:
                pass
            elif '.png' in self.file:
                pass
            elif 'logcapture.txt' in self.file:
                pass
            elif '.docx' in self.file:
                pass
            elif '.xlsx' in self.file:
                pass
            elif '.pdf' in self.file:
                pass
            else:
                try:
                    read_file = open(self.file, 'r')
                    read_file_list = read_file.readlines()
                except:
                    try:
                        read_file = open(self.file, 'r', encoding='latin-1')
                        read_file_list = read_file.readlines()
                    except:
                        print('Error Codec!!!')
                        pass
                xcek='disable'
                for i in read_file_list:
                    if re.findall('.*PID:.*C3750X',i):
                        print('Execute with C3750X')
                        cisco_C3750X(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C1000\S+',i):
                        print('Execute with C1000')
                        cisco_C1000X(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*DESCR:.*C4500X',i):
                        print('Execute with C4500X')
                        cisco_C4500X(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C4503\S+',i):
                        print('Execute with C4503')
                        cisco_C4503(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C4506',i):
                        print('Execute with C4506')
                        cisco_C4506(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^PID:\s+\S+4507R[+]E',i):
                        print('Execute with C4507RE')
                        cisco_C4507RE(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C4507R',i):
                        print('Execute with C4507R')
                        cisco_C4507R(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*DESCR:.*C4900M',i):
                        print('Execute with C4900M')
                        cisco_C4900M(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*DESCR:.*C4948',i):
                        print('Execute with C4948')
                        cisco_C4948(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C6504',i):
                        print('Execute with C6504')
                        cisco_C6504(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C6506',i):
                        print('Execute with C6506')
                        cisco_C6506(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+2960CX',i):
                        print ('Executing with C2960CX')
                        cisco_C2960CX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+2960C',i):
                        print ('Executing with C2960C')
                        cisco_C2960C(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+2960L',i):
                        print ('Executing with C2960L')
                        cisco_C2960L(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+2960S',i):
                        print ('Executing with C2960S')
                        cisco_C2960S(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+2960XR',i):
                        print ('Executing with C2960XR')
                        cisco_C2960XR(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID: WS-C2960X-\S*',i):
                        print ('Executing with C2960X')
                        cisco_C2960X(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+2960',i):
                        print ('Executing with C2960')
                        cisco_C2960(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3560CG',i):
                        print ('Executing with C3560CG')
                        cisco_C3560CG(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3560CX',i):
                        print ('Executing with C3560CX')
                        cisco_C3560CX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3560C',i):
                        print ('Executing with C3560C')
                        cisco_C3560C(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3560G',i):
                        print ('Executing with C3560G')
                        cisco_C3560G(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3560V2',i):
                        print ('Executing with C3560V2')
                        cisco_C3560V2(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3560X',i):
                        print ('Executing with C3560X')
                        cisco_C3560X(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3560',i):
                        print ('Executing with C3560')
                        cisco_C3560(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+C3650\S+-[EL]',i):
                        print ('Executing with C3650-E')
                        cisco_C3650_E(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+C3650',i):
                        print ('Executing with C3650')
                        cisco_C3650(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3750E',i):
                        print ('Executing with C3750E')
                        cisco_C3750E(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3750G',i):
                        print ('Executing with C3750G')
                        cisco_C3750G(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3750V2',i):
                        print ('Executing with C3750V2')
                        cisco_C3750V2(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+\S+3850',i):
                        print ('Executing with C3850')
                        cisco_C3850(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C6509',i):
                        print('Executing with C6509')
                        cisco_C6509(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C6513',i):
                        print('Executing with C6513')
                        cisco_C6513(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+C6800IA',i):
                        print('Executing with C6800IA')
                        cisco_C6800IA(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C6807',i):
                        print('Executing with C6807')
                        cisco_C6807(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C6832',i):
                        print('Executing with C6832')
                        cisco_C6832(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C6880',i):
                        print('Executing with C6880')
                        cisco_C6880(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C9200L',i):
                        print('Executing with C9200L')
                        cisco_C9200L(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C9200',i):
                        print('Executing with C9200')
                        cisco_C9200(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C9300',i):
                        print('Executing with C9300')
                        cisco_C9300(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C9500',i):
                        print('Executing with C9500')
                        cisco_C9500(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*CISCO2921',i):
                        print('Executing with CISCO2921')
                        cisco_2921(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*CISCO2951',i):
                        print('Executing with CISCO2951')
                        cisco_2951(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*CISCO3825',i):
                        print('Executing with CISCO3825')
                        cisco_3825(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*CISCO3845',i):
                        print('Executing with CISCO3845')
                        cisco_3845(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*CISCO3925',i):
                        print('Executing with CISCO3925')
                        cisco_3925(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*CISCO3945',i):
                        print('Executing with CISCO3945')
                        cisco_3945(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5505',i):
                        print('Executing with ASA5505')
                        cisco_ASA5505(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5508',i):
                        print('Executing with ASA5508')
                        cisco_ASA5508(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5510',i):
                        print('Executing with ASA5510')
                        cisco_ASA5510(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5555',i):
                        print('Executing with ASA5555')
                        cisco_ASA5555(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*FPR4K-SM.*',i):
                        print('Executing with FPR4KSM')
                        cisco_FPR4KSM(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO1905',i):
                        print('Executing with 1905')
                        cisco_1905(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO1921',i):
                        print('Executing with 1921')
                        cisco_1921(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO1941',i):
                        print('Executing with 1941')
                        cisco_1941(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO2801',i):
                        print('Executing with 2801')
                        cisco_2801(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO2811',i):
                        print('Executing with 2811')
                        cisco_2811(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO2821',i):
                        print('Executing with 2821')
                        cisco_2821(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO2901',i):
                        print('Executing with 2901')
                        cisco_2901(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+CISCO2911',i):
                        print('Executing with 2911')
                        cisco_2911(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5512',i):
                        print('Executing with ASA5512')
                        cisco_ASA5512(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5515',i):
                        print('Executing with ASA5515')
                        cisco_ASA5515(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5516',i):
                        print('Executing with ASA5516')
                        cisco_ASA5516(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5520',i):
                        print('Executing with ASA5520')
                        cisco_ASA5520(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5540',i):
                        print('Executing with ASA5540')
                        cisco_ASA5540(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5525',i):
                        print('Executing with ASA5525')
                        cisco_ASA5525(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ISR4451',i):
                        print('Executing with ISR4451')
                        cisco_ISR4451(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ISR4331',i):
                        print('Executing with ISR4331')
                        cisco_ISR4331(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ISR4351',i):
                        print('Executing with ISR4351')
                        cisco_ISR4351(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ISR4321',i):
                        print('Executing with ISR4321')
                        cisco_ISR4321(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ISR4221',i):
                        print('Executing with ISR4221')
                        cisco_ISR4221(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C881-V-K9',i):
                        print('Executing with ISRC881')
                        cisco_ISRC881(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASR100\d',i):
                        print('Executing with ASR1000')
                        cisco_ASR1000(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASA5585',i):
                        print('Executing with ASA5585')
                        cisco_ASA5585(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^PID:\s+A[9S][RK]-\S+-',i):
                        print('Executing with ASR9K')
                        cisco_ASR9K(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ASR-902',i):
                        print('Executing with ASR902')
                        cisco_ASR902(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*ISR4431\S+',i):
                        print('Executing with ISR4431')
                        cisco_ISR4431(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*Extender\s+Model:\s+N2K-C2348\S+',i):
                        print('Executing with N2K-C2348UPQ')
                        cisco_N2K_C2348UPQ(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^.Product\sNumber\s+:+\s+(N3K-C3048TP-\S+)',i):
                        print('Executing with N3K-C3048TP')
                        cisco_N3K_C3048TP(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^.Product\sNumber\s+:+\s+(N3K-C3064PQ-\S+)',i):
                        print('Executing with N3K-C3064PQ')
                        cisco_N3K_C3064PQ(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^.Product\sNumber\s+:+\s+(N3K-C3064TQ-\S+)',i):
                        print('Executing with N3K-C3064TQ')
                        cisco_N3K_C3064TQ(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N3K-C3172TQ-\S+',i):
                        print('Executing with N3K-C3172TQ')
                        cisco_N3K_C3172TQ(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N3K-C3548P-\S+',i):
                        print('Executing with N3K-C3548P')
                        cisco_N3K_C3548P(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N5K-C5596T',i):
                        print('Executing with N5K-C5596T')
                        cisco_N5K_C5596T(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N5K-C5596UP',i):
                        print('Executing with N5K-C5596UP')
                        cisco_N5K_C5596UP(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C93180YC-EX',i):
                        print('Executing with N9K-C93180YC-EX')
                        cisco_N9K_C93180YC_EX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C93180YC-FX-24',i):
                        print('Executing with N9K-C93180YC-EX')
                        cisco_N9K_C93180YC_EX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C93108TC-EX',i):
                        print('Executing with N9K-C93108TC-EX')
                        cisco_N9K_C93108TC_EX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C93108TC-FX',i):
                        print('Executing with N9K-C93108TC-FX')
                        cisco_N9K_C93108TC_FX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C93216TC-FX',i):
                        print('Executing with N9K-C93216TC-FX')
                        cisco_N9K_C93216TC_FX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C9348GC',i):
                        print('Executing with N9K-C9348GC')
                        cisco_N9K_C9348GC(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C9332C',i):
                        print('Executing with N9K-C9332C')
                        cisco_N9K_C9348GC(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C93128TX',i):
                        print('Executing with N9K_C93128TX')
                        cisco_N9K_C93128TX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('PID:\s+N9K-C9372PX',i):
                        print('Executing with N9K-C9372PX')
                        cisco_N9K_C9372PX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+N9K-C9504',i):
                        print('Executing with N9K-C9504')
                        cisco_N9K_C9504(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+N9K-C9508',i):
                        print('Executing with N9K-C9508')
                        cisco_N9K_C9508(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+N9K-C93180YC-FX',i):
                        print('Executing with N9K-C93180YC-FX')
                        cisco_N9K_C93180YC_EX(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*N7K-C7004',i):
                        print('Executing with N7K-C7004')
                        cisco_N7K_C7004(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*N7K-C7009',i):
                        print('Executing with N7K-C7009')
                        cisco_N7K_C7009(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*N77-C7702',i):
                        print('Executing with N77-C7702')
                        cisco_N7K_C7702(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*N77-C7706',i):
                        print('Executing with N77-C7706')
                        cisco_N7K_C7706(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*N7K-C7010',i):
                        print('Executing with N7K-C7010')
                        cisco_N7K_C7010(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*N77-C7710',i):
                        print('Executing with N7K-C7710')
                        cisco_N7K_C7710(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CT2504',i):
                        print('Executing with AIR-CT2504')
                        cisco_WLC_2504(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CT3504',i):
                        print('Executing with AIR-CT3504')
                        cisco_WLC_3504(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CT5508',i):
                        print('Executing with AIR-CT5508')
                        cisco_WLC_5508(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CT5520',i):
                        print('Executing with AIR-CT5520')
                        cisco_WLC_5520(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+C9800-CL\S+',i):
                        print('Executing with WLC-C9800')
                        cisco_WLC_9800(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*WS\S+',i):
                        print('Executing with WS-SVC-WISM2')
                        cisco_WLC_WISM2(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*VG\S+',i):
                        print('Executing with Router VG')
                        cisco_VG(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-AP1815\S+',i):
                        print('Executing with AIR-AP1815')
                        cisco_AP1815(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-AP1832\S+',i):
                        print('Executing with AIR-AP1832')
                        cisco_AP1832(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-AP1852\S+',i):
                        print('Executing with AIR-AP1852')
                        cisco_AP1852(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-AP2802\S+',i):
                        print('Executing with AIR-AP2802')
                        cisco_AP2802(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-BR1310G\S+',i):
                        print('Executing with AIR-BR1310G')
                        cisco_AIRBR1310G(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-SAP1602\S+',i):
                        print('Executing with AIR-SAP1602I')
                        cisco_SAP1602I(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*Product.*Number\s+:\s+AIR-SAP2602E\S+',i):
                        print('Executing with AIR-SAP2602E')
                        cisco_SAP2602E(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CAP702\S+',i):
                        print('Executing with AIR-CAP702')
                        cisco_CAP702(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CAP1532\S+',i):
                        print('Executing with AIR-CAP1532')
                        cisco_CAP1532(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CAP1602\S+',i):
                        print('Executing with AIR-CAP1602')
                        cisco_CAP1602(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CAP1702\S+',i):
                        print('Executing with AIR-CAP1702')
                        cisco_CAP1702(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CAP2602\S+',i):
                        print('Executing with AIR-CAP2602')
                        cisco_CAP2602(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CAP2702\S+',i):
                        print('Executing with AIR-CAP2702')
                        cisco_CAP2702(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-CAP3602\S+',i):
                        print('Executing with AIR-CAP3602')
                        cisco_CAP3602(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-.*AP1142\S+',i):
                        print('Executing with AIR-LAP1142')
                        cisco_LAP1142(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-LAP1252\S+',i):
                        print('Executing with AIR-LAP1252')
                        cisco_LAP1252(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-LAP1261\S+',i):
                        print('Executing with AIR-LAP1261')
                        cisco_LAP1261(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*AIR-LAP1262\S+',i):
                        print('Executing with AIR-LAP1262')
                        cisco_LAP1262(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C9120AX\S+',i):
                        print('Executing with APC9120AX')
                        cisco_APC9120(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:.*C9105AX\S+',i):
                        print('Executing with APC9105AX')
                        cisco_APC9105(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+C9115AX\S+',i):
                        print('Executing with APC9115AX')
                        cisco_APC9115(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*Model\s+number:\s+WS-C3550\S*',i):
                        print('Executing with C3550')
                        cisco_C3550(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^Model\s+Number\s+:\s+SG220\S+',i):
                        print('Executing with SG220')
                        cisco_SG220(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^.*\d+\s+SG250\S+',i):
                        print('Executing with SG250')
                        cisco_SG250(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*System\s+Name:\s+SG\d+',i):
                        print('Executing with SG300')
                        cisco_SG300(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+SM-X-ES3\S+',i):
                        print('Executing with SM-X-ES3D')
                        cisco_SM_X_ES3D(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*PID:\s+N540\S+',i):
                        print('Executing with N540')
                        cisco_N540(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_cucm_pub$',i):
                        print('Executing with CUCM_PUB')
                        cisco_CUCM_PUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_cucm_sub$',i):
                        print('Executing with CUCM_SUB')
                        cisco_CUCM_SUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_cuc_pub$',i):
                        print('Executing with CUC_PUB')
                        cisco_CUC_PUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_cuc_sub$',i):
                        print('Executing with CUC_SUB')
                        cisco_CUC_SUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_imp_pub$',i):
                        print('Executing with IMP_PUB')
                        cisco_IMP_PUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_imp_sub$',i):
                        print('Executing with IMP_SUB')
                        cisco_IMP_SUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_ccx_pub$',i):
                        print('Executing with CCX_PUB')
                        cisco_CCX_PUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^ucm_ccx_sub$',i):
                        print('Executing with CCX_SUB')
                        cisco_CCX_SUB(self.file)
                        xcek='disable'
                        break
                    elif re.findall('Model\s+:\s+.*5406\S+',i):
                        print('Executing with aruba_5406Rzl2')
                        aruba_5406RZL(self.file)
                        xcek='disable'
                        break
                    elif re.findall('Model\s+:\s+(\S+\s+\S+\s+3810M.*Switch)\s+',i):
                        print('Executing with aruba_3810M')
                        aruba_3810M(self.file)
                        xcek='disable'
                        break
                    elif re.findall('^HPE\s+5130.*',i):
                        print('Executing with aruba_HPE_5130')
                        aruba_HPE_5130(self.file)
                        xcek='disable'
                        break
                    elif re.findall('\S+\s+.MODEL:\s+(Aruba\S+).,',i):
                        print('Executing with aruba_7210')
                        aruba_7210(self.file)
                        xcek='disable'
                        break
                    elif re.findall('Product\s+(BIG-IP)',i):
                        print('Executing with BIG-IP F5')
                        f5_BIG_IP(self.file)
                        xcek='disable'
                        break
                    elif re.findall('.*(FortiGate-1000D)',i):
                        print('Executing with Fortigate 1000D')
                        fortigate_1000D(self.file)
                        xcek='disable'
                        break
                    else:
                        xcek='enable'
                    
                if xcek=='enable':
                    executing_print=('Executing None')
                    cisco_None(self.file)
                
        # except NameError:
        #    raise
        except:
            pass
            #count_file+=1
        return executing_print