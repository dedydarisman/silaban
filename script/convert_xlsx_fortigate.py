import sqlite3
import re
import pkg_resources
import os
import xlsxwriter

class convert_xlsx_fortigate:
    @staticmethod
    def convert_xlsx_fortigate():
        result_path = pkg_resources.resource_filename('netoprmgr', 'static/')
        result_path = os.path.join(result_path,'result/')

        print('')
        print('Processing Document')

        #open db connection
        db = sqlite3.connect('pmdb_fortigate')
        cursor = db.cursor()

        #System Analysis of FortiGate
        #sql query
        cursor.execute('''SELECT devicename, system, system_value FROM systematable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #define workbook
        # print(records)
        # input()
        workbook = xlsxwriter.Workbook('fortigate.xlsx')

        #format title
        format1 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 12,
                                'bold' : 1,
                                'text_wrap': True
                                })
        #format header table
        format2 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format number header
        format3 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'bold' : 1,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'fg_color' : '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #format data table
        format4 = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 10,
                                'border': 1,
                                'align' : 'center',
                                'valign' : 'vcenter',
                                'text_wrap': True
                                })
        #format for merge
        merge_format = workbook.add_format({
                                'font_name' : 'Arial',
                                'font_size' : 11,
                                'bold': 1,
                                'border': 1,
                                'align': 'center',
                                'valign': 'vcenter',
                                'fg_color': '#00659C',
                                'font_color': 'white',
                                'text_wrap': True
                                })
        #create sheet
        worksheet = workbook.add_worksheet('System Analysis of FortiGate')
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'System Analysis of FortiGate',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'System Name',format2)
        worksheet.write(1,2,'Detail System',format2)
        
        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 2
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                if 'Not Announced' in row[2]:
                    worksheet.write(count_row,2,'Isi Manual',format4)
                else:
                    worksheet.write(count_row,2,row[2],format4)
               
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1

        
        #HA TABLE
        #sql query
        cursor.execute('''SELECT devicename, ha, ha_value FROM hatable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('High Availability Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'High Availability Table',format1)
        worksheet.write(1,0,'Device Name',format2)
        worksheet.write(1,1,'HA Name',format2)
        worksheet.write(1,2,'Detail HA',format2)
        
        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 2
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
               
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1



        #CPU Utilization Table
        #sql query
        cursor.execute('''SELECT devicename, cpu_state, cpu_user, cpu_system, cpu_nice, cpu_idle, cpu_iowait, cpu_irq, cpu_softirq, cpu_status FROM cputable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('CPU Utilization Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'CPU Utilization Table',format1)
        worksheet.write(1,0,'Hostname',format2)
        worksheet.write(1,1,'CPU States',format2)
        worksheet.write(1,2,'User',format2)
        worksheet.write(1,3,'System',format2)
        worksheet.write(1,4,'Nice',format2)
        worksheet.write(1,5,'Idle',format2)
        worksheet.write(1,6,'Iowait',format2)
        worksheet.write(1,7,'Irq',format2)
        worksheet.write(1,8,'Softirq',format2)
        worksheet.write(1,9,'Status',format2)

        worksheet.merge_range(0, 11, 0, 12, 'For Chart',format1)
        worksheet.write(1,11,'Hostname',format4)
        worksheet.write(1,12,'100-Idle',format4)
        
        
        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 2
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2]+'%',format4)
                worksheet.write(count_row,3,row[3]+'%',format4)
                worksheet.write(count_row,4,row[4]+'%',format4)
                worksheet.write(count_row,5,row[5]+'%',format4)
                worksheet.write(count_row,6,row[6]+'%',format4)
                worksheet.write(count_row,7,row[7]+'%',format4)
                worksheet.write(count_row,8,row[8]+'%',format4)
                worksheet.write(count_row,9,row[9],format4)
                worksheet.write(count_row,11,row[0]+'-'+row[1],format4)
                worksheet.write(count_row,12,100-float(row[5]),format4)
                
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2]+'%',format4)
                worksheet.write(count_row,3,row[3]+'%',format4)
                worksheet.write(count_row,4,row[4]+'%',format4)
                worksheet.write(count_row,5,row[5]+'%',format4)
                worksheet.write(count_row,6,row[6]+'%',format4)
                worksheet.write(count_row,7,row[7]+'%',format4)
                worksheet.write(count_row,8,row[8]+'%',format4)
                worksheet.write(count_row,9,row[9],format4)
                worksheet.write(count_row,11,row[0]+'-'+row[1],format4)
                worksheet.write(count_row,12,100-float(row[5]),format4)
               
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1

        cpu_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            cpu_chart.add_series(
                {
                    "name" : "='CPU Utilization Table'!L"+str(i),
                    "values": "='CPU Utilization Table'!M"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('O2', cpu_chart)
    

    #Memory Utilization Table
        #sql query
        cursor.execute('''SELECT devicename, memory_used, memory_free, memory_freeable, memory_status FROM memtable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Memory Utilization Table')
        
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Memory Utilization Table',format1)
        worksheet.write(1,0,'Hostname',format2)
        worksheet.write(1,1,'Used',format2)
        worksheet.write(1,2,'Free',format2)
        worksheet.write(1,3,'Freeable',format2)
        worksheet.write(1,4,'Status',format2)
        

        worksheet.merge_range(0, 6, 0, 7, 'For Chart',format1)
        worksheet.write(1,6,'Hostname',format4)
        worksheet.write(1,7,'Used',format4)
        
        
        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 2
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2]+'%',format4)
                worksheet.write(count_row,3,row[3]+'%',format4)
                worksheet.write(count_row,4,row[4],format4)
                
                worksheet.write(count_row,6,row[0],format4)
                worksheet.write(count_row,7,float(row[1]),format4)
                
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2]+'%',format4)
                worksheet.write(count_row,3,row[3]+'%',format4)
                worksheet.write(count_row,4,row[4],format4)
               
                worksheet.write(count_row,6,row[0],format4)
                worksheet.write(count_row,7,float(row[1]),format4)
               
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1

        cpu_chart = workbook.add_chart({'type': 'column'})
        for i in range(3,count_row+1):
            cpu_chart.add_series(
                {
                    "name" : "='Memory Utilization Table'!G"+str(i),
                    "values": "='Memory Utilization Table'!H"+str(i),
                    "data_labels": {"value": True},
                    }
                )
        worksheet.insert_chart('J2', cpu_chart)
        
        #Session Utilization Table
        #sql query
        cursor.execute('''SELECT devicename, session_type, session_1, session_10, session_30 FROM sessiontable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Session Utilization Table')
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Session Utilization Table',format1)
        worksheet.write(1,0,'Hostname',format2)
        worksheet.write(1,1,'Session Type',format2)
        worksheet.write(1,2,'1 min',format2)
        worksheet.write(1,3,'10 min',format2)
        worksheet.write(1,4,'30 min',format2)
        
        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 2
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[3],format4)
                worksheet.write(count_row,4,row[4],format4)
                
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[3],format4)
                worksheet.write(count_row,4,row[4],format4)
               
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1
        
        
        #License and Certificate Analysis of FortiGate
        #sql query
        cursor.execute('''SELECT devicename, lic_entitlement, lic_contract_exp, lic_last_update_schedule, lic_last_update_attempt, lic_result, lic_status FROM lictable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('License and Certificate Table')
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'License and Certificate Table',format1)
        worksheet.write(1,0,'Hostname',format2)
        worksheet.write(1,1,'Entitlement',format2)
        worksheet.write(1,2,'Contract Expiry Date',format2)
        worksheet.write(1,3,'Last Update Using Schedule Update',format2)
        worksheet.write(1,4,'Last Update Attempt',format2)
        worksheet.write(1,5,'Result',format2)
        worksheet.write(1,6,'Status',format2)
        
        #data section
        row_check = 'ludesdeveloper'
        row_version = 'ludesdeveloper'
        iteration_check = False
        count_row = 2
        for row in records:
            if row_check not in row[0] and iteration_check == False:

                row_check = row[0]
                row_version = row[1]
                iteration_check = False
                start_row = count_row

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[3],format4)
                worksheet.write(count_row,4,row[4],format4)
                worksheet.write(count_row,5,row[5],format4)
                worksheet.write(count_row,6,row[6],format4)
                
                iteration_check == True
                count_row+=1

            elif row_check in row[0] and row_version not in row[1] and iteration_check == False:
                row_check = row[0]
                row_version = row[1]

                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[3],format4)
                worksheet.write(count_row,4,row[4],format4)
                worksheet.write(count_row,5,row[5],format4)
                worksheet.write(count_row,6,row[6],format4)
                
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
                iteration_check == True
                count_row+=1

        #Certificate Table
        #sql query
        cursor.execute('''SELECT devicename, cert_name, cert_subject, cert_issuer, cert_validto, cert_status FROM certtable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #create sheet
        worksheet = workbook.add_worksheet('Certificate Table')
        #header section
        worksheet.merge_range(0, 0, 0, 4, 'Certificate Table',format1)
        worksheet.write(1,0,'Hostname',format2)
        worksheet.write(1,1,'Certificate Name',format2)
        worksheet.write(1,2,'Subject',format2)
        worksheet.write(1,3,'Issuer',format2)
        worksheet.write(1,4,'Valid To',format2)
        worksheet.write(1,5,'Status',format2)

        
        #data section
        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 2
        for row in records:
            if row_check not in row[0]:
                row_check = row[0]
                iteration_check = False
                start_row = count_row
            else:
                pass
            if row_check == row[0] and iteration_check == False:
                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[3],format4)
                worksheet.write(count_row,4,row[4],format4)
                worksheet.write(count_row,5,row[5],format4)
                iteration_check = True
            else:
                worksheet.write(count_row,0,row[0],format4)
                worksheet.write(count_row,1,row[1],format4)
                worksheet.write(count_row,2,row[2],format4)
                worksheet.write(count_row,3,row[3],format4)
                worksheet.write(count_row,4,row[4],format4)
                worksheet.write(count_row,5,row[5],format4)
                iteration_check = True
                end_row = count_row
                worksheet.merge_range(start_row, 0, end_row, 0, row[0], format4)
            count_row+=1

        #CLOSE WORKBOOK
        workbook.close()

        #close database
        db.close()
        
        #save document
        print('')
        print('Saving Document')
        print('Document has been saved to fortigate.xlsx')
