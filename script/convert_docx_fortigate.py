from docx import Document
from docx.oxml.shared import OxmlElement, qn
import sqlite3
import re
from docx.shared import Pt
import pkg_resources
import os
import subprocess
from sys import platform
from netoprmgr.script.convert_docx_create import convert_docx_create
from docx.enum.text import WD_LINE_SPACING
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.table import WD_ROW_HEIGHT
from docx.enum.table import WD_ALIGN_VERTICAL
from docx.shared import Inches
from docx.shared import Pt
from docx.shared import Cm
from docx.shared import RGBColor

data_path = pkg_resources.resource_filename('netoprmgr', 'static/')
data_path = os.path.join(data_path,'data/')
capture_path = pkg_resources.resource_filename('netoprmgr', 'static/')
capture_path = os.path.join(capture_path,'capture/')
main_path = pkg_resources.resource_filename('netoprmgr', '/')

docx_create = convert_docx_create()
def func_get_req(address):
    try:
        os.chdir(main_path)
        files = ['init', 'init.exe']
        for file in files:
            os.chmod(file, 0o0777)
        address = re.findall(".*(qr.*)", address)
        address = address[0]
        if platform == "win32":
            cmd = ("init ")
            qr = subprocess.check_output(cmd+address, shell=True)
        else:
            cmd = ("./init ")
            qr = subprocess.check_output(cmd+address, shell=True)

        qr = str(qr)
        qr = re.findall("b\W(.*)\Wn\W", qr)
        qr = qr[0]
        return qr
    except:
        return

def shade_cells(cells, shade):
    for cell in cells:
        tcPr = cell._tc.get_or_add_tcPr()
        tcVAlign = OxmlElement("w:shd")
        tcVAlign.set(qn("w:fill"), shade)
        tcPr.append(tcVAlign)

def set_repeat_table_header(row):
    tr = row._tr
    trPr = tr.get_or_add_trPr()
    qr45 = func_get_req(docx_create+"qr45")
    tblHeader = OxmlElement(qr45)
    tblHeader.set(qn('w:val'), "true")
    trPr.append(tblHeader)
    return row

class convert_docx_fortigate:
    #@staticmethod
    def __init__(self, customer, review, prepared, pic, bulan, tahun):
        #variable constructor
        self.customer = customer
        self.review = review
        self.prepared = prepared
        self.pic = pic
        self.bulan = bulan
        self.tahun = tahun

    def convert_docx_fortigate(self):
        print('')
        print('Processing Document')

        #open db connection
        db = sqlite3.connect('pmdb_fortigate')
        cursor = db.cursor()

        #using document docx module
        report_template = open(data_path+'template_content.docx', 'rb')
        document = Document(report_template)

        red = RGBColor(255, 0, 0)
        
        #SYSTEM ANALYSIS
        p = document.add_paragraph('System Analysis of FortiGate')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('System Analysis Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #SYSTEM ANALYSIS TABLE
        #sql query
        cursor.execute('''SELECT devicename, system, system_value FROM systematable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=3)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'System Name'
        hdr_cells[2].text = 'Detail System'   

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(4.94)
        hdr_cells[2].width = Cm(8)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(4.94)
            row_cells[2].width = Cm(8)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1]) 

            if 'Not Announced' in row[2]:
                row_cells[2].text = '-'
                run1 = row_cells[2].paragraphs[0].runs[0]
                run1.font.color.rgb = red
            else:
                row_cells[2].text = (row[2]) 

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #SYSTEM ANALYSIS DETAILS   
        p = document.add_paragraph('Version')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('Software which running on the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Serial Number')
        p.style = document.styles['Title']
        p = document.add_paragraph('Serial number of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('BIOS Version')
        p.style = document.styles['Title']
        p = document.add_paragraph('BIOS version of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Log hard disk')
        p.style = document.styles['Title']
        p = document.add_paragraph('Physical disk status of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Hostname ')
        p.style = document.styles['Title']
        p = document.add_paragraph('Device hostname.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Operation Mode')
        p.style = document.styles['Title']
        p = document.add_paragraph('Operation mode of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Current virtual domain')
        p.style = document.styles['Title']
        p = document.add_paragraph('Current virtual domain when the captured was taken.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Max number of virtual domains')
        p.style = document.styles['Title']
        p = document.add_paragraph('Maximum number of virtual domain supported in this FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Override')
        p.style = document.styles['Title']
        p = document.add_paragraph('The status of the override option for the current cluster unit.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Virtual domain Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Virtual domain status of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Virtual domain configuration')
        p.style = document.styles['Title']
        p = document.add_paragraph('Virtual domain configuration status of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('System Time')
        p.style = document.styles['Title']
        p = document.add_paragraph('Time of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Uptime')
        p.style = document.styles['Title']
        p = document.add_paragraph('Running time the devices from last reboot.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Hardware End of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date to receive service and support for the hardware.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Software End of Support')
        p.style = document.styles['Title']
        p = document.add_paragraph('The last date to receive service and support for the software.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Latest Software Version')
        p.style = document.styles['Title']
        p = document.add_paragraph('Latest Patch on the major version.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('System Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<System Analysis Summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #HIGH AVAILABILITY
        p = document.add_paragraph('High Availability Table')
        p.style = document.styles['Heading 2']

        #HIGH AVAILABILITY TABLE
        #sql query
        cursor.execute('''SELECT devicename, ha, ha_value FROM hatable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=3)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Device Name'
        hdr_cells[1].text = 'HA Name'
        hdr_cells[2].text = 'Detail HA'   

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(4.94)
        hdr_cells[2].width = Cm(8)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(4.25)
            row_cells[1].width = Cm(4.94)
            row_cells[2].width = Cm(8)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1]) 
            row_cells[2].text = (row[2])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1
        
        #HIGH AVAILABILITY DETAILS   
        p = document.add_paragraph('HA Health Status')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('High Availability health status of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Model')
        p.style = document.styles['Title']
        p = document.add_paragraph('The FortiGate model number.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Mode')
        p.style = document.styles['Title']
        p = document.add_paragraph('The HA mode of the cluster, for example, HA A-P or HA A-A.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Group')
        p.style = document.styles['Title']
        p = document.add_paragraph('The group ID of the cluster.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Debug')
        p.style = document.styles['Title']
        p = document.add_paragraph('The debug status of the cluster.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Cluster Uptime')
        p.style = document.styles['Title']
        p = document.add_paragraph('The running time that the cluster has been operating.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Cluster State Change Time')
        p.style = document.styles['Title']
        p = document.add_paragraph('The date and time at which the FortiGate most recently changed state.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Ses_Pickup')
        p.style = document.styles['Title']
        p = document.add_paragraph('The status of session pickup: enable or disable.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Override')
        p.style = document.styles['Title']
        p = document.add_paragraph('The status of the override option for the current cluster unit.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Primary')
        p.style = document.styles['Title']
        p = document.add_paragraph('The Primary device of the cluster.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Secondary')
        p.style = document.styles['Title']
        p = document.add_paragraph('The secondary device of the cluster.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Number of vcluster')
        p.style = document.styles['Title']
        p = document.add_paragraph('The number of virtual clusters.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('High Availability Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<High Availability Analysis Summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #SYSTEM PERFORMANCE ANALYSIS
        p = document.add_paragraph('System Performance Analysis of FortiGate')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('CPU Utilization Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_hardware = (capture_path+'/CPU Summary FortiGate.png')
        document.add_picture(img_hardware, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        p = document.add_paragraph('CPU Utilization Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #sql query
        cursor.execute('''SELECT devicename, cpu_state, cpu_user, cpu_system, cpu_nice, cpu_idle, cpu_iowait, cpu_irq, cpu_softirq, cpu_status FROM cputable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=10)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Hostname'
        hdr_cells[1].text = 'CPU States'
        hdr_cells[2].text = 'User'
        hdr_cells[3].text = 'System'
        hdr_cells[4].text = 'Nice'
        hdr_cells[5].text = 'Idle'
        hdr_cells[6].text = 'Iowait'
        hdr_cells[7].text = 'Irq'
        hdr_cells[8].text = 'Softirq'
        hdr_cells[9].text = 'Status'

        hdr_cells[0].width = Cm(3)
        hdr_cells[1].width = Cm(2.25)
        hdr_cells[2].width = Cm(1.51)
        hdr_cells[3].width = Cm(1.51)
        hdr_cells[4].width = Cm(1.51)
        hdr_cells[5].width = Cm(1.51)
        hdr_cells[6].width = Cm(1.51)
        hdr_cells[7].width = Cm(1.51)
        hdr_cells[8].width = Cm(1.51)
        hdr_cells[9].width = Cm(1.77)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(3)
            row_cells[1].width = Cm(2.25)
            row_cells[2].width = Cm(1.51)
            row_cells[3].width = Cm(1.51)
            row_cells[4].width = Cm(1.51)
            row_cells[5].width = Cm(1.51)
            row_cells[6].width = Cm(1.51)
            row_cells[7].width = Cm(1.51)
            row_cells[8].width = Cm(1.51)
            row_cells[9].width = Cm(1.77)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1]) 
            row_cells[2].text = (row[2]+'%')
            row_cells[3].text = (row[3]+'%') 
            row_cells[4].text = (row[4]+'%')
            row_cells[5].text = (row[5]+'%') 
            row_cells[6].text = (row[6]+'%')
            row_cells[7].text = (row[7]+'%') 
            row_cells[8].text = (row[8]+'%')
            row_cells[9].text = (row[9]) 

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #CPU UTILIZASTION DETAILS
        p = document.add_paragraph('Hostname')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('Device hostname.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('CPU States')
        p.style = document.styles['Title']
        p = document.add_paragraph('CPU States of the FortiGate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('User')
        p.style = document.styles['Title']
        p = document.add_paragraph('% CPU usage for user space application.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('System')
        p.style = document.styles['Title']
        p = document.add_paragraph('% CPU usage for system processes (kernel).')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Nice')
        p.style = document.styles['Title']
        p = document.add_paragraph('% CPU usage for niced user processes')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Idle')
        p.style = document.styles['Title']
        p = document.add_paragraph('% CPU usage of idle time.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Iowait')
        p.style = document.styles['Title']
        p = document.add_paragraph('% CPU usage for waiting for I/O completion.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Irq')
        p.style = document.styles['Title']
        p = document.add_paragraph('% CPU usage for hardware interrupts.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Softirq')
        p.style = document.styles['Title']
        p = document.add_paragraph('% CPU usage for software interrupts.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('CPU utilization status. CPU utilization levels:')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-25%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 26-75%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 75-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('CPU Utilization Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<CPU Utilization Analysis Summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        p = document.add_paragraph('Memory Utilization Summary')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        chg_dir = os.chdir(capture_path)
        current_dir=os.getcwd()
        img_hardware = (capture_path+'/Memory Summary FortiGate.png')
        document.add_picture(img_hardware, width=Cm(15))
        last_paragraph = document.paragraphs[-1] 
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        p = document.add_paragraph('Memory Utilization Table')
        p.style = document.styles['Heading 2']

        #sql query
        cursor.execute('''SELECT devicename, memory_used, memory_free, memory_freeable, memory_status FROM memtable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Hostname'
        hdr_cells[1].text = 'Used'
        hdr_cells[2].text = 'Free'
        hdr_cells[3].text = 'Freeable'
        hdr_cells[4].text = 'Status'

        hdr_cells[0].width = Cm(4.25)
        hdr_cells[1].width = Cm(2.25)
        hdr_cells[2].width = Cm(2.25)
        hdr_cells[3].width = Cm(2.25)
        hdr_cells[4].width = Cm(2.25)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(3)
            row_cells[1].width = Cm(2.25)
            row_cells[2].width = Cm(2.25)
            row_cells[3].width = Cm(2.25)
            row_cells[4].width = Cm(2.25)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1]+'%') 
            row_cells[2].text = (row[2]+'%')
            row_cells[3].text = (row[3]+'%') 
            row_cells[4].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP

            count_row+=1
        
        #MEMORY UTILIZASTION DETAILS
        p = document.add_paragraph('Hostname')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('Device hostname.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Used')
        p.style = document.styles['Title']
        p = document.add_paragraph('% Memory utilization.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Free')
        p.style = document.styles['Title']
        p = document.add_paragraph('% Memory free.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Freeable')
        p.style = document.styles['Title']
        p = document.add_paragraph('% Memory which can be freaable.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Memory utilization status. Memory utilization levels:')
        p.style = document.styles['No Spacing']
        p = document.add_paragraph('Low = 0-25%')
        p.style = document.styles['List Nilai']     
        p = document.add_paragraph('Medium = 26-75%')
        p.style = document.styles['List Nilai']
        p = document.add_paragraph('High = 75-100%')
        p.style = document.styles['List Nilai']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Memory Utilization Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Memory Utilization Analysis Summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        p = document.add_paragraph('Session Utilization Table')
        p.style = document.styles['Heading 2']

        #sql query
        cursor.execute('''SELECT devicename, session_type, session_1, session_10, session_30 FROM sessiontable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=5)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Hostname'
        hdr_cells[1].text = 'Session Type'
        hdr_cells[2].text = '1 min'
        hdr_cells[3].text = '10 min'
        hdr_cells[4].text = '30 min'

        hdr_cells[0].width = Cm(3.94)
        hdr_cells[1].width = Cm(4.19)
        hdr_cells[2].width = Cm(3)
        hdr_cells[3].width = Cm(3)
        hdr_cells[4].width = Cm(3)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(3.94)
            row_cells[1].width = Cm(4.19)
            row_cells[2].width = Cm(3)
            row_cells[3].width = Cm(3)
            row_cells[4].width = Cm(3)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1]) 
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3]) 
            row_cells[4].text = (row[4])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.TOP
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1
        
        #SESSION UTILIZASTION DETAILS
        p = document.add_paragraph('Hostname')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('Device hostname.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Session Type')
        p.style = document.styles['Title']
        p = document.add_paragraph('Type of session that used by device.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('1 min')
        p.style = document.styles['Title']
        p = document.add_paragraph('Usage or Session in 1 Minute.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('10 min')
        p.style = document.styles['Title']
        p = document.add_paragraph('Usage or Session in 10 Minutes.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('30 min')
        p.style = document.styles['Title']
        p = document.add_paragraph('Usage or Session in 30 Minutes.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Session Utilization Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Session Utilization Analysis Summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        #LICENSE AND CERTIFICATE ANALYSIS
        p = document.add_paragraph('License and Certificate Analysis of FortiGate')
        p.style = document.styles['Heading 1']

        p = document.add_paragraph('License Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #sql query
        cursor.execute('''SELECT devicename, lic_entitlement, lic_contract_exp, lic_last_update_schedule, lic_last_update_attempt, lic_result, lic_status FROM lictable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=7)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Hostname'
        hdr_cells[1].text = 'Entitlement'
        hdr_cells[2].text = 'Contract Expiry Date'
        hdr_cells[3].text = 'Last Update Using Schedule Update'
        hdr_cells[4].text = 'Last Update Attempt'
        hdr_cells[5].text = 'Result'
        hdr_cells[6].text = 'Status'

        hdr_cells[0].width = Cm(3.94)
        hdr_cells[1].width = Cm(4.19)
        hdr_cells[2].width = Cm(3)
        hdr_cells[3].width = Cm(3)
        hdr_cells[4].width = Cm(3)
        hdr_cells[5].width = Cm(3)
        hdr_cells[6].width = Cm(3)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(3.94)
            row_cells[1].width = Cm(4.19)
            row_cells[2].width = Cm(3)
            row_cells[3].width = Cm(3)
            row_cells[4].width = Cm(3)
            row_cells[5].width = Cm(3)
            row_cells[6].width = Cm(3)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1]) 
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3]) 
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5])
            row_cells[6].text = (row[6])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #LICENSE DETAILS
        p = document.add_paragraph('Entitlement')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('License entitlement.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Contract Expiry Date')
        p.style = document.styles['Title']
        p = document.add_paragraph('The date contract will expire.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Last Update Using Schedule Update')
        p.style = document.styles['Title']
        p = document.add_paragraph('The date when the device try to update using schedule update.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Last Update Attempt')
        p.style = document.styles['Title']
        p = document.add_paragraph('The date when the device try to update.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Result')
        p.style = document.styles['Title']
        p = document.add_paragraph('Result of the update status.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('License expiration status information.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('License Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<License Summary Analysis must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        document.add_page_break()

        p = document.add_paragraph('Certificate Table')
        p.style = document.styles['Heading 2']
        p.paragraph_format.space_before = Pt(6)

        #sql query
        cursor.execute('''SELECT devicename, cert_name, cert_subject, cert_issuer, cert_validto, cert_status FROM certtable ORDER BY devicename ASC''')
        records = cursor.fetchall()
        #add to document
        table = document.add_table(rows=1, cols=6)
        table.allow_autofit = True
        qr46 = func_get_req(docx_create+"qr46")
        table.style = qr46
        hdr_cells = table.rows[0].cells
        set_repeat_table_header(table.rows[0])
        table.rows[0].height = Cm(1.2)
        
        hdr_cells[0].text = 'Hostname'
        hdr_cells[1].text = 'Certificate Name'
        hdr_cells[2].text = 'Subject'
        hdr_cells[3].text = 'Issuer'
        hdr_cells[4].text = 'Valid To'
        hdr_cells[5].text = 'Status'

        hdr_cells[0].width = Cm(2.48)
        hdr_cells[1].width = Cm(3.21)
        hdr_cells[2].width = Cm(3.75)
        hdr_cells[3].width = Cm(3.75)
        hdr_cells[4].width = Cm(2.11)
        hdr_cells[5].width = Cm(2.25)

        iteration_check = False
        row_check = 'ludesdeveloper'
        count_row = 1       
        for row in records:
            row_cells = table.add_row().cells
            row_cells[0].width = Cm(2.48)
            row_cells[1].width = Cm(3.21)
            row_cells[2].width = Cm(3.75)
            row_cells[3].width = Cm(3.75)
            row_cells[4].width = Cm(2.11)
            row_cells[5].width = Cm(2.25)

            if row_check == row[0]:
                pass
            else:
                row_check = row[0]
                iteration_check = False
                start_row = table.cell(count_row, 0)
            if row_check == row[0] and iteration_check == False:
                row_cells[0].text = (row[0])
                iteration_check = True
            else:
                end_row = table.cell(count_row, 0)
                merge_row = start_row.merge(end_row)

            row_cells[1].text = (row[1]) 
            row_cells[2].text = (row[2])
            row_cells[3].text = (row[3]) 
            row_cells[4].text = (row[4])
            row_cells[5].text = (row[5])

            row_cells[0].vertical_alignment = WD_ALIGN_VERTICAL.CENTER
            row_cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[2].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
            row_cells[3].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT

            count_row+=1

        #CERTIFICATE DETAILS
        p = document.add_paragraph('Hostname')
        p.style = document.styles['Title']
        p.paragraph_format.space_before = Pt(6)
        p = document.add_paragraph('Device hostname.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Certificate Name')
        p.style = document.styles['Title']
        p = document.add_paragraph('Name of the certificate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Subject')
        p.style = document.styles['Title']
        p = document.add_paragraph('Subject of the certificate.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Issuer')
        p.style = document.styles['Title']
        p = document.add_paragraph('The date when the certificate will expire.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Status')
        p.style = document.styles['Title']
        p = document.add_paragraph('Certificate expiration status information.')
        p.style = document.styles['No Spacing']
        p.paragraph_format.space_after = Pt(6)

        p = document.add_paragraph('Certificate Analysis Summary')
        p.style = document.styles['Title']
        p = document.add_paragraph()
        p.style = document.styles['No Spacing']
        run = p.add_run('<Certificate Analysis Summary must be summarized in here>')
        font = run.font
        font.color.rgb = RGBColor(255, 0, 0)

        # Appendix FortiGate
        files = os.listdir(capture_path)
        list_name = []
        list_png = []
        
        for i in files:
            fel = re.findall('(.*).pdf', i)
            for file in fel:
                list_name.append(file)
        
        if len(list_name) == 0:
            p = document.add_page_break()
            # Kondisi Appendix FortiGate Kosong
            p = document.add_paragraph('Appendix Report FortiGate')
            p.style
            p.style.name
            'Normal'
            p.style = document.styles['Heading 1']
            p.style.name
            'Heading 1'

            # pdf tidak terupload
            document.add_paragraph()
            p = document.add_paragraph()
            run = p.add_run('PDF tidak di upload')
            font = run.font
            font.color.rgb = RGBColor(255, 0, 0)

        else:
            # Kondisi Appendix FortiGate Ada
            total = len(list_png)
            tot = len(list_name)
            for w in range(tot):
                document.add_page_break()
                p = document.add_paragraph('Appendix Report '+list_name[w])
                p.style = document.styles['Heading 1']
                fin = list_name[w]
                for png in files:
                    pd = re.findall(f'{fin}-\d+.png', png)
                    for p in pd:
                        list_png.append(p)
                total = len(list_png)
                for x in list_png:
                    if x == list_name[w]+'-0.png':
                        document.add_picture(capture_path+list_name[w]+'-0.png', width=Cm(17))
                        os.remove(capture_path+list_name[w]+'-0.png')
                print(list_png)
                
                for i in range(3, total):
                    document.add_picture(capture_path+list_name[w]+f'-{i}.png', width=Cm(17))
                    os.remove(capture_path+list_name[w]+f'-{i}.png')
                list_png.clear()
                try:    
                    os.remove(capture_path+list_name[w]+'-1.png')
                    os.remove(capture_path+list_name[w]+'-2.png')
                except:
                    pass

        db.close()
        
        #save document
        os.chdir(capture_path)
        print('')
        print('Saving Document')
        document.save('pm_fortigate.docx')
        print('Document has been saved to pm_fortigate.docx')