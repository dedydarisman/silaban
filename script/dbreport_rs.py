import sqlite3

class dbreport_rs:
    def __init__(self):
        #destroy table summarytable
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swsumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE swtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE hwsumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE hwcardtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE cpusumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE memsumtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE envtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE logtable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE eosltable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE tipetable''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE filerating''')
            db.commit()
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''DROP TABLE aset''')
            db.commit()
            db.close()
        except:
            pass
        #open db connection to table summary table
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swsumtable(id INTEGER PRIMARY KEY, version TEXT, date_eos TEXT, date_last TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE swtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, iosversion TEXT, uptime TEXT, confreg TEXT, version TEXT, date_eos TEXT, date_last TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE hwsumtable(id INTEGER PRIMARY KEY, model TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE hwcardtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, card TEXT, slot TEXT, sn TEXT, hwdscr TEXT, eosl_date TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE cpusumtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, total TEXT, process TEXT, interrupt TEXT, topcpu TEXT, status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE memsumtable(id INTEGER PRIMARY KEY, devicename TEXT,
                model TEXT, utils TEXT, topproc TEXT, status TEXT)                    
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE envtable(id INTEGER PRIMARY KEY, devicename TEXT,
                                system TEXT, item TEXT, status TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE logtable(id INTEGER PRIMARY KEY, devicename TEXT, model TEXT, script TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE eosltable(id INTEGER PRIMARY KEY, model TEXT, card TEXT, date TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE tipetable(id INTEGER PRIMARY KEY, tipe TEXT)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE filerating(id INTEGER PRIMARY KEY, filename TEXT, deviceclassname TEXT, missingsection TEXT, totalrating INTERGER)
            ''')
            db.close()
        except:
            pass
        try:
            db = sqlite3.connect('pmdb_rs')
            cursor = db.cursor()
            cursor.execute('''
                CREATE TABLE aset(id INTEGER PRIMARY KEY, devicename TEXT, model TEXT, sn TEXT, tipe TEXT, ip TEXT)
            ''')
            db.close()
        except:
            pass
