import os
import sys
import re
import shutil
import time
import requests
from datetime import datetime
import pkg_resources
import json
import xlrd
import xlsxwriter
import urllib.request
from werkzeug.utils import secure_filename
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, EqualTo, ValidationError
from flask import Flask, render_template, send_file, flash, request, redirect, Response, jsonify
from flask_login import (login_user, current_user, logout_user,
                         login_required, LoginManager, UserMixin)
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_wtf import FlaskForm
import pymongo
from docxcompose.composer import Composer
from docx import Document
from zipfile import ZipFile

# FLASK CONFIGURATION
app = Flask(__name__)
app.config['SECRET_KEY'] = 'a35ca9f60ead933ddcbf093ed5a92296'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'
# DATABASE
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)

    def __repr__(self):
        return f"User('{self.username}')"


# DIRECTORY
BASE_DIR = pkg_resources.resource_filename('netoprmgr', '')
os.chdir(BASE_DIR)
CAPT_DIR = os.path.join(BASE_DIR, 'static', 'capture')
DATA_DIR = os.path.join(BASE_DIR, 'static', 'data')
SCRIPT_DIR = os.path.join(BASE_DIR, 'script')
RESULT_DIR = os.path.join(BASE_DIR, 'static', 'result')

ALLOWED_EXTENSIONS_CAPT = set(['txt', 'log', 'pdf'])
ALLOWED_EXTENSIONS_DATA = set(['xlsx', ])

app.config['UPLOAD_FOLDER_CAPT'] = CAPT_DIR
app.config['UPLOAD_FOLDER_DATA'] = DATA_DIR


def allowed_file_capt(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_CAPT


def allowed_file_data(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_DATA
# FORM


class RegistrationForm(FlaskForm):
    username = StringField('Nama Pengguna',
                           validators=[DataRequired(), Length(min=2, max=20)])
    password = PasswordField('Kata Sandi', validators=[DataRequired()])
    confirm_password = PasswordField('Konfirmasi Sandi',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Daftar')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError(
                'Nama pengguna itu sudah terpakai. Silakan pilih yang lain.')


class LoginForm(FlaskForm):
    username = StringField('Nama Pengguna',
                           validators=[DataRequired()])
    password = PasswordField('Kata Sandi', validators=[DataRequired()])
    remember = BooleanField('Ingatkan Saya')
    submit = SubmitField('Masuk')

# ROUTE
@app.route("/")
@login_required
def home():
    chg_dir = os.chdir(SCRIPT_DIR)
    current_dir = os.getcwd()
    read_file = open('file_identification.py', 'r')
    read_file_list = read_file.readlines()
    for line in read_file_list:
        if '#' not in line and 'except NameError' in line:
            flash('Debug File Identification Still On')
    return render_template('home.html')


@app.route("/about")
def about():
    return render_template('about.html')


@app.route("/ludes_register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect('/')
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user = User(username=form.username.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect('/login')
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('/')
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect('/')
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route("/login_failed")
def login_failed():
    return render_template('login_failed.html')


@app.route("/logout_page")
def logout_page():
    return render_template('logout_page.html')


@app.route("/logout")
def logout():
    from netoprmgr.main_cli import MainCli
    MainCli.deleteCapture()
    MainCli.deleteResult()
    chg_dir = os.chdir(SCRIPT_DIR)
    current_dir = os.getcwd()
    write_file = open('active_login_count.py', 'w')
    write_file.write('0')
    logout_user()
    return redirect('/')


@app.route("/log/upload")
@login_required
def log_upload_page():
    chg_dir = os.chdir(CAPT_DIR)
    current_dir = os.getcwd()
    files = os.listdir(current_dir)
    # remove __init__ count
    count_file = (len(files)-1)

    return render_template('log_upload_page.html', count_file=count_file)


@app.route('/log/upload', methods=['POST'])
@login_required
def log_upload():
    if request.method == 'POST':
        # check if the post request has the files part
        if 'files[]' not in request.files:
            flash('No file part')
            return redirect(request.url)
        files = request.files.getlist('files[]')
        for file in files:
            if file and allowed_file_capt(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(
                    app.config['UPLOAD_FOLDER_CAPT'], filename))
                print('Uploading '+str(file))
            # flash('Logs successfully uploaded')
        flash(str(len(files))+' Data Backup Konfig berhasil di unggah')
        return redirect('/report/generate_page')


@app.route('/command_guide')
@login_required
def command_guide():
    return render_template('command_guide.html')


@app.route('/support_device')
@login_required
def support_device():
    return render_template('support_device.html')


@app.route('/report/verify_page')
@login_required
def report_verify_page():
    return render_template('report_verify.html')


@app.route('/report/verify', methods=['GET'])
@login_required
def report_verify():
    from netoprmgr.script.verify_capture import verify_capture
    verify_capture = verify_capture(CAPT_DIR)
    verify_captures = verify_capture.verify_capture()
    return jsonify(verify_captures)


@app.route('/report/generate_page')
@login_required
def report_generate_page():
    return render_template('report_generate.html')


@app.route('/report/generate')
@login_required
def report_generate():
    query_parameters = request.args
    customer = query_parameters.get('customer')
    customer = customer
    prepared = query_parameters.get('prepared')
    prepared = prepared
    review = query_parameters.get('review')
    review = review
    pic = query_parameters.get('pic')
    pic = pic
    bulan = query_parameters.get('bulan')
    bulan = bulan
    tahun = query_parameters.get('tahun')
    tahun = tahun
    report_type = query_parameters.get('my_string')
    report_type = report_type

    # tulis = input(my_string)
    def report_generate_detail():
        from netoprmgr.script.file_identification import file_identification
        from netoprmgr.script.convert_docx_cover import convert_docx_cover
        from netoprmgr.script.convert_docx_rs import convert_docx_rs
        from netoprmgr.script.convert_docx_ucm import convert_docx_ucm
        from netoprmgr.script.convert_docx_f5 import convert_docx_f5
        from netoprmgr.script.convert_docx_fortigate import convert_docx_fortigate
        from netoprmgr.script.convert_docx_log_report import convert_docx_log_report
        from netoprmgr.script.convert_xlsx_rs import convert_xlsx_rs
        from netoprmgr.script.convert_xlsx_ucm import convert_xlsx_ucm
        from netoprmgr.script.convert_xlsx_f5 import convert_xlsx_f5
        from netoprmgr.script.convert_xlsx_fortigate import convert_xlsx_fortigate
        from netoprmgr.script.convert_chart_rs import convert_chart_rs
        from netoprmgr.script.convert_chart_ucm import convert_chart_ucm
        from netoprmgr.script.convert_chart_f5 import convert_chart_f5
        from netoprmgr.script.convert_chart_fortigate import convert_chart_fortigate
        from netoprmgr.script.convert_pdf_img import convert_pdf_img
        from netoprmgr.script.dbreport_rs import dbreport_rs
        from netoprmgr.script.dbreport_ucm import dbreport_ucm
        from netoprmgr.script.dbreport_f5 import dbreport_f5
        from netoprmgr.script.dbreport_fortigate import dbreport_fortigate
        from netoprmgr.script.dbcustomer import dbcustomer
        from netoprmgr.script.dbgrtemplate import dbgrtemplate

        # change directory
        chg_dir = os.chdir(CAPT_DIR)
        current_dir = os.getcwd()
        files = os.listdir(current_dir)

        # Creat DB
        # dbreport_cover()
        dbreport_rs()
        dbreport_ucm()
        dbreport_f5()
        dbreport_fortigate()

        list_all_docx = []

        for enum, file in enumerate(files, 1):
            total_print = ('File '+str(enum)+' of '+str(len(files)))
            func_file_identification = file_identification(file)
            file_execute_print = func_file_identification.file_identification()
            yield f"data:{total_print}\n\n"
            yield f"data:Processing File : {file}\n\n"
            print(file)
        yield f"data:Processing Document\n\n"

        # KONDISI JIKA HANYA ROUTING AND SWITCHING
        if 'Routing and Switching' in report_type:

            convert_chart_rs = convert_chart_rs()
            convert_chart_rs.convert_chart_rs()

            convert_docx_rs = convert_docx_rs(
                customer, review, prepared, pic, bulan, tahun)
            convert_docx_rs.convert_docx_rs()
            time.sleep(3)

            docx_rs = (CAPT_DIR+'/pm_rs.docx')
            list_all_docx.append(docx_rs)

            src_mv = (CAPT_DIR+'/Software Summary.png')
            dst_mv = (RESULT_DIR+'/Software Summary.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/Hardware Summary.png')
            dst_mv = (RESULT_DIR+'/Hardware Summary.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/CPU Summary.png')
            dst_mv = (RESULT_DIR+'/CPU Summary.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/Memory Summary.png')
            dst_mv = (RESULT_DIR+'/Memory Summary.png')
            shutil.move(src_mv, dst_mv)

            # try:
            #     convert_xlsx_rs = convert_xlsx_rs()
            #     convert_xlsx_rs.convert_xlsx_rs()
            #     time.sleep(3)
            # except:
            #     os.remove('routing_switching.xlsx')
            #     time.sleep(3)
            #     workbook = xlsxwriter.Workbook('routing_switching.xlsx')
            #     worksheet = workbook.add_worksheet('error')
            #     worksheet.write(0, 0, 'No Data')
            #     workbook.close()
            #     print('Error Generate routing_switching.xlsx')
            #     yield f"data:Error Generate routing_switching.xlsx\n\n"

            # src_mv = (CAPT_DIR+'/routing_switching.xlsx')
            # dst_mv = (RESULT_DIR+'/routing_switching.xlsx')
            # shutil.move(src_mv, dst_mv)

        # KONDISI JIKA HANYA COLLABORATION
        if 'Collaboration' in report_type:

            convert_chart_ucm = convert_chart_ucm()
            convert_chart_ucm.convert_chart_ucm()

            convert_docx_ucm = convert_docx_ucm(
                customer, review, prepared, pic, bulan, tahun)
            convert_docx_ucm.convert_docx_ucm()
            time.sleep(3)

            docx_ucm = (CAPT_DIR+'/pm_ucm.docx')

            list_all_docx.append(docx_ucm)

            src_mv = (CAPT_DIR+'/Software Summary UCM.png')
            dst_mv = (RESULT_DIR+'/Software Summary UCM.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/CPU Summary UCM.png')
            dst_mv = (RESULT_DIR+'/CPU Summary UCM.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/Memory Summary UCM.png')
            dst_mv = (RESULT_DIR+'/Memory Summary UCM.png')
            shutil.move(src_mv, dst_mv)

            try:
                convert_xlsx_ucm = convert_xlsx_ucm()
                convert_xlsx_ucm.convert_xlsx_ucm()
                time.sleep(3)
            except:
                os.remove('collaboration.xlsx')
                time.sleep(3)
                workbook = xlsxwriter.Workbook('collaboration.xlsx')
                worksheet = workbook.add_worksheet('error')
                worksheet.write(0, 0, 'No Data')
                workbook.close()
                print('Error Generate collaboration.xlsx')
                yield f"data:Error Generate collaboration.xlsx\n\n"

            src_mv = (CAPT_DIR+'/collaboration.xlsx')
            dst_mv = (RESULT_DIR+'/collaboration.xlsx')
            shutil.move(src_mv, dst_mv)
        
        # KONDISI JIKA HANYA F5
        if 'F5' in report_type:

            convert_chart_f5 = convert_chart_f5()
            convert_chart_f5.convert_chart_f5()

            convert_docx_f5 = convert_docx_f5(
                customer, review, prepared, pic, bulan, tahun)
            convert_docx_f5.convert_docx_f5()
            time.sleep(3)

            docx_f5 = (CAPT_DIR+'/pm_f5.docx')

            list_all_docx.append(docx_f5)

            src_mv = (CAPT_DIR+'/Software Summary F5.png')
            dst_mv = (RESULT_DIR+'/Software Summary F5.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/Hardware Summary F5.png')
            dst_mv = (RESULT_DIR+'/Hardware Summary F5.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/CPU Summary F5.png')
            dst_mv = (RESULT_DIR+'/CPU Summary F5.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/Memory Summary F5.png')
            dst_mv = (RESULT_DIR+'/Memory Summary F5.png')
            shutil.move(src_mv, dst_mv)

            try:
                convert_xlsx_f5 = convert_xlsx_f5()
                convert_xlsx_f5.convert_xlsx_f5()
                time.sleep(3)
            except:
                os.remove('f5.xlsx')
                time.sleep(3)
                workbook = xlsxwriter.Workbook('f5.xlsx')
                worksheet = workbook.add_worksheet('error')
                worksheet.write(0, 0, 'No Data')
                workbook.close()
                print('Error Generate f5.xlsx')
                yield f"data:Error Generate f5.xlsx\n\n"

            src_mv = (CAPT_DIR+'/f5.xlsx')
            dst_mv = (RESULT_DIR+'/f5.xlsx')
            shutil.move(src_mv, dst_mv)
        
        #KONDISI JIKA HANYA FORTIGATE
        if 'FortiGate' in report_type:

            #Cek ada file pdf atau tidak
            files = os.listdir(CAPT_DIR)
            list_appendix_pdf = []
            for file in files:
                if '.pdf' in file:
                    appendix_pdf = file
                    list_appendix_pdf.append(appendix_pdf)
                else:
                    pass

            convert_chart_fortigate = convert_chart_fortigate()
            convert_chart_fortigate.convert_chart_fortigate()

            if len(list_appendix_pdf) == 0:
                pass
            else:
                convert_pdf_img = convert_pdf_img()
                convert_pdf_img.convert_pdf_img()
                time.sleep(3)

            convert_docx_fortigate = convert_docx_fortigate(
                customer, review, prepared, pic, bulan, tahun)
            convert_docx_fortigate.convert_docx_fortigate()
            time.sleep(3)

            docx_fortigate = (CAPT_DIR+'/pm_fortigate.docx')

            list_all_docx.append(docx_fortigate)

            src_mv = (CAPT_DIR+'/CPU Summary FortiGate.png')
            dst_mv = (RESULT_DIR+'/CPU Summary FortiGate.png')
            shutil.move(src_mv, dst_mv)
            src_mv = (CAPT_DIR+'/Memory Summary FortiGate.png')
            dst_mv = (RESULT_DIR+'/Memory Summary FortiGate.png')
            shutil.move(src_mv, dst_mv)

            try:
                convert_xlsx_fortigate = convert_xlsx_fortigate()
                convert_xlsx_fortigate.convert_xlsx_fortigate()
                time.sleep(3)
            except:
                # os.remove('fortigate.xlsx')
                time.sleep(3)
                workbook = xlsxwriter.Workbook('fortigate.xlsx')
                worksheet = workbook.add_worksheet('error')
                worksheet.write(0, 0, 'No Data')
                workbook.close()
                print('Error Generate fortigate.xlsx')
                yield f"data:Error Generate fortigate.xlsx\n\n"

            src_mv = (CAPT_DIR+'/fortigate.xlsx')
            dst_mv = (RESULT_DIR+'/fortigate.xlsx')
            shutil.move(src_mv, dst_mv)

        # DOCX COVER
        convert_docx_cover = convert_docx_cover(
            customer, review, prepared, pic, bulan, tahun, report_type)
        convert_docx_cover.convert_docx_cover()
        time.sleep(3)

        # # DOCX LOG REPORT
        # convert_docx_log_report = convert_docx_log_report()
        # convert_docx_log_report.convert_docx_log_report()
        # time.sleep(3)

        docx_cover = (CAPT_DIR+'/pm_cover.docx')
        final_file_name = (f'Laporan Jaringan {customer}.docx')
        final_file = (CAPT_DIR+'/'+final_file_name)

        for enum, list_docx in enumerate(list_all_docx):
            # step 1 condition
            if enum == 0:
                master = Document(docx_cover)
                composer = Composer(master)
                doc_1 = Document(list_all_docx[enum])
                composer.append(doc_1)
                composer.save(final_file_name)
                os.remove(list_all_docx[enum])
            # step 2 condition
            else:
                master = Document(final_file)
                composer = Composer(master)
                doc_1 = Document(list_all_docx[enum])
                composer.append(doc_1)
                composer.save(final_file_name)
                os.remove(list_all_docx[enum])

        src_mv = (CAPT_DIR+'/'+final_file_name)
        dst_mv = (RESULT_DIR+'/'+final_file_name)
        shutil.move(src_mv, dst_mv)

        # src_mv = (CAPT_DIR+'/log_generate_report.docx')
        # dst_mv = (RESULT_DIR+'/log_generate_report.docx')
        # shutil.move(src_mv, dst_mv)

        # # send helmi's data
        # func_dbcustomer = dbcustomer(customer)
        # func_dbcustomer = func_dbcustomer.dbcustomer()
        # func_dbgrtemplate = dbgrtemplate(customer)
        # func_dbgrtemplate = func_dbgrtemplate.dbgrtemplate()

        os.remove("pm_cover.docx")
        os.remove("pmdb_rs")
        os.remove("pmdb_ucm")
        os.remove("pmdb_f5")
        os.remove("pmdb_fortigate")
        # os.remove("watermark.png")

        yield f"data:Zipping files to Silaban.zip\n\n"

        chg_dir = os.chdir(RESULT_DIR)

        try:
            os.remove("Silaban.zip")
        except:
            pass

        current_dir = os.getcwd()
        files = os.listdir(current_dir)
        zipObj = ZipFile('Silaban.zip', 'w')
        for file in files:
            if '__init__.py' in file:
                pass
            elif '.png' in file:
                pass
            else:
                zipObj.write(file)
        zipObj.close()

        # yield f"data:Document has been saved to Laporan Jaringan DISPUSIP.docx\n\n"
        # yield f"data:Document has been saved to Laporan Jaringan DISPUSIP.xlsx\n\n"
        yield f"data:Zipping files is success to Silaban.zip\n\n"
        yield f"data:Finished\n\n"

        print("FINISHED ALL")
    # return redirect('/report/result')
    return Response(report_generate_detail(), mimetype='text/event-stream')


@app.route('/report/result')
@login_required
def report_download():
    return render_template('report_download.html')


@app.route('/log/generate_page', methods=['GET', 'POST'])
@login_required
def log_generate_page():
    return render_template('log_generate.html')


@app.route('/log/generate', methods=['GET', 'POST'])
@login_required
def log_generate():
    # request value from front end
    month_list = re.findall('([A-Za-z][A-Za-z][A-Za-z])', str(request.data))
    year_get = re.findall('\d+', str(request.data))
    year = ''
    for i in year_get:
        year = i

    def log_generate_detail():
        # import module
        from netoprmgr.script.get_log import get_log
        # change directory
        chg_dir = os.chdir(CAPT_DIR)
        current_dir = os.getcwd()
        # create list
        list_hostname = []
        list_log = []
        list_severity_value = []
        # iteration over function
        files = os.listdir(current_dir)
        for enum, file in enumerate(files):
            # yield to front end
            yield f"Processing File : {file}|"
            func_get_log = get_log(file, month_list, year)
            # getting value return from function
            get_list_hostname, get_list_log, get_list_severity_value = func_get_log.get_log()
            list_hostname = list_hostname + get_list_hostname
            list_log = list_log + get_list_log
            list_severity_value = list_severity_value + get_list_severity_value
        # removing db
        time.sleep(3)
        for file in files:
            if file.endswith("db"):
                os.remove(file)
        # creating log file from list
        print('')
        print('Processing Document')
        write = open('logresult_rs.csv', 'w')
        write.write('Device Name'+' | '+'Log Event' +
                    ' | '+'Severity Value'+' | '+'Severity'+'\n')
        for enum, logtext in enumerate(list_log):
            write.write(list_hostname[enum]+' | '+logtext +
                        ' | '+list_severity_value[enum]+'\n')
        write.close()
        print('')
        print('Saving Document')
        print('Document has been saved to logresult_rs.csv')
        time.sleep(3)
        src_mv = (CAPT_DIR+'/logresult_rs.csv')
        dst_mv = (RESULT_DIR+'/logresult_rs.csv')
        shutil.move(src_mv, dst_mv)
        yield f"Document has been saved to logresult_rs.csv|"
        time.sleep(1)
        yield f"Finished|"
    return Response(log_generate_detail(), mimetype='text/event-stream')


@app.route('/log/result')
@login_required
def log_download():
    return render_template('log_download.html')
# batas
@app.route('/log')
@login_required
def log_delete_page():
    return render_template('log_delete.html')


@app.route('/log/delete')
@login_required
def log_delete():
    from netoprmgr.main_cli import MainCli
    MainCli.deleteCapture()
    MainCli.deleteResult()
    return redirect('/log/upload')

# http://localhost:5002/search?PID=WS-DEDAR
@app.route('/search', methods=['GET'])
def api_search():
    query_parameters = request.args

    PID = query_parameters.get('PID')
    Script = query_parameters.get('Script')

    chg_dir = os.chdir(DATA_DIR)
    current_dir = os.getcwd()
    support_dir = (DATA_DIR+'/support_devices.xlsx')
    book = xlrd.open_workbook(support_dir)
    first_sheet = book.sheet_by_index(0)
    cell = first_sheet.cell(0, 0)
    to_filter = []

    if not (PID or Script):
        return 'page_not_found(404)'

    for i in range(first_sheet.nrows):
        if first_sheet.row_values(i)[1] == PID:
            results = first_sheet.row_values(i)[3]

    return results


@app.route('/autocomplete', methods=['GET'])
def api_autocomplete():
    chg_dir = os.chdir(DATA_DIR)
    current_dir = os.getcwd()
    support_dir = (DATA_DIR+'/support_devices.xlsx')
    book = xlrd.open_workbook(support_dir)
    first_sheet = book.sheet_by_index(0)
    cell = first_sheet.cell(0, 0)

    PID = []
    Script = []
    count = 1
    for i in range(first_sheet.nrows):
        try:
            PID.append(first_sheet.row_values(count)[1])
            Script.append(first_sheet.row_values(count)[3])
            count += 1
        except:
            pass

    return jsonify(PID)
# pre run check


def pre_run_check():
    from netoprmgr.script.update_ip_variables import execute_update_ip
    execute_update_ip(SCRIPT_DIR)
    # chg_dir = os.chdir(SCRIPT_DIR)
    # current_dir = os.getcwd()
    act_log_dir = (SCRIPT_DIR+'/active_login_count.py')
    write_file = open(act_log_dir, 'w')
    write_file.write('0')


@app.route("/template_generate_page")
@login_required
def template_generate_page():
    return render_template('template_generate_page.html')


@app.route('/template_generate/')
@login_required
def template_generate():
    from netoprmgr.main_cli import MainCli
    MainCli.createTemplate()
    return send_file(DATA_DIR+'/template_chart.xlsx', attachment_filename='template_chart.xlsx', as_attachment=True)


@app.route('/chart/create_chart_page')
@login_required
def create_chart_page():
    return render_template('create_chart_page.html')


@app.route('/chart/create')
@login_required
def chart_create():
    def chart_create_detail():
        from netoprmgr.script.convert_chart import convert_chart

        convert_chart = convert_chart()
        convert_chart.reconvert_chart()
        time.sleep(3)

        src_mv = (CAPT_DIR+'/Software Summary.png')
        dst_mv = (RESULT_DIR+'/Software Summary.png')
        shutil.move(src_mv, dst_mv)
        src_mv = (CAPT_DIR+'/Hardware Summary.png')
        dst_mv = (RESULT_DIR+'/Hardware Summary.png')
        shutil.move(src_mv, dst_mv)
        src_mv = (CAPT_DIR+'/CPU Summary.png')
        dst_mv = (RESULT_DIR+'/CPU Summary.png')
        shutil.move(src_mv, dst_mv)
        src_mv = (CAPT_DIR+'/Memory Summary.png')
        dst_mv = (RESULT_DIR+'/Memory Summary.png')
        shutil.move(src_mv, dst_mv)
        os.remove("pmdb")
        yield f"data:Document has been saved to Laporan Jaringan DISPUSIP.docx\n\n"
        yield f"data:Finished\n\n"
    # return redirect('/report/result')
    return Response(chart_create_detail(), mimetype='text/event-stream')


@app.route('/port_check')
def port_check():
    chg_dir = os.chdir(SCRIPT_DIR)
    current_dir = os.getcwd()
    read_file = open('active_login_count.py', 'r')
    read_file_list = read_file.readlines()
    return jsonify(read_file_list[0])


@app.route('/api_cs', methods=['GET'])
def api_cs():
    URL = "https://ytvhi2fa9h.execute-api.ap-southeast-1.amazonaws.com/default/mtcm_customer"

    # PARAMS = {'customer':customerName}
    # params = dict(customer='customer', name=["name"])

    r = requests.get(url=URL)

    cs = r.json()

    return (cs)


@app.route('/api_engineer', methods=['GET'])
def api_engineer():
    URL = "https://57y8oszxol.execute-api.ap-southeast-1.amazonaws.com/default/mtcm_engineer"

    # PARAMS = {'customer':customerName}
    # params = dict(customer='customer', name=["name"])

    r = requests.get(url=URL)

    engineer = r.json()

    return (engineer)


if __name__ == "__main__":
    pre_run_check()
    # app.run(debug=True, host='0.0.0.0', port='5002')
    app.run(debug=True, host='0.0.0.0', port='5002', ssl_context=('cert.pem', 'key.pem'))